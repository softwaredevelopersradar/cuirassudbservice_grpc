﻿using System;
using CuirassUModelsDBLib.Errors;

namespace GrpcDbClientLib
{
    public class ExceptionDatabase : Exception, CuirassUModelsDBLib.InheritorsExceptions.IExceptionDb
    {
        public EnumDBError Error { get; protected set; }

        public ExceptionDatabase(EnumDBError error) : base(DBError.GetDefenition(error))
        {
            Error = error;
        }

        public ExceptionDatabase(string message) : base(message)
        {
            Error = EnumDBError.UnknownError;
        }

        public ExceptionDatabase(CuirassUModelsDBLib.InheritorsExceptions.IExceptionDb exception) : base(exception.Message)
        {
            Error = exception.Error;
        }
    }
}
