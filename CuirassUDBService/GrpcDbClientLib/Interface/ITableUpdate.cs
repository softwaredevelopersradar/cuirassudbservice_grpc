﻿using CuirassUModelsDBLib.InheritorsEventArgs;
using System;

namespace GrpcDbClientLib
{
    public interface ITableUpdate<T> where T : CuirassUModelsDBLib.AbstractCommonTable
    {
        event EventHandler<TableEventArgs<T>> OnUpTable;
    }
}
