﻿using CuirassUModelsDBLib.InheritorsEventArgs;
using System;

namespace GrpcDbClientLib
{
    public interface ITableAddRange<T> where T : CuirassUModelsDBLib.AbstractCommonTable
    {
        event EventHandler<TableEventArgs<T>> OnAddRange;
    }
}
