﻿using CuirassUModelsDBLib.InheritorsEventArgs;

namespace GrpcDbClientLib
{
    internal interface IClickUpRecord
    {
        void ClickUpRecord(RecordEventArgs eventArgs);
    }
}
