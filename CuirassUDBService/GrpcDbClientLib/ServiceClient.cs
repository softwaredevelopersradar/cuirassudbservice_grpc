﻿using System;
using CuirassUDbPackage;
using Grpc.Core;
using System.Threading.Tasks;
using System.Collections.Generic;
using CuirassUModelsDBLib;
using CuirassUModelsDBLib.InheritorsEventArgs;
using System.Threading;
using ProtoCuirassULib;

namespace GrpcDbClientLib
{
    public class ServiceClient : IDisposable
    {
        private int _ID;
        private readonly string _name = "";
        private readonly string _serverIp = "127.0.0.1";
        private readonly int _serverPort = 30051;        
        private CuirassUDbTransmission.CuirassUDbTransmissionClient? _client;

        private Channel? _channel;
        private CancellationTokenSource? _tokenSource;
        //private bool _isInitialized;
        private int _deadlineMs = 5000;
        public DateTime Deadline => DateTime.UtcNow.AddMilliseconds(_deadlineMs);

        private bool _isConnected;
        public bool IsConnected
        {
            get
            {
                if (_channel != null && _channel.State == ChannelState.Ready)
                    return true;
                return false;
            }
            private set
            {
                if (_isConnected != value)
                {
                    if (value)
                    {
                        OnConnect?.Invoke(this, new ClientEventArgs(ClientEventArgs.ActServer.Connect, $"Id = {_ID}"));
                    }
                    else
                        OnDisconnect?.Invoke(this, new ClientEventArgs(ClientEventArgs.ActServer.Connect, $"Id = {_ID}"));
                }

                _isConnected = value;
            }
        }

        public ServiceClient(string name, string serverIp, int serverPort)
        {
            try { 
                _name = name;
                _serverIp = serverIp;
                _serverPort = serverPort;

            }
            catch (Exception ex)
            { }
        }

        public ServiceClient(string name, string serverIp, int serverPort, int deadlineReplyMs)
        {
            try
            {
                _name = name;
                _serverIp = serverIp;
                _serverPort = serverPort;
                this._deadlineMs = deadlineReplyMs;

            }
            catch (Exception ex)
            { }
        }


        /// <summary>
        /// словарь, который хранит NameTable - имя таблицы, 
        /// ITableAction - объект класса, который реализует интерфейс(ITableAction - действия над таблицей) 
        /// </summary>
        public readonly Dictionary<NameTable, IClassTables> Tables = new Dictionary<NameTable, IClassTables>
        {
            { NameTable.TableRemotePoints, new ClassTable<TableRemotePoints>() },
            { NameTable.TableSectorsRecon, new ClassTable<TableSectorsRecon>() },
            { NameTable.TableFreqForbidden, new ClassTable<TableFreqForbidden>() },
            { NameTable.TableFreqRangesRecon, new ClassTable<TableFreqRangesRecon>() },
            { NameTable.TableRes, new ClassTable<TableRes>() },
            { NameTable.GlobalPropertiesRRS, new ClassTable<GlobalPropertiesRRS>() },
            { NameTable.GlobalProperties, new ClassTable<GlobalProperties>() },
            { NameTable.TableResPattern, new ClassTable<TableResPattern>() },
            { NameTable.TableResArchive, new ClassTable<TableResArchive>() },
            { NameTable.TableTrackArchive, new ClassTable<TableTrackArchive>() },
            { NameTable.TableResDistribution, new ClassTable<TableResDistribution>() },
            { NameTable.TableTrackDistribution, new ClassTable<TableTrackDistribution>() }
        };

        #region Events
        public event EventHandler<ClientEventArgs>? OnConnect;
        public event EventHandler<ClientEventArgs>? OnDisconnect;
        public event EventHandler<DataEventArgs>? OnUpData;
        public event EventHandler<OperationTableEventArgs>? OnErrorDataBase;
        #endregion


        //public void Abort()
        //{
           
        //      //(Transmission.TransmissionClient)_client.).Dispose(); //check
        //    _client = null;

        //    foreach (ClassTable table in Tables.Values)
        //        table.Dispose();
        //    OnDisconnect(null, new ClientEventArgs(ClientEventArgs.ActServer.Disconnect));
        //}

        private async Task Abort()
        {
            try
            {
               this._tokenSource?.Cancel();
                this.IsConnected = false;

                await Task.Delay(3000);
                if (_channel != null && !this._channel.ShutdownToken.IsCancellationRequested)
                    await _channel.ShutdownAsync().ConfigureAwait(false);
                
                 
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            
            //this._channel = null;
            this._client = null;
            foreach (ClassTable table in Tables.Values)
                table.Dispose();
        }

        //public bool IsConnected()
        //{
        //    if (_client == null)
        //        return false;
        //    try
        //    {
        //        return _client.Ping(new IdMessage { Id = _ID }).IsFirstPingRequest;
        //    }
        //    catch (Exception except)
        //    {
        //        OnErrorDataBase(this, new OperationTableEventArgs(NameTableOperation.Update, except.Message));
        //        return false;
        //    }
        //}

        public void Connect()
        {
            try
            {
                if (IsConnected)
                {
                    //OnConnect(this, new ClientEventArgs(ClientEventArgs.ActServer.Connect));
                    return;
                }

                this._channel = new Channel(_serverIp, _serverPort, ChannelCredentials.Insecure);
                this._client = new CuirassUDbTransmission.CuirassUDbTransmissionClient(
                    this._channel);

                Task.Run(async () => await Subscribe());

                _ID = _client.Connect(new PingRequest() {ClientName = _name }).Id;

                ClassTable.Id = _ID;
                ClassTable.ClientServiceDB = _client;
                IsConnected = true;
                //OnConnect(this, new ClientEventArgs(ClientEventArgs.ActServer.Connect, $"Id = {_ID}"));

            }
            catch (Exception error)
            {
                IsConnected = false;
                //OnDisconnect(this, new ClientEventArgs(ClientEventArgs.ActServer.Disconnect, error.Message));
            }
        }

        public async Task ConnectAsync()
        {
            try
            {
                if (IsConnected)
                {
                    return;
                }

                this._channel = new Channel(_serverIp, _serverPort, ChannelCredentials.Insecure);
                this._client = new CuirassUDbTransmission.CuirassUDbTransmissionClient(this._channel);
                
                var result = await this._client.ConnectAsync(request: new PingRequest() { ClientName = _name }, deadline: Deadline).ConfigureAwait(false);
                this._ID = result.Id;
                ClassTable.Id = _ID;
                ClassTable.ClientServiceDB = this._client;
                Task.Run(async () => await Subscribe().ConfigureAwait(false));

                IsConnected = true;
            }
            catch (Exception error)
            {
                IsConnected = false;
            }
        }

        public void Disconnect()
        {
            try
            {
                if (_client == null) 
                    return;

                _client.Disconnect(request: new IdMessage() { Id = _ID });
                Abort();
                
            }
            catch (Exception error)
            {
                Abort();
                throw new ExceptionClient(error.Message);
            }
        }

        public async Task DisconnectAsync()
        {
            try
            {
                if (this._client == null)
                    return;

                await this._client.DisconnectAsync(request: new IdMessage() { Id = _ID }, deadline: Deadline).ConfigureAwait(false);
                await this.Abort().ConfigureAwait(false);

            }
            catch (Exception error)
            {
                await this.Abort().ConfigureAwait(false);
                //throw new ExceptionClient(error.Message);
            }
        }




        public bool Ping()
        {
            if (_client == null) throw new InvalidOperationException("Client is null!");

            var result = _client.Ping(new IdMessage {Id = _ID });
            return result.IsFirstPingRequest;
        }

        //private async Task Subscribe()
        //{
        //    //if (_client == null) throw new InvalidOperationException("Client is null!");
        //    using (var stream = _client?.Subscribe(new DefaultRequest()))
        //    {
        //        var tokenSource = new CancellationTokenSource();
        //        if (stream != null) await DisplayAsync(stream.ResponseStream, tokenSource.Token);

        //        tokenSource.Cancel();
        //    }
        //}

        //private async Task DisplayAsync(IAsyncStreamReader<LoadMessage> stream, CancellationToken token)
        //{
        //    try
        //    {
        //        while (await stream.MoveNext(cancellationToken: token))
        //        {
        //            var responce = stream.Current;
        //            var nameTable = TypesConverter.ConvertToDBModel(responce.Table);
        //            var records = responce.Records.ConvertToDBModel(nameTable);

        //            OnUpData(this, new DataEventArgs(nameTable, records));
        //            (Tables[nameTable] as IClickUpData)?.ClickUpTable(records);
        //        }
        //    }
        //    catch (RpcException e)
        //    {
        //        if (e.StatusCode == StatusCode.Cancelled)
        //        {
        //            return;
        //        }

        //        Disconnect();
        //    }
        //    catch (OperationCanceledException)
        //    {
        //        Console.WriteLine("Finished.");
        //    }
        //    catch (Exception excp)
        //    {
        //        OnErrorDataBase(this, new OperationTableEventArgs(NameTableOperation.Update, excp.Message));
        //        Disconnect();
        //        return;
        //    }
        //}


        /// <summary>
        /// Moves to next message
        /// </summary>
        /// <remarks>
        /// <paramref name="cancellation"/> is not honored between waiting for next call in <see cref="IAsyncStreamReader{T}.MoveNext"/>, so this method solves it with extra cancellation task respecting <paramref name="cancellation"/> and <see cref="Task.WhenAny"/>, but when <paramref name="cancellation"/> is cancelled between messages, <see cref="IAsyncStreamReader{T}.MoveNext"/> is still in progress which can be stopped by disposing its call.
        /// </remarks>
        /// <typeparam name="T">Message type</typeparam>
        /// <param name="stream">Stream</param>
        /// <param name="cancellation">Cancellation</param>
        /// <returns>Whether there is any next message</returns>
        public static async Task<bool> MoveNextWith<T>(IAsyncStreamReader<T> stream, CancellationToken cancellation)
        {
            bool next;
            if (cancellation.CanBeCanceled)
            {
                var cancellationTaskSource = new TaskCompletionSource<bool>();
                using (var cancellationRegistration = cancellation.Register(
                    ct =>
                        {
                            if (ct == null) throw new ArgumentNullException(nameof(ct));
                            ((TaskCompletionSource<bool>)ct).SetCanceled();
                        },
                    cancellationTaskSource
                ))
                {
                var moveNextTask = stream.MoveNext(cancellation);
                var task = await Task.WhenAny(moveNextTask, cancellationTaskSource.Task);
                if (task == cancellationTaskSource.Task)
                    throw new OperationCanceledException(cancellation);
                next = await task.ConfigureAwait(false);
                }
                
            }
            else
                next = await stream.MoveNext(cancellation);
            return next;
        }

        //private async Task Subscribe()
        //{
        //    if (this._client == null) return;

        //    try
        //    {
        //        this._tokenSource = new CancellationTokenSource();
        //        var serverData = _client.Subscribe(new DefaultRequest());
        //        var responseStream = serverData.ResponseStream;
        //        while (await MoveNextWith(responseStream, cancellation: _tokenSource.Token))
        //        //while (await responseStream.MoveNext(cancellationToken: _tokenSource.Token))
        //        {
        //            var response = responseStream.Current;
        //            Task.Run(async () => await DecodeDataFromServer(response).ConfigureAwait(false));
        //        }
        //    }
        //    catch (RpcException e)
        //    {
        //        await this.Abort().ConfigureAwait(false);
        //    }
        //    catch (Exception ex)
        //    {

        //        OnErrorDataBase?.Invoke(this, new OperationTableEventArgs(NameTableOperation.Update, ex.Message));
        //        await this.Abort().ConfigureAwait(false);
        //    }
        //}

        private async Task Subscribe()
        {
            if (this._client == null) return;

            try
            {
                this._tokenSource = new CancellationTokenSource();
                using (var serverData = _client.Subscribe(new DefaultRequest(), cancellationToken: _tokenSource.Token))
                //using ( var serverData = _client.Subscribe(new DefaultRequest(), cancellationToken: _tokenSource.Token))
                {
                    var responseStream = serverData.ResponseStream;
                    //while (await MoveNextWith(responseStream, cancellation: _tokenSource.Token))
                    //while (await responseStream.MoveNext(cancellationToken: _tokenSource.Token))
                    
                    while (await responseStream.MoveNext(cancellationToken: _tokenSource.Token).ConfigureAwait(false))
                    {
                        var response = responseStream.Current;
                        Task.Run(async () => await DecodeDataFromServer(response).ConfigureAwait(false));
                    }
                }
                
            }
            catch (RpcException e)
            {
                if (e.StatusCode == StatusCode.Cancelled)
                {
                    return;
                }
                await this.Abort().ConfigureAwait(false);
            }
            catch (Exception ex)
            {
                if (_tokenSource!=null && _tokenSource.IsCancellationRequested)
                {
                    return;
                }
                OnErrorDataBase?.Invoke(this, new OperationTableEventArgs(NameTableOperation.Update, ex.Message));
                await this.Abort().ConfigureAwait(false);
            }
        }

        private async Task DecodeDataFromServer(LoadMessage? responce)
        {
            try
            {
                if (responce == null) return;

                var nameTable = TypesConverter.ConvertToDBModel(responce.Table);
                var records = responce.Records.ConvertToDBModel(nameTable);

                this.OnUpData?.Invoke(this, new DataEventArgs(nameTable, records));
                (Tables[nameTable] as IClickUpData)?.ClickUpTable(records);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                OnErrorDataBase?.Invoke(this, new OperationTableEventArgs(NameTableOperation.Update, e.Message));
            }
           
        }


        //public void Dispose()
        //{
        //    try { Disconnect(); }
        //    catch { }
        //}

        public async void Dispose()
        {
            try
            {
                await this.DisconnectAsync().ConfigureAwait(false);
            }
            catch { }
        }

        public void ShutDown()
        {
            if (!IsConnected)
                return;
            IsConnected = false;

            this._tokenSource?.Cancel();
            _channel?.ShutdownAsync()?.ConfigureAwait(false);
            this._client = null;
            foreach (ClassTable table in Tables.Values)
                table.Dispose();
        }
    }
}
