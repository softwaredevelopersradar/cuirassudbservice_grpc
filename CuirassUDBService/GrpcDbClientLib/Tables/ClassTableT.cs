﻿namespace GrpcDbClientLib
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;

    using CuirassUDbPackage;

    using CuirassUModelsDBLib;
    using CuirassUModelsDBLib.Errors;
    using CuirassUModelsDBLib.InheritorsEventArgs;

    using Grpc.Core;

    using ProtoCuirassULib;

    internal class ClassTable<T> : ClassTable, IClassTables, IClickUpData, IClickUpRecord, IClickUpAddRange, ITableUpRecord<T>, ITableAddRange<T>, ITableUpdate<T> where T : AbstractCommonTable
    {
        protected NameTable Name;
        #region ITableAddRange

        public virtual event EventHandler<TableEventArgs<T>>? OnAddRange;

        public virtual void ClickUpAddRange(ClassDataCommon dataCommon)
        {
            OnAddRange?.Invoke(this, new TableEventArgs<T>(dataCommon));
        }
        #endregion

        #region UpFullData

        public virtual event EventHandler<TableEventArgs<T>>? OnUpTable;

        public virtual void ClickUpTable(ClassDataCommon data)
        {
            OnUpTable?.Invoke(this, new TableEventArgs<T>(data));
        }

        #endregion

        #region UpRecord

        public virtual event EventHandler<T>? OnAddRecord;

        public virtual event EventHandler<T>? OnDeleteRecord;

        public virtual event EventHandler<T>? OnChangeRecord;


        public virtual void ClickUpRecord(RecordEventArgs eventArgs)
        {
            switch (eventArgs.NameAction)
            {
                case NameChangeOperation.Add:
                    OnAddRecord?.Invoke(this, eventArgs.AbstractRecord as T ?? throw new InvalidOperationException());
                    break;
                case NameChangeOperation.Change:
                    OnChangeRecord?.Invoke(this, eventArgs.AbstractRecord as T ?? throw new InvalidOperationException());
                    break;
                case NameChangeOperation.Delete:
                    OnDeleteRecord?.Invoke(this, eventArgs.AbstractRecord as T ?? throw new InvalidOperationException());
                    break;
            }
        }

        #endregion

        public ClassTable() : base()
        {
            object[] attrs = typeof(T).GetCustomAttributes(typeof(InfoTableAttribute), false);
            foreach (InfoTableAttribute info in attrs)
            {
                Name = info.Name;
            }
        }

        public ClassTable(ref CuirassUDbTransmission.CuirassUDbTransmissionClient clientServiceDB, int id) : base(clientServiceDB, id)
        {
            object[] attrs = typeof(T).GetCustomAttributes(typeof(InfoTableAttribute), false);
            foreach (InfoTableAttribute info in attrs)
            {
                Name = info.Name;
            }
        }

        protected bool ValidConnection()
        {
            if (ClientServiceDB == null)
            {
                //добавить ф-цию на стороне сервера Ping()
                // ErrorCallbackClient(this, Errors.EnumClientError.NoConnection, "");
                return false;
            }
            return true;
        }


        public virtual void Add(object obj)
        {
            try
            {
                if (!ValidConnection())
                {
                    throw new ExceptionClient(EnumClientError.NoConnection);
                }
                var record = obj as AbstractCommonTable;
                if (record == null) 
                    throw new InvalidOperationException("Record is null!");

                var message = new ChangeRecordMessage() { Table = this.Name.ConvertToProto(), Operation = EnumChangeRecordOperation.Add, Record = record.ConvertToProto(this.Name) };
                var result = ClientServiceDB?.ChangeRecord(message, this.CreateMetadata());
                return;
            }
            catch (RpcException rpcEx)
            {
                throw new ExceptionClient(rpcEx.Message);
            }
            catch (ExceptionClient except)
            {
                throw new ExceptionClient(except);
            }
            catch (Exception error)
            {
                throw new ExceptionClient(error.Message);
            }
        }

        public virtual async Task AddAsync(object obj)
        {
            try
            {
                if (!ValidConnection())
                {
                    throw new ExceptionClient(EnumClientError.NoConnection);
                }
                var record = obj as AbstractCommonTable;
                if (record == null)
                    throw new InvalidOperationException("Record is null!");

                var message = new ChangeRecordMessage() { Table = Name.ConvertToProto(), Operation = EnumChangeRecordOperation.Add, Record = record.ConvertToProto(Name) };
                if (ClientServiceDB != null)
                {
                    var result = await ClientServiceDB.ChangeRecordAsync(message, this.CreateMetadata())
                                     .ConfigureAwait(false);
                }
            }
            catch (ExceptionClient except)
            {
                throw new ExceptionClient(except);
            }
            catch (Exception error)
            {
                throw new ExceptionClient(error.Message);
            }
        }

        public virtual void AddRange(object rangeObj)
        {
            try
            {
                if (!ValidConnection())
                {
                    throw new ExceptionClient(EnumClientError.NoConnection);
                }
                if (rangeObj is List<T>)
                {
                    ClassDataCommon data = ClassDataCommon.ConvertToListAbstractCommonTable(rangeObj as List<T>) ?? throw new InvalidOperationException("Data is null!");
                    var message = new ChangeRangeMessage() { Table = Name.ConvertToProto(), Operation = EnumChangeRecordOperation.Add };
                    message.Records.AddRange(data.ConvertToProto(Name));
                    var result = ClientServiceDB?.ChangeRange(message, CreateMetadata());
                    return;
                }
                throw new ExceptionClient("AddRange: Mismatch with expected type!");
            }
            catch (ExceptionClient except)
            {
                throw new ExceptionClient(except);
            }
            catch (Exception error)
            {
                throw new ExceptionClient(error.Message);
            }
        }

        public virtual async Task AddRangeAsync(object rangeObj)
        {
            try
            {
                if (!ValidConnection())
                {
                    throw new ExceptionClient(EnumClientError.NoConnection);
                }

                if (rangeObj is List<T>)
                {
                    ClassDataCommon data = ClassDataCommon.ConvertToListAbstractCommonTable(rangeObj as List<T>) ?? throw new InvalidOperationException("Data is null!");
                    var message = new ChangeRangeMessage() { Table = Name.ConvertToProto(), Operation = EnumChangeRecordOperation.Add };
                    message.Records.AddRange(data.ConvertToProto(Name));
                    if (ClientServiceDB != null)
                    {
                        var result = await ClientServiceDB.ChangeRangeAsync(message, this.CreateMetadata());
                    }

                    return;
                }
                throw new ExceptionClient("AddRange: Mismatch with expected type!");
            }
            catch (ExceptionClient except)
            {
                throw new ExceptionClient(except);
            }
            catch (Exception error)
            {
                throw new ExceptionClient(error.Message);
            }
        }

        public virtual void RemoveRange(object rangeObj)
        {
            try
            {
                if (!ValidConnection())
                {
                    throw new ExceptionClient(EnumClientError.NoConnection);
                }
                if (rangeObj is List<T>)
                {
                    ClassDataCommon data = ClassDataCommon.ConvertToListAbstractCommonTable(rangeObj as List<T>) ?? throw new InvalidOperationException("Data is null!");
                    var message = new ChangeRangeMessage() { Table = Name.ConvertToProto(), Operation = EnumChangeRecordOperation.Delete };
                    message.Records.AddRange(data.ConvertToProto(Name));
                    var result = ClientServiceDB?.ChangeRange(message, CreateMetadata());
                    return;
                }
                throw new ExceptionClient("RemoveRange: Mismatch with expected type!");
            }
            catch (ExceptionClient except)
            {
                throw new ExceptionClient(except);
            }
            catch (Exception error)
            {
                throw new ExceptionClient(error.Message);
            }
        }

        public virtual async Task RemoveRangeAsync(object rangeObj)
        {
            try
            {
                if (!ValidConnection())
                {
                    throw new ExceptionClient(EnumClientError.NoConnection);
                }
                if (rangeObj is List<T>)
                {
                    ClassDataCommon data = ClassDataCommon.ConvertToListAbstractCommonTable(rangeObj as List<T>) ?? throw new InvalidOperationException("Data is null!");
                    var message = new ChangeRangeMessage() { Table = Name.ConvertToProto(), Operation = EnumChangeRecordOperation.Delete };
                    message.Records.AddRange(data.ConvertToProto(Name));
                    if (ClientServiceDB != null)
                    {
                        var result = await ClientServiceDB.ChangeRangeAsync(message, this.CreateMetadata());
                    }

                    return;
                }
                throw new ExceptionClient("RemoveRange: Mismatch with expected type!");
            }
            catch (ExceptionClient except)
            {
                throw new ExceptionClient(except);
            }
            catch (Exception error)
            {
                throw new ExceptionClient(error.Message);
            }
        }

        public virtual void Change(object obj)
        {
            try
            {
                if (!ValidConnection())
                {
                    throw new ExceptionClient(EnumClientError.NoConnection);
                }
                var record = obj as AbstractCommonTable;
                if (record == null)
                    throw new InvalidOperationException("Record is null!");
                var message = new ChangeRecordMessage() { Table = Name.ConvertToProto(), Operation = EnumChangeRecordOperation.Change, Record = record.ConvertToProto(Name) };

                var result = ClientServiceDB?.ChangeRecord(message, CreateMetadata());
            }
            catch (ExceptionClient except)
            {
                throw new ExceptionClient(except);
            }
            catch (Exception error)
            {
                throw new ExceptionClient(error.Message);
            }
        }

        public virtual async Task ChangeAsync(object obj)
        {
            try
            {
                if (!ValidConnection())
                {
                    throw new ExceptionClient(EnumClientError.NoConnection);
                }
                var record = obj as AbstractCommonTable;
                if (record == null)
                    throw new InvalidOperationException("Record is null!");
                var message = new ChangeRecordMessage() { Table = Name.ConvertToProto(), Operation = EnumChangeRecordOperation.Change, Record = record.ConvertToProto(Name) };
                if (ClientServiceDB != null)
                {
                    var result = await ClientServiceDB.ChangeRecordAsync(message, this.CreateMetadata());
                }

                return;
            }
            catch (ExceptionClient except)
            {
                throw new ExceptionClient(except);
            }
            catch (Exception error)
            {
                throw new ExceptionClient(error.Message);
            }
        }

        public virtual void ChangeRange(object rangeObj)
        {
            try
            {
                if (!ValidConnection())
                {
                    throw new ExceptionClient(EnumClientError.NoConnection);
                }
                if (rangeObj is List<T>)
                {
                    ClassDataCommon data = ClassDataCommon.ConvertToListAbstractCommonTable(rangeObj as List<T>) ?? throw new InvalidOperationException("Data is null!");
                    var message = new ChangeRangeMessage() { Table = Name.ConvertToProto(), Operation = EnumChangeRecordOperation.Change };
                    message.Records.AddRange(data.ConvertToProto(Name));
                    var result = ClientServiceDB?.ChangeRangeAsync(message, this.CreateMetadata());
                    return;
                }
                throw new ExceptionClient("ChangeRange: Mismatch with expected type!");
            }
            catch (ExceptionClient except)
            {
                throw new ExceptionClient(except);
            }
            catch (Exception error)
            {
                throw new ExceptionClient(error.Message);
            }
        }


        public virtual async Task ChangeRangeAsync(object rangeObj)
        {
            try
            {
                if (!ValidConnection())
                {
                    throw new ExceptionClient(EnumClientError.NoConnection);
                }
                if (rangeObj is List<T>)
                {
                    ClassDataCommon data = ClassDataCommon.ConvertToListAbstractCommonTable(rangeObj as List<T>) ?? throw new InvalidOperationException("Data is null!");
                    var message = new ChangeRangeMessage() { Table = Name.ConvertToProto(), Operation = EnumChangeRecordOperation.Change };
                    message.Records.AddRange(data.ConvertToProto(Name));
                    if (ClientServiceDB != null)
                    {
                        var result = await ClientServiceDB.ChangeRangeAsync(message, this.CreateMetadata());
                    }

                    return;
                }
                throw new ExceptionClient("ChangeRange: Mismatch with expected type!");
            }
            catch (ExceptionClient except)
            {
                throw new ExceptionClient(except);
            }
            catch (Exception error)
            {
                throw new ExceptionClient(error.Message);
            }
        }

        public virtual void Clear()
        {
            try
            {
                if (!ValidConnection())
                {
                    throw new ExceptionClient(EnumClientError.NoConnection);
                }
                var message = new NameTableMessage() { Table = Name.ConvertToProto()};
                var result = ClientServiceDB?.ClearTable(message, CreateMetadata());
                return;
            }
            catch (ExceptionClient except)
            {
                throw new ExceptionClient(except);
            }
            catch (Exception error)
            {
                throw new ExceptionClient(error.Message);
            }
        }

        public virtual async Task CLearAsync()
        {
            try
            {
                if (!ValidConnection())
                {
                    throw new ExceptionClient(EnumClientError.NoConnection);
                }
                var message = new NameTableMessage() { Table = Name.ConvertToProto() };
                if (ClientServiceDB != null)
                {
                    var result = await ClientServiceDB.ClearTableAsync(message, this.CreateMetadata());
                }

                return;
            }
            catch (ExceptionClient except)
            {
                throw new ExceptionClient(except);
            }
            catch (Exception error)
            {
                throw new ExceptionClient(error.Message);
            }
        }

        public virtual void Delete(object obj)
        {
            try
            {
                if (!ValidConnection())
                {
                    throw new ExceptionClient(EnumClientError.NoConnection);
                }
                var record = obj as AbstractCommonTable;
                if (record == null)
                    throw new InvalidOperationException("Record is null!");
                var message = new ChangeRecordMessage() { Table = Name.ConvertToProto(), Operation = EnumChangeRecordOperation.Delete, Record = record.ConvertToProto(Name) };
                var result = ClientServiceDB?.ChangeRecord(message, CreateMetadata());
                return;
            }
            catch (ExceptionClient except)
            {
                throw new ExceptionClient(except);
            }
            catch (Exception error)
            {
                throw new ExceptionClient(error.Message);
            }
        }

        public virtual async Task DeleteAsync(object obj)
        {
            try
            {
                if (!ValidConnection())
                {
                    throw new ExceptionClient(EnumClientError.NoConnection);
                }
                var record = obj as AbstractCommonTable;
                if (record == null)
                    throw new InvalidOperationException("Record is null!");
                var message = new ChangeRecordMessage() { Table = Name.ConvertToProto(), Operation = EnumChangeRecordOperation.Delete, Record = record.ConvertToProto(Name) };
                if (ClientServiceDB != null)
                {
                    var result = await ClientServiceDB.ChangeRecordAsync(message, this.CreateMetadata());
                }

                return;
            }
            catch (ExceptionClient except)
            {
                throw new ExceptionClient(except);
            }
            catch (Exception error)
            {
                throw new ExceptionClient(error.Message);
            }
        }

        public virtual List<V> Load<V>() where V : AbstractCommonTable
        {
            try
            {
                if (!ValidConnection())
                {
                    throw new ExceptionClient(EnumClientError.NoConnection);
                }
                var message = new NameTableMessage() { Table = Name.ConvertToProto()};
                var Data = ClientServiceDB?.LoadTable(message, CreateMetadata()).Records.ConvertToDBModel(Name);

                if(Data != null)
                    return Data.ToList<V>();
                else 
                    throw new InvalidOperationException("List is null!");
            }
            catch (ExceptionClient except)
            {
                throw new ExceptionClient(except);
            }
            catch (Exception error)
            {
                throw new ExceptionClient(error.Message);
            }
        }

        public virtual async Task<List<V>> LoadAsync<V>() where V : AbstractCommonTable
        {
            try
            {
                if (!ValidConnection())
                {
                    throw new ExceptionClient(EnumClientError.NoConnection);
                }
                var message = new NameTableMessage() { Table = Name.ConvertToProto() };
                if (ClientServiceDB != null)
                {
                    var temp = await ClientServiceDB.LoadTableAsync(message, this.CreateMetadata());
                    var result = temp.Records.ConvertToDBModel(this.Name);
                    return result.ToList<V>();
                }
                else
                    throw new InvalidOperationException("List is null!");
            }
            catch (ExceptionClient except)
            {
                throw new ExceptionClient(except);
            }
            //catch (FaultException<InheritorsException.ExceptionWCF> except)
            //{
            //    throw new ExceptionDatabase(except.Detail);
            //}
            catch (Exception error)
            {
                throw new ExceptionClient(error.Message);
            }
        }





        protected Metadata CreateMetadata()
        {
            return new Metadata() { new Metadata.Entry("clientid", Id.ToString()) };
        }

    }
}
