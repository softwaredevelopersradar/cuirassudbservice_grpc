﻿using System;
using CuirassUDbPackage;

namespace GrpcDbClientLib
{
    internal class ClassTable : IDisposable
    {
        public static CuirassUDbTransmission.CuirassUDbTransmissionClient? ClientServiceDB { get; internal set; } = null;

        public static int Id { get; internal set; } = 0;

        public ClassTable(CuirassUDbTransmission.CuirassUDbTransmissionClient clientServiceDB, int ID)
        {
            if (ClientServiceDB != null)
                return;

            ClientServiceDB = clientServiceDB;
            Id = ID;
        }

        public ClassTable()
        { }

        public void Dispose()
        {
            if (ClientServiceDB == null)
                return;

            //ClientServiceDB.Abort();
            ClientServiceDB = null;
        }
    }
}
