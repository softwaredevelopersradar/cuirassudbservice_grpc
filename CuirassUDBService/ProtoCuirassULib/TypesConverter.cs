﻿namespace ProtoCuirassULib
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using CuirassUDbPackage;

    using CuirassUModelsDBLib;

    using Google.Protobuf;
    using Google.Protobuf.WellKnownTypes;

    using Mapster;

    public static class TypesConverter
    {
        #region Enums

        public static CuirassUModelsDBLib.NameTable ConvertToDBModel(this CuirassUDbPackage.EnumTables args) =>
            (CuirassUModelsDBLib.NameTable)args;
        public static CuirassUDbPackage.EnumTables ConvertToProto(this CuirassUModelsDBLib.NameTable args) =>
            (CuirassUDbPackage.EnumTables)args;


        public static CuirassUModelsDBLib.NameChangeOperation ConvertToDBModel(this CuirassUDbPackage.EnumChangeRecordOperation args) =>
            (CuirassUModelsDBLib.NameChangeOperation)args;
        public static CuirassUDbPackage.EnumChangeRecordOperation ConvertToProto(this CuirassUModelsDBLib.NameChangeOperation args) =>
            (CuirassUDbPackage.EnumChangeRecordOperation)args;

        #endregion



        #region Models

        public static CuirassUModelsDBLib.TableRemotePoints ConvertToDBModel(this CuirassUDbPackage.RemotePointMessage args)
        {
            return args.Adapt<TableRemotePoints>(typeAdapterConfig);
        }
        public static CuirassUDbPackage.RemotePointMessage ConvertToProto(this CuirassUModelsDBLib.TableRemotePoints args)
        {
            return args.Adapt<RemotePointMessage>(typeAdapterConfig);
        }


        public static CuirassUModelsDBLib.Coord ConvertToDBModel(this CuirassUDbPackage.CoordMessage args)
        {
            return args.Adapt<Coord>();
        }
        public static CuirassUDbPackage.CoordMessage ConvertToProto(this CuirassUModelsDBLib.Coord args)
        {
            return args.Adapt<CoordMessage>();
        }


        public static CuirassUModelsDBLib.TableFreqRanges ConvertToDBModel(this CuirassUDbPackage.FreqRangeMessage args)
        {
            return args.Adapt<TableFreqRanges>();
        }
        public static CuirassUDbPackage.FreqRangeMessage ConvertToProto(this CuirassUModelsDBLib.TableFreqRanges args)
        {
            return args.Adapt<FreqRangeMessage>();
        }

        public static CuirassUModelsDBLib.TableFreqForbidden ConvertToDBModel(this CuirassUDbPackage.ForbiddenFreqRangeMessage args)
        {
            return args.Adapt<TableFreqForbidden>(typeAdapterConfig);
        }
        public static CuirassUDbPackage.ForbiddenFreqRangeMessage ConvertToProto(this CuirassUModelsDBLib.TableFreqForbidden args)
        {
            return args.Adapt<ForbiddenFreqRangeMessage>(typeAdapterConfig);
        }


        public static CuirassUModelsDBLib.TableSectorsRecon ConvertToDBModel(this CuirassUDbPackage.SectorMessage args)
        {
            return args.Adapt<TableSectorsRecon>();
        }
        public static CuirassUDbPackage.SectorMessage ConvertToProto(this CuirassUModelsDBLib.TableSectorsRecon args)
        {
            return args.Adapt<SectorMessage>();
        }

        public static CuirassUModelsDBLib.TableRes ConvertToDBModel(this CuirassUDbPackage.SourceMessage args)
        {
            return args.Adapt<TableRes>(typeAdapterConfig);
        }
        public static CuirassUDbPackage.SourceMessage ConvertToProto(this CuirassUModelsDBLib.TableRes args)
        {
            return args.Adapt<SourceMessage>(typeAdapterConfig);
        }

        public static CuirassUModelsDBLib.TableResArchive ConvertToDBModel(this CuirassUDbPackage.SourceArchiveMessage args)
        {
            return args.Adapt<TableResArchive>(typeAdapterConfig);
        }
        public static CuirassUDbPackage.SourceArchiveMessage ConvertToProto(this CuirassUModelsDBLib.TableResArchive args)
        {
            var final = args.Adapt<SourceArchiveMessage>(typeAdapterConfig);
            if (args.Track != null)
            {
                final.Track.AddRange(args.Track.Select(x => x.Adapt<TrackMessage>()));
            }

            return final;
        }

        public static CuirassUModelsDBLib.TableTrack ConvertToDBModel(this CuirassUDbPackage.TrackMessage args)
        {
            return args.Adapt<TableTrack>(typeAdapterConfig);
        }
        public static CuirassUDbPackage.TrackMessage ConvertToProto(this CuirassUModelsDBLib.TableTrack args)
        {
            return args.Adapt<TrackMessage>(typeAdapterConfig);
        }

        public static CuirassUModelsDBLib.TableResDistribution ConvertToDBModel(this CuirassUDbPackage.SourceDistributionMessage args)
        {
            return args.Adapt<TableResDistribution>(typeAdapterConfig);
        }
        public static CuirassUDbPackage.SourceDistributionMessage ConvertToProto(this CuirassUModelsDBLib.TableResDistribution args)
        {
            var final = args.Adapt<SourceDistributionMessage>(typeAdapterConfig);
            if (args.Track != null)
            {
                final.Track.AddRange(args.Track.Select(ConvertToProto));
            }
            return final;
        }

        public static CuirassUModelsDBLib.TableResPattern ConvertToDBModel(this CuirassUDbPackage.SourcePatternMessage args)
        {
            return args.Adapt<TableResPattern>(typeAdapterConfig);
        }
        public static CuirassUDbPackage.SourcePatternMessage ConvertToProto(this CuirassUModelsDBLib.TableResPattern args)
        {
            return args.Adapt<SourcePatternMessage>(typeAdapterConfig);
        }

        private static readonly TypeAdapterConfig typeAdapterConfig = GetTypeAdapterConfig();

        private static TypeAdapterConfig GetTypeAdapterConfig()
        {
            var config = new TypeAdapterConfig();
            config.NewConfig<Timestamp, DateTime>().MapWith(src => src.ToDateTime());
            config.NewConfig<DateTime, Timestamp>().MapWith(src => DateTime.SpecifyKind(src, DateTimeKind.Utc).ToTimestamp());
            config.NewConfig<Timestamp, DateTime?>().MapWith(src => src.ToDateTime()).IgnoreNullValues(true);

            config.NewConfig<byte[], ByteString>().MapWith(src => ByteString.CopyFrom(src)).IgnoreNullValues(true);
            config.NewConfig<ByteString, byte[]>().MapWith(src => src.ToArray()).IgnoreNullValues(true);
            config.NewConfig<GlobalSettingsMessage, GlobalPropertiesRRS>().IgnoreNullValues(true);
            config.NewConfig<GlobalPropertiesRRS, GlobalSettingsMessage>().IgnoreNullValues(true);
            config.NewConfig<ParamDP, ParamDPMessage>();
            config.NewConfig<ParamDPMessage, ParamDP>();
            config.NewConfig<Location, LocationMessage>();
            config.NewConfig<LocationMessage, Location>();
            config.NewConfig<IQData, IQDataMessage>();
            config.NewConfig<IQDataMessage, IQData>();
            config.NewConfig<Orientation, OrientationMessage>();
            config.NewConfig<OrientationMessage, Orientation>();
            config.NewConfig<Coord, CoordMessage>();
            config.NewConfig<CoordMessage, Coord>();
            config.NewConfig<RadioIntelegence, RadioIntelegenceMessage>();
            config.NewConfig<RadioIntelegenceMessage, RadioIntelegence>();
            config.NewConfig<Calibration, CalibrationMessage>();
            config.NewConfig<CalibrationMessage, Calibration>();
            config.NewConfig<TableTrackArchive, TrackMessage>().IgnoreNullValues(true);
            config.NewConfig<TrackMessage, TableTrackArchive>().IgnoreNullValues(true);
            config.NewConfig<TableTrackDistribution, TrackMessage>();
            config.NewConfig<TrackMessage, TableTrackDistribution>();
            config.NewConfig<TableResPattern, SourcePatternMessage>().IgnoreNullValues(true);
            config.NewConfig<SourcePatternMessage, TableResPattern>().IgnoreNullValues(true);
            config.NewConfig<RemotePointMessage, TableRemotePoints>().IgnoreNullValues(true);
            config.NewConfig<TableRemotePoints, RemotePointMessage>().IgnoreNullValues(true);
            config.NewConfig<SourceMessage, TableRes>().IgnoreNullValues(true);
            config.NewConfig<TableRes, SourceMessage>().IgnoreNullValues(true);
            config.NewConfig<SourceArchiveMessage, TableResArchive>().IgnoreNullValues(true);
            config.NewConfig<TableResArchive, SourceArchiveMessage>().IgnoreNullValues(true);
            config.NewConfig<SourceDistributionMessage, TableResDistribution>().IgnoreNullValues(true);
            config.NewConfig<TableResDistribution, SourceDistributionMessage>().IgnoreNullValues(true);
            return config;
        }

        public static CuirassUModelsDBLib.GlobalPropertiesRRS ConvertToDBModel(this CuirassUDbPackage.GlobalSettingsRRSMessage args)
        {
            return args.Adapt<GlobalPropertiesRRS>(typeAdapterConfig);
        }
        public static CuirassUDbPackage.GlobalSettingsRRSMessage ConvertToProto(this CuirassUModelsDBLib.GlobalPropertiesRRS args)
        {
            return args.Adapt<GlobalSettingsRRSMessage>(typeAdapterConfig);
        }

        public static CuirassUModelsDBLib.Location ConvertToDBModel(this CuirassUDbPackage.LocationMessage args)
        {
            return args.Adapt<Location>(typeAdapterConfig);
        }
        public static CuirassUDbPackage.LocationMessage ConvertToProto(this CuirassUModelsDBLib.Location args)
        {
            return args.Adapt<LocationMessage>(typeAdapterConfig);
        }

        public static CuirassUModelsDBLib.GlobalProperties ConvertToDBModel(this CuirassUDbPackage.GlobalSettingsMessage args)
        {
            return args.Adapt<GlobalProperties>(typeAdapterConfig);
        }
        public static CuirassUDbPackage.GlobalSettingsMessage ConvertToProto(this CuirassUModelsDBLib.GlobalProperties args)
        {
            return args.Adapt<GlobalSettingsMessage>(typeAdapterConfig);
        }

        public static CuirassUModelsDBLib.Calibration ConvertToDBModel(this CuirassUDbPackage.CalibrationMessage args)
        {
            return args.Adapt<Calibration>(typeAdapterConfig);
        }
        public static CuirassUDbPackage.CalibrationMessage ConvertToProto(this CuirassUModelsDBLib.Calibration args)
        {
            return args.Adapt<CalibrationMessage>(typeAdapterConfig);
        }

        public static CuirassUModelsDBLib.IQData ConvertToDBModel(this CuirassUDbPackage.IQDataMessage args)
        {
            return args.Adapt<IQData>(typeAdapterConfig);
        }
        public static CuirassUDbPackage.IQDataMessage ConvertToProto(this CuirassUModelsDBLib.IQData args)
        {
            return args.Adapt<IQDataMessage>(typeAdapterConfig);
        }

        public static CuirassUModelsDBLib.Orientation ConvertToDBModel(this CuirassUDbPackage.OrientationMessage args)
        {
            return args.Adapt<Orientation>(typeAdapterConfig);
        }
        public static CuirassUDbPackage.OrientationMessage ConvertToProto(this CuirassUModelsDBLib.Orientation args)
        {
            return args.Adapt<OrientationMessage>(typeAdapterConfig);
        }

        public static CuirassUModelsDBLib.RadioIntelegence ConvertToDBModel(this CuirassUDbPackage.RadioIntelegenceMessage args)
        {
            return args.Adapt<RadioIntelegence>(typeAdapterConfig);
        }
        public static CuirassUDbPackage.RadioIntelegenceMessage ConvertToProto(this CuirassUModelsDBLib.RadioIntelegence args)
        {
            return args.Adapt<RadioIntelegenceMessage>(typeAdapterConfig);
        }
        #endregion




        #region General

        public static CuirassUModelsDBLib.AbstractCommonTable ConvertToDBModel(this Google.Protobuf.WellKnownTypes.Any args, NameTable nameTable)
        {
            switch (nameTable)
            {
                case NameTable.TableRemotePoints:
                    return ConvertToDBModel(args.Unpack<CuirassUDbPackage.RemotePointMessage>());
                case NameTable.TableFreqForbidden:
                    return ConvertToDBModel(args.Unpack<CuirassUDbPackage.ForbiddenFreqRangeMessage>());
                case NameTable.TableFreqRangesRecon:
                    return ConvertToDBModel(args.Unpack<CuirassUDbPackage.FreqRangeMessage>()).ToRangesRecon();
                case NameTable.TableSectorsRecon:
                    return ConvertToDBModel(args.Unpack<CuirassUDbPackage.SectorMessage>());
                case NameTable.GlobalProperties:
                    return ConvertToDBModel(args.Unpack<CuirassUDbPackage.GlobalSettingsMessage>());
                case NameTable.GlobalPropertiesRRS:
                    return ConvertToDBModel(args.Unpack<CuirassUDbPackage.GlobalSettingsRRSMessage>());
                case NameTable.TableRes:
                    return ConvertToDBModel(args.Unpack<CuirassUDbPackage.SourceMessage>());
                case NameTable.TableResDistribution:
                    return ConvertToDBModel(args.Unpack<CuirassUDbPackage.SourceDistributionMessage>());
                case NameTable.TableTrackDistribution:
                    return ConvertToDBModel(args.Unpack<CuirassUDbPackage.TrackMessage>()).ToTrackDistribution();
                case NameTable.TableResArchive:
                    return ConvertToDBModel(args.Unpack<CuirassUDbPackage.SourceArchiveMessage>());
                case NameTable.TableTrackArchive:
                    return ConvertToDBModel(args.Unpack<CuirassUDbPackage.TrackMessage>()).ToTrackArchive();
                case NameTable.TableResPattern:
                    return ConvertToDBModel(args.Unpack<CuirassUDbPackage.SourcePatternMessage>());
                default:
                    throw new InvalidOperationException("Unknown converter!");
            }
        }
        public static Google.Protobuf.WellKnownTypes.Any ConvertToProto(this CuirassUModelsDBLib.AbstractCommonTable args, NameTable nameTable)
        {
            switch (nameTable)
            {
                case NameTable.TableRemotePoints:
                    return Google.Protobuf.WellKnownTypes.Any.Pack(ConvertToProto(args as TableRemotePoints ?? throw new InvalidOperationException()));
                case NameTable.TableFreqForbidden:
                    return Google.Protobuf.WellKnownTypes.Any.Pack(ConvertToProto(args as TableFreqForbidden ?? throw new InvalidOperationException()));
                case NameTable.TableFreqRangesRecon:
                    return Google.Protobuf.WellKnownTypes.Any.Pack(ConvertToProto(args as TableFreqRangesRecon ?? throw new InvalidOperationException()));
                case NameTable.TableSectorsRecon:
                    return Google.Protobuf.WellKnownTypes.Any.Pack(ConvertToProto(args as TableSectorsRecon ?? throw new InvalidOperationException()));
                case NameTable.GlobalProperties:
                    return Google.Protobuf.WellKnownTypes.Any.Pack(ConvertToProto(args as GlobalProperties ?? throw new InvalidOperationException()));
                case NameTable.GlobalPropertiesRRS:
                    return Google.Protobuf.WellKnownTypes.Any.Pack(ConvertToProto(args as GlobalPropertiesRRS ?? throw new InvalidOperationException()));
                case NameTable.TableRes:
                    return Google.Protobuf.WellKnownTypes.Any.Pack(ConvertToProto(args as TableRes ?? throw new InvalidOperationException()));
                case NameTable.TableResDistribution:
                    return Google.Protobuf.WellKnownTypes.Any.Pack(ConvertToProto(args as TableResDistribution ?? throw new InvalidOperationException()));
                case NameTable.TableTrackDistribution:
                    return Google.Protobuf.WellKnownTypes.Any.Pack(ConvertToProto(args as TableTrackDistribution ?? throw new InvalidOperationException()));
                case NameTable.TableResArchive:
                    return Google.Protobuf.WellKnownTypes.Any.Pack(ConvertToProto(args as TableResArchive ?? throw new InvalidOperationException()));
                case NameTable.TableTrackArchive:
                    return Google.Protobuf.WellKnownTypes.Any.Pack(ConvertToProto(args as TableTrackArchive ?? throw new InvalidOperationException()));
                case NameTable.TableResPattern:
                    return Google.Protobuf.WellKnownTypes.Any.Pack(ConvertToProto(args as TableResPattern ?? throw new InvalidOperationException()));
                default:
                    throw new InvalidOperationException("Unknown converter!");
            }
        }



        public static CuirassUModelsDBLib.ClassDataCommon ConvertToDBModel(this Google.Protobuf.Collections.RepeatedField<Google.Protobuf.WellKnownTypes.Any> args, NameTable nameTable)
        {
            var result = new List<AbstractCommonTable>();

            foreach (var t in args)
            {
                result.Add(t.ConvertToDBModel(nameTable));
            }
            return ClassDataCommon.ConvertToListAbstractCommonTable(result) ?? throw new InvalidOperationException();
        }

        public static Google.Protobuf.Collections.RepeatedField<Google.Protobuf.WellKnownTypes.Any> ConvertToProto(this CuirassUModelsDBLib.ClassDataCommon args, NameTable nameTable)
        {
            var result = new Google.Protobuf.Collections.RepeatedField<Google.Protobuf.WellKnownTypes.Any>();

            foreach (var t in args.ListRecords)
            {
                result.Add(t.ConvertToProto(nameTable));
            }
            return result;
        }

        #endregion
    }
}
