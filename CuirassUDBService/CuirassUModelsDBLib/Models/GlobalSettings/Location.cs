﻿namespace CuirassUModelsDBLib
{
    using System.ComponentModel;
    using System.Runtime.CompilerServices;
    using System.Runtime.Serialization;
    using System;

    public class Location : INotifyPropertyChanged, IEquatable<Location>
    {

        #region Model Methods

        public Location() { }
        public Location(CoordinateSystem coordinateSystem, double latitude, double longitude, double altitude, float courseAngle, float compassAngle)
        {
            CoordinateSystem = coordinateSystem;
            Latitude = latitude;
            Longitude = longitude;
            Altitude = altitude;
            CourseAngle = courseAngle;
            CompassAngle= compassAngle;
        }

        public bool EqualTo(Location model)
        {
            return CoordinateSystem == model.CoordinateSystem
                && Longitude == model.Longitude
                && Latitude == model.Latitude
                && Altitude == model.Altitude
                && CourseAngle == model.CourseAngle
                && CompassAngle == model.CompassAngle;
        }

        public Location Clone() =>  new Location(CoordinateSystem, Latitude, Longitude, Altitude, CourseAngle, CompassAngle);

        public void Update(Location model)
        {
            CoordinateSystem = model.CoordinateSystem;
            Longitude = model.Longitude;
            Latitude = model.Latitude;
            Altitude = model.Altitude;
            CourseAngle = model.CourseAngle;
            CompassAngle = model.CompassAngle;
        }

        #endregion


        private CoordinateSystem _coordinateSystem = CoordinateSystem.MGRS;
        private double _latitude = 500;
        private double _longitude = 500;
        private double _altitude = -1;
        private float _courseAngle;
        private float _compassAngle;

        [DataMember]
        [NotifyParentProperty(true)]
        public CoordinateSystem CoordinateSystem
        {
            get => _coordinateSystem;
            set
            {
                if (_coordinateSystem == value) return;
                _coordinateSystem = value;
                OnPropertyChanged();
            }
        }

        [DataMember]
        [NotifyParentProperty(true)]
        public double Latitude
        {
            get => _latitude;
            set
            {
                if (_latitude == value) return;
                _latitude = value;
                OnPropertyChanged();
            }
        }

        [DataMember]
        [NotifyParentProperty(true)]
        public double Longitude
        {
            get => _longitude;
            set
            {
                if (_longitude == value) return;
                _longitude = value;
                OnPropertyChanged();
            }
        }

        [DataMember]
        [NotifyParentProperty(true)]
        public double Altitude
        {
            get => _altitude;
            set
            {
                if (_altitude == value) return;
                _altitude = value;
                OnPropertyChanged();
            }
        }

        [DataMember]
        [NotifyParentProperty(true)]
        public float CourseAngle
        {
            get => _courseAngle;
            set
            {
                if (_courseAngle == value) return;
                _courseAngle = value;
                OnPropertyChanged();
            }
        }

        [DataMember]
        [NotifyParentProperty(true)]
        public float CompassAngle
        {
            get => _compassAngle;
            set
            {
                if (_compassAngle == value) return;
                _compassAngle = value;
                OnPropertyChanged();
            }
        }

        #region NotifyPropertyChanged
        public event PropertyChangedEventHandler? PropertyChanged;

        public void OnPropertyChanged([CallerMemberName] string prop = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
        }
        #endregion

        public bool Equals(Location? other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return _coordinateSystem == other._coordinateSystem && _latitude.Equals(other._latitude) && _longitude.Equals(other._longitude) && _altitude.Equals(other._altitude) && _courseAngle.Equals(other._courseAngle) && _compassAngle.Equals(other._compassAngle);
        }

        public override bool Equals(object? obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((Location)obj);
        }

        public override int GetHashCode()
        {
            return HashCode.Combine((int)_coordinateSystem, _latitude, _longitude, _altitude, _courseAngle, _compassAngle);
        }
    }
}
