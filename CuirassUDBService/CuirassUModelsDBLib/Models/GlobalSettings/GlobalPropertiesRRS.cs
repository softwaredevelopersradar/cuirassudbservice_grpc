﻿namespace CuirassUModelsDBLib
{
    using System.ComponentModel;
    using System.Runtime.CompilerServices;
    using System.Runtime.Serialization;
    using System;

    [DataContract]
    [KnownType(typeof(AbstractCommonTable))]
    [InfoTable(NameTable.GlobalPropertiesRRS)]
    public class GlobalPropertiesRRS : AbstractCommonTable, INotifyPropertyChanged, IEquatable<GlobalPropertiesRRS>
    {
        private Location _location = new Location();


        [DataMember]
        [Browsable(false)]
        public override int Id { get; set; }

        [DataMember]
        [Category("Location")]
        [TypeConverter(typeof(ExpandableObjectConverter))]
        public Location Location 
        {
            get => _location;
            set
            {
                if (Equals(value, this._location)) return;
                this.Location.PropertyChanged -= Property_PropertyChanged;
                _location = value;
                this.Location.PropertyChanged += Property_PropertyChanged;
                OnPropertyChanged();
            }
        }




        #region IModelMethods

        public GlobalPropertiesRRS Clone()
        {
            return new GlobalPropertiesRRS
            {
                Id = Id,
                Location = Location.Clone()
            };
        }

        public bool EqualTo(GlobalPropertiesRRS model)
        {
            return Location.EqualTo(model.Location);
        }

        public override void Update(AbstractCommonTable model)
        {
            var newModel = (GlobalPropertiesRRS)model;
            
            Location.Update(newModel.Location);
        }



        #endregion


        //#region NotifyPropertyChanged

        //public event PropertyChangedEventHandler? PropertyChanged;

        //public void OnPropertyChanged([CallerMemberName]string prop = "")
        //{
        //    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
        //}

        //#endregion

        public bool Equals(GlobalPropertiesRRS? other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return _location.Equals(other._location) && Id == other.Id;
        }

        public override bool Equals(object? obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((GlobalPropertiesRRS)obj);
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(_location, Id);
        }

        private void Property_PropertyChanged(object? sender, PropertyChangedEventArgs e)
        {
            if (object.ReferenceEquals(sender, Location))
            {
                this.OnPropertyChanged(nameof(this.Location));
            }
        }
    }
}
