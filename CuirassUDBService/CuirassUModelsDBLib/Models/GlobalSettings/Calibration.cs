﻿namespace CuirassUModelsDBLib
{
    using System;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;
    using System.Runtime.Serialization;

    [DataContract]
    public class Calibration : INotifyPropertyChanged, IEquatable<Calibration>
    {
        #region Model Methods

        public bool EqualTo(Calibration model)
        {
            return Type == model.Type;
        }

        public Calibration Clone()
        {
            return new Calibration
            {
                Type = Type
            };
        }

        public void Update(Calibration model)
        {
            Type = model.Type;
        }

        #endregion

        private CalibrationTypes _type = CalibrationTypes.Generator;

        [DataMember]
        [NotifyParentProperty(true)]
        public CalibrationTypes Type
        {
            get => _type;
            set
            {
                if (_type == value) return;
                _type = value;
                OnPropertyChanged();
            }
        }



        #region NotifyPropertyChanged

        public event PropertyChangedEventHandler? PropertyChanged;

        public void OnPropertyChanged([CallerMemberName]string prop = "")
        {
            try
            {
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
            }
            catch { }
        }

        #endregion


        public bool Equals(Calibration? other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return _type == other._type;
        }

        public override bool Equals(object? obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((Calibration)obj);
        }

        public override int GetHashCode()
        {
            return (int)_type;
        }
    }
}
