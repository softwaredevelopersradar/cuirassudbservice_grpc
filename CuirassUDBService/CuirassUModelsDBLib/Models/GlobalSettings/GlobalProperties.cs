﻿namespace CuirassUModelsDBLib
{
    using System.ComponentModel;
    using System.Runtime.Serialization;
    using System;

    [DataContract]
    [KnownType(typeof(AbstractCommonTable))]
    [InfoTable(NameTable.GlobalProperties)]
    public class GlobalProperties : AbstractCommonTable, IEquatable<GlobalProperties>
    {
        private Coord _location = new Coord();
        private Calibration _calibration = new Calibration();
        private RadioIntelegence _radioIntelegence = new RadioIntelegence();
        private IQData _iqData = new IQData();
        private Orientation _orientation = new Orientation();


        [DataMember]
        [Browsable(false)]
        public override int Id { get; set; }

        [DataMember]
        [Category(nameof(RadioIntelegence))]
        [TypeConverter(typeof(ExpandableObjectConverter))]
        public RadioIntelegence RadioIntelegence
        {
            get => _radioIntelegence;
            set
            {
                if (Equals(value, this._radioIntelegence)) return;
                this.RadioIntelegence.PropertyChanged -= Property_PropertyChanged;
                _radioIntelegence = value;
                this.RadioIntelegence.PropertyChanged += Property_PropertyChanged;
                OnPropertyChanged();
            }
        }

        
        [DataMember]
        [Category("Calibration")]
        [TypeConverter(typeof(ExpandableObjectConverter))]
        public Calibration Calibration
        {
            get => _calibration;
            set
            {
                if (Equals(value, this._calibration)) return;
                this.Calibration.PropertyChanged -= Property_PropertyChanged;
                _calibration = value; 
                this.Calibration.PropertyChanged += Property_PropertyChanged;
                OnPropertyChanged();
            }
        }
        

        [DataMember]
        [Category("Location")]
        [TypeConverter(typeof(ExpandableObjectConverter))]
        public Coord Location
        {
            get => _location;
            set
            {
                if (Equals(value, this._location)) return;
                this.Location.PropertyChanged -= Property_PropertyChanged;
                _location = value;
                this.Location.PropertyChanged += Property_PropertyChanged;
                OnPropertyChanged();
            }
        }
       

        [DataMember]
        [Category("Location")]
        [TypeConverter(typeof(ExpandableObjectConverter))]
        public Orientation CompassOrientation
        {
            get => this._orientation;
            set
            {
                if (Equals(value, this._orientation)) return;
                this.CompassOrientation.PropertyChanged -= Property_PropertyChanged;
                _orientation = value; 
                this.CompassOrientation.PropertyChanged += Property_PropertyChanged;
                OnPropertyChanged();
            }
        }
        

        [DataMember]
        [Category("IQData")]
        [TypeConverter(typeof(ExpandableObjectConverter))]
        public IQData IQData
        {
            get => _iqData;
            set
            {
                if (Equals(value, this._iqData)) return;
                this.IQData.PropertyChanged -= Property_PropertyChanged;
                _iqData = value;
                this.IQData.PropertyChanged += Property_PropertyChanged;
                OnPropertyChanged();
            }
        }

        

        #region IModelMethods

        public GlobalProperties Clone()
        {
            return new GlobalProperties
               {
                   Id = Id,
                   RadioIntelegence = RadioIntelegence.Clone(),
                   Calibration = Calibration.Clone(),
                   Location = Location.Clone(),
                   IQData = IQData.Clone(),
                   CompassOrientation = CompassOrientation.Clone()
               };
        }

        public bool EqualTo(GlobalProperties model)
        {
            return RadioIntelegence.EqualTo(model.RadioIntelegence)
                   && Calibration.EqualTo(model.Calibration)
                   && Location.EqualTo(model.Location)
                   && IQData.EqualTo(model.IQData)
                   && CompassOrientation.EqualTo(model.CompassOrientation);
        }

        public override void Update(AbstractCommonTable model)
        {
            var newModel = (GlobalProperties)model;
            RadioIntelegence.Update(newModel.RadioIntelegence);
            Location.Update(newModel.Location);
            Calibration.Update(newModel.Calibration);
            IQData.Update(newModel.IQData);
            CompassOrientation.Update(newModel.CompassOrientation);
        }

       
        #endregion


        //#region NotifyPropertyChanged

        //public event PropertyChangedEventHandler? PropertyChanged;

        //public void OnPropertyChanged([CallerMemberName] string prop = "")
        //{
        //    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
        //}

        //#endregion
        
        public bool Equals(GlobalProperties? other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return _location.Equals(other._location) && _calibration.Equals(other._calibration) && _radioIntelegence.Equals(other._radioIntelegence) && _iqData.Equals(other._iqData) && _orientation.Equals(other._orientation) && Id == other.Id;
        }

        public override bool Equals(object? obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((GlobalProperties)obj);
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(_location, _calibration, _radioIntelegence, _iqData, _orientation, Id);
        }


        private void Property_PropertyChanged(object? sender, PropertyChangedEventArgs e)
        {
            if (object.ReferenceEquals(sender, Calibration))
            {
                this.OnPropertyChanged(nameof(this.Calibration));
            }

            if (object.ReferenceEquals(sender, IQData))
            {
                this.OnPropertyChanged(nameof(this.IQData));
            }

            if (object.ReferenceEquals(sender, RadioIntelegence))
            {
                this.OnPropertyChanged(nameof(this.RadioIntelegence));
            }

            if (object.ReferenceEquals(sender, Location))
            {
                this.OnPropertyChanged(nameof(this.Location));
            }

            if (object.ReferenceEquals(sender, CompassOrientation))
            {
                this.OnPropertyChanged(nameof(this.CompassOrientation));
            }
        }
    }
}
