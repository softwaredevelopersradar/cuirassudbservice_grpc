﻿namespace CuirassUModelsDBLib
{
    using System.ComponentModel;
    using System.Runtime.CompilerServices;
    using System.Runtime.Serialization;
    using System;

    [DataContract]
    public class RadioIntelegence : INotifyPropertyChanged, IEquatable<RadioIntelegence>
    {
        #region IModelMethods

        public RadioIntelegence() { }
        public RadioIntelegence(float courseAngle, SynchronizeTypes synchronizeType, NarrowFilterWidth narrowFilterWidth, 
            float durationMin, float durationMax, float periodMin, float periodMax, short switchingTime, short reviewsCount, DeviceAnalysisTypes deviceAnalysisTypes, bool autoCourseAngle)
        {
            CourseAngle = courseAngle;
            SynchronizeType = synchronizeType;
            DurationMin = durationMin;
            DurationMax = durationMax;
            PeriodMin = periodMin;
            PeriodMax = periodMax;
            SwitchingTime = switchingTime;
            NumberOfReviews = reviewsCount;
            this.NarrowFilterWidth = narrowFilterWidth;
            DeviceAnalysisType = deviceAnalysisTypes;
            AutoCourseAngle = autoCourseAngle;
        }

        public RadioIntelegence Clone() => new RadioIntelegence
            (CourseAngle, SynchronizeType, this.NarrowFilterWidth, DurationMin, DurationMax, PeriodMin, PeriodMax, SwitchingTime, NumberOfReviews, this.DeviceAnalysisType, this.AutoCourseAngle);
        

        public bool EqualTo(RadioIntelegence model)
        {
            return CourseAngle == model.CourseAngle
                && SynchronizeType == model.SynchronizeType
                && this.NarrowFilterWidth == model.NarrowFilterWidth
                && DurationMin == model.DurationMin
                && DurationMax == model.DurationMax
                && PeriodMin == model.PeriodMin
                && PeriodMax == model.PeriodMax
                && NumberOfReviews == model.NumberOfReviews
                &&SwitchingTime == model.SwitchingTime
                && this.DeviceAnalysisType == model.DeviceAnalysisType
                && AutoCourseAngle == model.AutoCourseAngle;
        }

        public void Update(RadioIntelegence model)
        {
            CourseAngle = model.CourseAngle;
            SynchronizeType = model.SynchronizeType;
            this.NarrowFilterWidth = model.NarrowFilterWidth;
            DurationMin = model.DurationMin;
            DurationMax = model.DurationMax;
            PeriodMin = model.PeriodMin;
            PeriodMax = model.PeriodMax;
            SwitchingTime = model.SwitchingTime;
            NumberOfReviews = model.NumberOfReviews;
            this.DeviceAnalysisType = model.DeviceAnalysisType;
            AutoCourseAngle = model.AutoCourseAngle;
        }

        
        #endregion



        private float _courseAngle;
        private SynchronizeTypes _synchronizeType = SynchronizeTypes.GPS;
        private float _durationMin = 2000;
        private float _durationMax = 2000;
        private float _periodMin = 10000;
        private float _periodMax = 10000;
        private short _switchingTime = 100;
        private short _numberOfReviews = 10;
        private NarrowFilterWidth _narrowFilterWidth = NarrowFilterWidth.FiftyMHz;
        private DeviceAnalysisTypes deviceAnalysisType = DeviceAnalysisTypes.Master;
        private bool _autoCourseAngle;

        [DataMember]
        [NotifyParentProperty(true)]
        public float CourseAngle
        {
            get => _courseAngle;
            set
            {
                if (_courseAngle == value) return;
                _courseAngle = value;
                OnPropertyChanged();
            }
        }


        [DataMember]
        [NotifyParentProperty(true)]
        public SynchronizeTypes SynchronizeType
        {
            get => _synchronizeType;
            set
            {
                if (_synchronizeType == value) return;
                _synchronizeType = value;
                OnPropertyChanged();
            }
        }


        [DataMember]
        [NotifyParentProperty(true)]
        public float DurationMin
        {
            get => _durationMin;
            set
            {
                if (_durationMin == value) return;
                _durationMin = value;
                OnPropertyChanged();
            }
        }


        [DataMember]
        [NotifyParentProperty(true)]
        public float DurationMax
        {
            get => _durationMax;
            set
            {
                if (_durationMax == value) return;
                _durationMax = value;
                OnPropertyChanged();
            }
        }


        [DataMember]
        [NotifyParentProperty(true)]
        public float PeriodMin
        {
            get => _periodMin;
            set
            {
                if (_periodMin == value) return;
                _periodMin = value;
                OnPropertyChanged();
            }
        }

        [DataMember]
        [NotifyParentProperty(true)]
        public float PeriodMax
        {
            get => _periodMax;
            set
            {
                if (_periodMax == value) return;
                _periodMax = value;
                OnPropertyChanged();
            }
        }

        [DataMember]
        [NotifyParentProperty(true)]
        public short SwitchingTime
        {
            get => _switchingTime;
            set
            {
                if (_switchingTime == value) return;
                _switchingTime = value;
                OnPropertyChanged();
            }
        }


        [DataMember]
        [NotifyParentProperty(true)]
        public short NumberOfReviews
        {
            get => _numberOfReviews;
            set
            {
                if (_numberOfReviews == value) return;
                _numberOfReviews = value;
                OnPropertyChanged();
            }
        }

        [DataMember]
        [NotifyParentProperty(true)]
        public NarrowFilterWidth NarrowFilterWidth
        {
            get => this._narrowFilterWidth;
            set
            {
                if (this._narrowFilterWidth == value) return;
                this._narrowFilterWidth = value;
                OnPropertyChanged();
            }
        }

        [DataMember]
        [NotifyParentProperty(true)]
        public DeviceAnalysisTypes DeviceAnalysisType
        {
            get => this.deviceAnalysisType;
            set
            {
                if(this.deviceAnalysisType == value) return;
                this.deviceAnalysisType = value;
                this.OnPropertyChanged();
            }
        }

        [DataMember]
        [NotifyParentProperty(true)]
        public bool AutoCourseAngle
        {
            get => _autoCourseAngle;
            set
            {
                if (_autoCourseAngle == value) return;
                _autoCourseAngle = value;
                OnPropertyChanged();
            }
        }

        #region NotifyPropertyChanged
        public event PropertyChangedEventHandler? PropertyChanged;

        public void OnPropertyChanged([CallerMemberName]string prop = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
        }
        #endregion


        public bool Equals(RadioIntelegence? other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return _courseAngle.Equals(other._courseAngle) && _synchronizeType == other._synchronizeType && _durationMin.Equals(other._durationMin) && _durationMax.Equals(other._durationMax) && _periodMin.Equals(other._periodMin) && _periodMax.Equals(other._periodMax) && _switchingTime == other._switchingTime && _numberOfReviews == other._numberOfReviews && _narrowFilterWidth == other._narrowFilterWidth && deviceAnalysisType == other.deviceAnalysisType && _autoCourseAngle == other._autoCourseAngle;
        }

        public override bool Equals(object? obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((RadioIntelegence)obj);
        }

        public override int GetHashCode()
        {
            var hashCode = new HashCode();
            hashCode.Add(_courseAngle);
            hashCode.Add((int)_synchronizeType);
            hashCode.Add(_durationMin);
            hashCode.Add(_durationMax);
            hashCode.Add(_periodMin);
            hashCode.Add(_periodMax);
            hashCode.Add(_switchingTime);
            hashCode.Add(_numberOfReviews);
            hashCode.Add((int)_narrowFilterWidth);
            hashCode.Add((int)deviceAnalysisType);
            hashCode.Add(_autoCourseAngle);
            return hashCode.ToHashCode();
        }
    }
}
