﻿namespace CuirassUModelsDBLib
{
    using System;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;
    using System.Runtime.Serialization;

    [DataContract]
    public class Orientation : INotifyPropertyChanged, IEquatable<Orientation>
    {
        private float? _azimuth = -1;
        private float? _bank = -1;
        private float? _pitch = -1;

        [DataMember]
        [NotifyParentProperty(true)]
        public float? Azimuth
        {
            get => _azimuth;
            set
            {
                if (_azimuth == value) return;
                _azimuth = value;
                OnPropertyChanged();
            }
        }

        [DataMember]
        [NotifyParentProperty(true)]
        public float? Bank
        {
            get => _bank;
            set
            {
                if (_bank == value) return;
                _bank = value;
                OnPropertyChanged();
            }
        }

        [DataMember]
        [NotifyParentProperty(true)]
        public float? Pitch
        {
            get => _pitch;
            set
            {
                if (_pitch == value) return;
                _pitch = value;
                OnPropertyChanged();
            }
        }

        #region Model Methods

        public Orientation() { }
        public Orientation(float? azimuth, float? bank, float? pitch)
        {
            Bank = bank;
            Pitch = pitch;
            Azimuth = azimuth;
        }

        public bool EqualTo(Orientation model)
        {
            return Azimuth == model.Azimuth
                   && Bank == model.Bank
                   && Pitch == model.Pitch;
        }

        public Orientation Clone() => new Orientation(Azimuth, Bank, Pitch);

        public void Update(Orientation model)
        {
            Bank = model.Bank;
            Pitch = model.Pitch;
            Azimuth = model.Azimuth;
        }

        #endregion


        #region NotifyPropertyChanged
        public event PropertyChangedEventHandler? PropertyChanged;

        public void OnPropertyChanged([CallerMemberName] string prop = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
        }
        #endregion


        public bool Equals(Orientation? other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return Nullable.Equals(_azimuth, other._azimuth) && Nullable.Equals(_bank, other._bank) && Nullable.Equals(_pitch, other._pitch);
        }

        public override bool Equals(object? obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((Orientation)obj);
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(_azimuth, _bank, _pitch);
        }
    }
}
