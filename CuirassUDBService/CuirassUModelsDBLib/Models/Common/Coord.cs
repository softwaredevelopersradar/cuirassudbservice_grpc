﻿namespace CuirassUModelsDBLib
{
    using System.ComponentModel;
    using System.Runtime.CompilerServices;
    using System.Runtime.Serialization;
    using System;

    /// <summary>
    /// Координаты
    /// </summary>
    [DataContract]
    public class Coord : INotifyPropertyChanged, IEquatable<Coord>
    {
        #region NotifyPropertyChanged

        public event PropertyChangedEventHandler? PropertyChanged;

        public void OnPropertyChanged([CallerMemberName]string prop = "")
        {
            try
            {
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
            }
            catch { }
        }

        #endregion

        #region Private

        private double? latitude;
        private double? longitude;
        private double? altitude;

        #endregion

        [DataMember]
        [DisplayName(nameof(Latitude))]
        [NotifyParentProperty(true)]
        [Category(nameof(Coord))]
        public double? Latitude
        {
            get { return latitude; }
            set
            {
                if (latitude == value)
                    return;
                latitude = value;
                OnPropertyChanged(nameof(Latitude));
            }
        }

        [DataMember]
        [DisplayName(nameof(Longitude))]
        [NotifyParentProperty(true)]
        [Category(nameof(Coord))]
        public double? Longitude
        {
            get { return longitude; }
            set
            {
                if (longitude == value)
                    return;
                longitude = value;
                OnPropertyChanged(nameof(Longitude));
            }
        }

        [DataMember]
        [DisplayName(nameof(Altitude))]
        [NotifyParentProperty(true)]
        [Category(nameof(Coord))]
        public double? Altitude
        {
            get { return altitude; }
            set
            {
                if (altitude == value)
                    return;
                altitude = value;
                OnPropertyChanged(nameof(Altitude));
            }
        }


        public Coord() { }
        public Coord(double? latitude, double? longitude, double? altitude)
        {
            Altitude = altitude;
            Latitude = latitude;
            Longitude = longitude;
        }

        public void Update(Coord record)
        {
            Altitude = record.Altitude;
            Latitude = record.Latitude;
            Longitude = record.Longitude;
        }

        public bool EqualTo(Coord record)
        {
            return Latitude == record.Latitude
                   && Longitude == record.Longitude
                   && Altitude == record.Altitude;
        }

        public Coord Clone() => new Coord(Latitude, Longitude, Altitude);

        public override string ToString()
        {
            if ((Latitude < 0 && Longitude < 0 && Altitude < 0) || (Latitude < 0 || Longitude < 0))
                return " ";
            if (Altitude < 0)
                return $"{latitude} : {longitude} : — ";
            return $"{latitude} : {longitude} : {altitude}";
        }
        
        public bool Equals(Coord? other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return Nullable.Equals(latitude, other.latitude) && Nullable.Equals(longitude, other.longitude) && Nullable.Equals(altitude, other.altitude);
        }

        public override bool Equals(object? obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((Coord)obj);
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(latitude, longitude, altitude);
        }
    }
}
