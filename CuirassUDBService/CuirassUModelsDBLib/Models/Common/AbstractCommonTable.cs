﻿namespace CuirassUModelsDBLib
{
    using System.Runtime.Serialization;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;
    //using Annotations;

    [DataContract]
    public abstract class AbstractCommonTable : INotifyPropertyChanged
    {
        [DataMember]
        public abstract int Id { get; set; }

        public object[] GetKey() => new object[] { Id };

        public abstract void Update(AbstractCommonTable record);


        public event PropertyChangedEventHandler? PropertyChanged;

        //[NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string? propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
