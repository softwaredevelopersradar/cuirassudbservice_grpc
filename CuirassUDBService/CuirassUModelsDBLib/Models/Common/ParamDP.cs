﻿namespace CuirassUModelsDBLib
{
    using System;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;
    using System.Runtime.Serialization;

    [DataContract]
    public class ParamDP : INotifyPropertyChanged, IEquatable<ParamDP>
    {
        #region NotifyPropertyChanged

        public event PropertyChangedEventHandler? PropertyChanged;

        public void OnPropertyChanged([CallerMemberName] string prop = "")
        {
            try
            {
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
            }
            catch { }
        }

        #endregion

        #region Private

        private float? duration;
        private float? period;

        #endregion

        [DataMember]
        [DisplayName(nameof(Duration))]
        [NotifyParentProperty(true)]
        [Category(nameof(ParamDP))]
        public float? Duration 
        { 
            get { return duration; }
            set
            {
                if (duration == value)
                    return;
                duration = value;
                OnPropertyChanged(nameof(Duration));
            }
        }

        [DataMember]
        [DisplayName(nameof(Period))]
        [NotifyParentProperty(true)]
        [Category(nameof(ParamDP))]
        public float? Period 
        {
            get { return period; }
            set
            {
                if (period == value)
                    return;
                period = value;
                OnPropertyChanged(nameof(Period));
            }
        }

        public ParamDP() { }

        public ParamDP(float? duration, float? period) 
        {
            Duration = duration;
            Period = period;
        }

        public ParamDP Clone() => new ParamDP(Duration, Period);

        public void Update(ParamDP record)
        {
            Duration = record.Duration;
            Period = record.Period;
        }

        public bool Equals(ParamDP? other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return Nullable.Equals(duration, other.duration) && Nullable.Equals(period, other.period);
        }

        public override bool Equals(object? obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((ParamDP)obj);
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(duration, period);
        }
    }
}
