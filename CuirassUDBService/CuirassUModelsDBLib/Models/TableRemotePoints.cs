﻿namespace CuirassUModelsDBLib
{
    using System.ComponentModel;
    using System.Runtime.Serialization;
    using System;

    /// <summary>
    /// УПП 
    /// </summary>
    [DataContract]
    [KnownType(typeof(AbstractCommonTable))]
    [InfoTable(NameTable.TableRemotePoints)]
    public class TableRemotePoints : AbstractCommonTable, IEquatable<TableRemotePoints>
    {
        private int id;

        private Coord coordinates = new Coord();

        private float? courseAngle;

        private short? timeError;

        private string ipAddress = string.Empty;

        private int port;

        private string? note;

        [DataMember]
        [Category("ID")]
        [DisplayName(nameof(Id))]
        [Browsable(true)]
        public override int Id
        {
            get => this.id;
            set
            {
                if (value == this.id) return;
                this.id = value;
                this.OnPropertyChanged();
            }
        }

        [DataMember]
        [Category(nameof(Coordinates))]
        [DisplayName(" ")]
        [TypeConverter(typeof(ExpandableObjectConverter))]
        public Coord Coordinates
        {
            get => this.coordinates;
            set
            {
                if (Equals(value, this.coordinates)) return;
                this.Coordinates.PropertyChanged -= Coordinates_PropertyChanged;
                this.coordinates = value;
                this.Coordinates.PropertyChanged += Coordinates_PropertyChanged;
                this.OnPropertyChanged();
            }
        }

        [DataMember]
        [Category("Общие")]
        [DisplayName(nameof(CourseAngle))]
        public float? CourseAngle
        {
            get => this.courseAngle;
            set
            {
                if (value.Equals(this.courseAngle)) return;
                this.courseAngle = value;
                this.OnPropertyChanged();
            }
        }

        [DataMember]
        [Category("Общие")]
        [DisplayName(nameof(TimeError))]
        public short? TimeError
        {
            get => this.timeError;
            set
            {
                if (value == this.timeError) return;
                this.timeError = value;
                this.OnPropertyChanged();
            }
        }

        [DataMember]
        [Category("Общие")]
        [DisplayName(nameof(IpAddress))]
        public string IpAddress
        {
            get => this.ipAddress;
            set
            {
                if (value == this.ipAddress) return;
                this.ipAddress = value;
                this.OnPropertyChanged();
            }
        }

        [DataMember]
        [Category("Общие")]
        [DisplayName(nameof(Port))]
        public int Port
        {
            get => this.port;
            set
            {
                if (value == this.port) return;
                this.port = value;
                this.OnPropertyChanged();
            }
        }

        [DataMember]
        [Category("Прочее")]
        [DisplayName(nameof(Note))]
        public string? Note
        {
            get => this.note;
            set
            {
                if (value == this.note) return;
                this.note = value;
                this.OnPropertyChanged();
            }
        }

        public TableRemotePoints()
        {
            if (this.Coordinates != null) this.Coordinates.PropertyChanged += Coordinates_PropertyChanged;
        }

        private void Coordinates_PropertyChanged(object? sender, PropertyChangedEventArgs e)
        {
            this.OnPropertyChanged(nameof(Coordinates));
        }

        public TableRemotePoints(Coord coord, float? courseAngle, short? timeError, string ipAddress, int port, string? note, int id = 0)
        {
            Coordinates = coord.Clone();
            CourseAngle = courseAngle;
            TimeError = timeError;
            IpAddress = ipAddress;
            Port = port;
            Note = note;
            Id = id;
        }

        public override void Update(AbstractCommonTable record)
        {
            var newRecord = (TableRemotePoints)record;

            //if (newRecord.Coordinates == null || Coordinates == null)
            //{
            //    Coordinates = newRecord.Coordinates;
            //}
            //else
            //{
                Coordinates.Update(newRecord.Coordinates);
            //}

            CourseAngle = newRecord.CourseAngle;
            TimeError = newRecord.TimeError;
            IpAddress = newRecord.IpAddress;
            Port = newRecord.Port;
            Note = newRecord.Note;
        }

        public TableRemotePoints Clone() => new TableRemotePoints(Coordinates, CourseAngle, TimeError, IpAddress, Port, Note, Id);

        public bool Equals(TableRemotePoints? other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return this.id == other.id && this.coordinates.Equals(other.coordinates) && Nullable.Equals(this.courseAngle, other.courseAngle) && this.timeError == other.timeError && this.ipAddress == other.ipAddress && this.port == other.port && this.note == other.note;
        }

        public override bool Equals(object? obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((TableRemotePoints)obj);
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(this.id, this.coordinates, this.courseAngle, this.timeError, this.ipAddress, this.port, this.note);
        }
    }
}
