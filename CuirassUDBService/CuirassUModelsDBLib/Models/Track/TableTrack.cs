﻿namespace CuirassUModelsDBLib
{
    using System;
    using System.ComponentModel;
    using System.Runtime.Serialization;

    /// <summary>
    /// Таблица Точки маршрута
    /// </summary>
    [DataContract]
    [KnownType(typeof(TableTrackArchive))]
    [KnownType(typeof(TableTrackDistribution))]
    public class TableTrack : AbstractCommonTable, IEquatable<TableTrack>
    {
        private int id;

        private DateTime time;

        private float tau1;

        private float tau2;

        private float tau3;

        private float tau4;

        private Coord coordinates = new Coord();

        private int tableResId;

        [DataMember]
        public override int Id
        {
            get => this.id;
            set
            {
                if (value == this.id) return;
                this.id = value;
                this.OnPropertyChanged();
            }
        }

        [DataMember]
        public DateTime Time
        {
            get => this.time;
            set
            {
                if (value.Equals(this.time)) return;
                this.time = value;
                this.OnPropertyChanged();
            }
        }

        [DataMember]
        public float Tau1
        {
            get => this.tau1;
            set
            {
                if (value.Equals(this.tau1)) return;
                this.tau1 = value;
                this.OnPropertyChanged();
            }
        }

        [DataMember]
        public float Tau2
        {
            get => this.tau2;
            set
            {
                if (value.Equals(this.tau2)) return;
                this.tau2 = value;
                this.OnPropertyChanged();
            }
        }

        [DataMember]
        public float Tau3
        {
            get => this.tau3;
            set
            {
                if (value.Equals(this.tau3)) return;
                this.tau3 = value;
                this.OnPropertyChanged();
            }
        }

        [DataMember]
        public float Tau4
        {
            get => this.tau4;
            set
            {
                if (value.Equals(this.tau4)) return;
                this.tau4 = value;
                this.OnPropertyChanged();
            }
        }

        [DataMember]
        [TypeConverter(typeof(ExpandableObjectConverter))]
        public Coord Coordinates
        {
            get => this.coordinates;
            set
            {
                if (Equals(value, this.coordinates)) return;
                this.Coordinates.PropertyChanged -= Property_PropertyChanged;
                this.coordinates = value;
                this.Coordinates.PropertyChanged += Property_PropertyChanged;
                this.OnPropertyChanged();
            }
        }

        [DataMember]
        public int TableResId
        {
            get => this.tableResId;
            set
            {
                if (value == this.tableResId) return;
                this.tableResId = value;
                this.OnPropertyChanged();
            }
        }

        public TableTrack() { }
        public TableTrack(int tableResId, DateTime time, float tau1, float tau2, float tau3, float tau4, Coord coord, int id = 0)
        {
            Id = id;
            Time = time;
            Tau1 = tau1;
            Tau2 = tau2;
            Tau3 = tau3;
            Tau4 = tau4;
            Coordinates = coord.Clone();
            TableResId = tableResId;
        }

        public override void Update(AbstractCommonTable record)
        {
            var newRecord = (TableTrack)record;

            Time = newRecord.Time;
            Tau1 = newRecord.Tau1;
            Tau2 = newRecord.Tau2;
            Tau3 = newRecord.Tau3;
            Tau4 = newRecord.Tau4;
            Coordinates.Update(newRecord.Coordinates);
            TableResId = newRecord.TableResId;
        }

        public TableTrack Clone() => new TableTrack(TableResId, Time, Tau1, Tau2, Tau3, Tau4, Coordinates, Id);


        public TableTrackArchive ToTrackArchive()
        {
            return new TableTrackArchive()
            {
                Id = Id,
                Time = Time,
                Tau1 = Tau1,
                Tau2 = Tau2,
                Tau3 = Tau3,
                Tau4 = Tau4,
                Coordinates = Coordinates.Clone(),
                TableResId = TableResId
            };
        }

        public TableTrackDistribution ToTrackDistribution()
        {
            return new TableTrackDistribution()
            {
                Id = Id,
                Time = Time,
                Tau1 = Tau1,
                Tau2 = Tau2,
                Tau3 = Tau3,
                Tau4 = Tau4,
                Coordinates = Coordinates.Clone(),
                TableResId = TableResId
            };
        }

        public bool Equals(TableTrack? other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return id == other.id && time.Equals(other.time) && tau1.Equals(other.tau1) && tau2.Equals(other.tau2) && tau3.Equals(other.tau3) && tau4.Equals(other.tau4) && coordinates.Equals(other.coordinates) && tableResId == other.tableResId;
        }

        public override bool Equals(object? obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((TableTrack)obj);
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(id, time, tau1, tau2, tau3, tau4, coordinates, tableResId);
        }

        private void Property_PropertyChanged(object? sender, PropertyChangedEventArgs e)
        {
            if (object.ReferenceEquals(sender, Coordinates))
            {
                this.OnPropertyChanged(nameof(this.Coordinates));
            }
            
        }
    }
}
