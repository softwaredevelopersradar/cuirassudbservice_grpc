﻿namespace CuirassUModelsDBLib
{
    using System.Runtime.Serialization;

    /// <summary>
    /// Таблица Точки маршрута ИРИ Архив
    /// </summary>
    [DataContract]
    [KnownType(typeof(TableTrack))]
    [InfoTable(NameTable.TableTrackArchive)]
    public class TableTrackArchive : TableTrack
    {
    }
}
