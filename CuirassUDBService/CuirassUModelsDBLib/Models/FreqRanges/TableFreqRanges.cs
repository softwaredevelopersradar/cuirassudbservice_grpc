﻿namespace CuirassUModelsDBLib
{
    using System.Runtime.Serialization;
    using Interfaces;
    using System;
    using System.ComponentModel;

    /// <summary>
    /// Диапазоны частот
    /// </summary>
    [DataContract]
    [KnownType(typeof(TableFreqRangesRecon))]
    [KnownType(typeof(TableFreqForbidden))]

    public class TableFreqRanges : AbstractCommonTable, IFixFreqRanges, IEquatable<TableFreqRanges>
    {
        private int id;

        private double freqMinMHz;

        private double freqMaxMHz;

        private string note = string.Empty;

        private bool isActive;

        [DataMember]
        [Category("ID")]
        [DisplayName(nameof(Id)), Browsable(false)]
        public override int Id
        {
            get => this.id;
            set
            {
                if (value == this.id) return;
                this.id = value;
                this.OnPropertyChanged();
            }
        }

        [DataMember]
        [Category("Диапазон")]
        [DisplayName(nameof(FreqMinMHz))]
        public double FreqMinMHz
        {
            get => this.freqMinMHz;
            set
            {
                if (value.Equals(this.freqMinMHz)) return;
                this.freqMinMHz = value;
                this.OnPropertyChanged();
            }
        }

        [DataMember]
        [Category("Диапазон")]
        [DisplayName(nameof(FreqMaxMHz))]
        public double FreqMaxMHz
        {
            get => this.freqMaxMHz;
            set
            {
                if (value.Equals(this.freqMaxMHz)) return;
                this.freqMaxMHz = value;
                this.OnPropertyChanged();
            }
        }

        [DataMember]
        [Category("Прочее")]
        [DisplayName(nameof(Note))]
        public string Note
        {
            get => this.note;
            set
            {
                if (value == this.note) return;
                this.note = value;
                this.OnPropertyChanged();
            }
        }

        [DataMember]
        [Category("Прочее")]
        [DisplayName(nameof(IsActive)), Browsable(false)]
        public bool IsActive
        {
            get => this.isActive;
            set
            {
                if (value == this.isActive) return;
                this.isActive = value;
                this.OnPropertyChanged();
            }
        }

        public TableFreqRanges Clone()
        {
            return new TableFreqRanges
            {
                Id = this.Id,
                FreqMinMHz = this.FreqMinMHz,
                FreqMaxMHz = this.FreqMaxMHz,
                Note = this.Note,
                IsActive = this.IsActive
            };
        }

        public TableFreqRangesRecon ToRangesRecon()
        {
            TableFreqRangesRecon table = new TableFreqRangesRecon()
            {
                Id = this.Id,
                FreqMaxMHz = this.FreqMaxMHz,
                FreqMinMHz = this.FreqMinMHz,
                Note = this.Note,
                IsActive = this.IsActive
            };
            return table;
        }

        public override void Update(AbstractCommonTable record)
        {
            var newRecord = (TableFreqRanges)record;
            FreqMaxMHz = newRecord.FreqMaxMHz;
            FreqMinMHz = newRecord.FreqMinMHz;
            Note = newRecord.Note;
            IsActive = newRecord.IsActive;
        }

        public bool Equals(TableFreqRanges? other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return id == other.id && freqMinMHz.Equals(other.freqMinMHz) && freqMaxMHz.Equals(other.freqMaxMHz) && note == other.note && isActive == other.isActive;
        }

        public override bool Equals(object? obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((TableFreqRanges)obj);
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(id, freqMinMHz, freqMaxMHz, note, isActive);
        }
    }
}
