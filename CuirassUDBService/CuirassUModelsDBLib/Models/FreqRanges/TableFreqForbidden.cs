﻿namespace CuirassUModelsDBLib
{
    using System.ComponentModel;
    using System.Runtime.Serialization;

    /// <summary>
    /// Запрещенные частоты (ЗЧ)
    /// </summary>
    [DataContract]
    [KnownType(typeof(TableFreqRanges))]
    [InfoTable(NameTable.TableFreqForbidden)]

    public class TableFreqForbidden : TableFreqRanges
    {
        private ParamDP pulse = new ParamDP();

        [DataMember]
        [Category("Импульс")]
        [DisplayName(" ")]
        [TypeConverter(typeof(ExpandableObjectConverter))]
        public ParamDP Pulse
        {
            get => this.pulse;
            set
            {
                if (Equals(value, this.pulse)) return;
                this.Pulse.PropertyChanged -= Pulse_PropertyChanged;
                this.pulse = value;
                this.Pulse.PropertyChanged += Pulse_PropertyChanged;
                this.OnPropertyChanged();
            }
        }

        public override void Update(AbstractCommonTable record)
        {
            var newRecord = (TableFreqForbidden)record;
            FreqMaxMHz = newRecord.FreqMaxMHz;
            FreqMinMHz = newRecord.FreqMinMHz;
            Note = newRecord.Note;
            IsActive = newRecord.IsActive;
            Pulse.Update(newRecord.Pulse);
        }

        public new TableFreqForbidden Clone()
        {
            return new TableFreqForbidden
            {
                Id = this.Id,
                FreqMinMHz = this.FreqMinMHz,
                FreqMaxMHz = this.FreqMaxMHz,
                Note = this.Note,
                IsActive = this.IsActive,
                Pulse = this.Pulse.Clone() 
            };
        }

        private void Pulse_PropertyChanged(object? sender, PropertyChangedEventArgs e)
        {
            this.OnPropertyChanged(nameof(this.Pulse));
        }
    }
}
