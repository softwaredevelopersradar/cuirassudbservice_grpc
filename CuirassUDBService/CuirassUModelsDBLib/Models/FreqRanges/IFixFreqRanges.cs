﻿
namespace CuirassUModelsDBLib.Interfaces
{
    public interface IFixFreqRanges
    {
        int Id { get; set; }

        double FreqMinMHz { get; set; }

        double FreqMaxMHz { get; set; }
    }
}
