﻿namespace CuirassUModelsDBLib
{
    using System.Runtime.Serialization;

    /// <summary>
    /// Диапазоны радиоразведки (ДРР)
    /// </summary>
    [DataContract]
    [KnownType(typeof(TableFreqRanges))]
    [InfoTable(NameTable.TableFreqRangesRecon)]

    public class TableFreqRangesRecon : TableFreqRanges
    {
    }
}
