﻿namespace CuirassUModelsDBLib
{
    using System;
    using System.Collections.ObjectModel;
    using System.ComponentModel;
    using System.Runtime.Serialization;

    /// <summary>
    /// ИРИ Архив
    /// </summary>
    [DataContract]
    [KnownType(typeof(AbstractCommonTable))]
    [InfoTable(NameTable.TableResArchive)]
    public class TableResArchive : AbstractCommonTable, IEquatable<TableResArchive>
    {
        private int id;

        private double frequencyMHz;

        private double bandMHz;

        private ParamDP pulse = new ParamDP();

        private ParamDP series = new ParamDP();

        private string? targetType;

        private byte antennaMaster;

        private byte antennaSlave;

        private DateTime? dateTimeStart;

        private DateTime? dateTimeStop;

        private ObservableCollection<TableTrackArchive> track = new();

        private string? note;

        [DataMember]
        [Category("ID")]
        [DisplayName(nameof(Id))]
        [Browsable(true)]
        public override int Id
        {
            get => this.id;
            set
            {
                if (value == this.id) return;
                this.id = value;
                this.OnPropertyChanged();
            }
        }

        [DataMember]
        [Category("Общие")]
        [DisplayName(nameof(FrequencyMHz))]
        public double FrequencyMHz
        {
            get => this.frequencyMHz;
            set
            {
                if (value.Equals(this.frequencyMHz)) return;
                this.frequencyMHz = value;
                this.OnPropertyChanged();
            }
        }

        [DataMember]
        [Category("Общие")]
        [DisplayName(nameof(BandMHz))]
        public double BandMHz
        {
            get => this.bandMHz;
            set
            {
                if (value.Equals(this.bandMHz)) return;
                this.bandMHz = value;
                this.OnPropertyChanged();
            }
        }

        [DataMember]
        [Category(nameof(ParamDP))]
        [DisplayName(" ")]
        [TypeConverter(typeof(ExpandableObjectConverter))]
        public ParamDP Pulse
        {
            get => this.pulse;
            set
            {
                if (Equals(value, this.pulse)) return;
                this.Pulse.PropertyChanged -= Pulse_PropertyChanged;
                this.pulse = value;
                this.Pulse.PropertyChanged += Pulse_PropertyChanged;
                this.OnPropertyChanged();
            }
        }

        [DataMember]
        [Category(nameof(ParamDP))]
        [DisplayName(" ")]
        [TypeConverter(typeof(ExpandableObjectConverter))]
        public ParamDP Series
        {
            get => this.series;
            set
            {
                if (Equals(value, this.series)) return;
                this.Series.PropertyChanged -= Series_PropertyChanged;
                this.series = value;
                this.Series.PropertyChanged += Series_PropertyChanged;
                this.OnPropertyChanged();
            }
        }

        [DataMember]
        [Category("Общие")]
        [DisplayName(nameof(TargetType))]
        public string? TargetType
        {
            get => this.targetType;
            set
            {
                if (value == this.targetType) return;
                this.targetType = value;
                this.OnPropertyChanged();
            }
        }

        [DataMember]
        [Category("Общие")]
        [DisplayName(nameof(AntennaMaster))]
        public byte AntennaMaster
        {
            get => this.antennaMaster;
            set
            {
                if (value == this.antennaMaster) return;
                this.antennaMaster = value;
                this.OnPropertyChanged();
            }
        }

        [DataMember]
        [Category("Общие")]
        [DisplayName(nameof(AntennaSlave))]
        public byte AntennaSlave
        {
            get => this.antennaSlave;
            set
            {
                if (value == this.antennaSlave) return;
                this.antennaSlave = value;
                this.OnPropertyChanged();
            }
        }

        [DataMember]
        [Category("Общие")]
        [DisplayName(nameof(DateTimeStart))]
        public DateTime? DateTimeStart
        {
            get => this.dateTimeStart;
            set
            {
                if (value.Equals(this.dateTimeStart)) return;
                this.dateTimeStart = value;
                this.OnPropertyChanged();
            }
        }

        [DataMember]
        [Category("Общие")]
        [DisplayName(nameof(DateTimeStop))]
        public DateTime? DateTimeStop
        {
            get => this.dateTimeStop;
            set
            {
                if (value.Equals(this.dateTimeStop)) return;
                this.dateTimeStop = value;
                this.OnPropertyChanged();
            }
        }

        [DataMember]
        public ObservableCollection<TableTrackArchive> Track
        {
            get => this.track;
            set
            {
                if (Equals(value, this.track)) return;
                this.track = value;
                this.OnPropertyChanged();
            }
        }

        [DataMember]
        [Category("Прочее")]
        [DisplayName(nameof(Note))]
        public string? Note
        {
            get => this.note;
            set
            {
                if (value == this.note) return;
                this.note = value;
                this.OnPropertyChanged();
            }
        }

        public TableResArchive() { }
        public TableResArchive(double frequencyMHz, double bandMHz, ParamDP pulse, ParamDP series, string? targetType, byte antennaMaster, byte antennaSlave, DateTime? dateTimeStart, DateTime? dateTimeStop, ObservableCollection<TableTrackArchive> track, string? note, int id = 0)
        {
            FrequencyMHz = frequencyMHz;
            BandMHz = bandMHz;
            Pulse = pulse.Clone();
            Series = series.Clone();
            TargetType = targetType;
            AntennaMaster = antennaMaster;
            AntennaSlave = antennaSlave;
            DateTimeStart = dateTimeStart;
            DateTimeStop = dateTimeStop;
            Track = new ObservableCollection<TableTrackArchive>(track);
            Note = note;
            Id = id;
        }

        public override void Update(AbstractCommonTable record)
        {
            var newRecord = (TableResArchive)record;
            FrequencyMHz = newRecord.FrequencyMHz;
            BandMHz = newRecord.BandMHz;
            //if (newRecord.Pulse == null || Pulse == null)
            //{
            //    Pulse = newRecord.Pulse;
            //}
            //else
            //{
                Pulse.Update(newRecord.Pulse);
            //}

            //if (newRecord.Series == null || Series == null)
            //{
            //    Series = newRecord.Series;
            //}
            //else
            //{
                Series.Update(newRecord.Series);
            //}

            TargetType = newRecord.TargetType;
            AntennaMaster = newRecord.AntennaMaster;
            AntennaSlave = newRecord.AntennaSlave;
            DateTimeStart = newRecord.DateTimeStart;
            DateTimeStop = newRecord.DateTimeStop;
            Note = newRecord.Note;

            Track = Track ?? newRecord.Track;
            if (Track != newRecord.Track)
            {
                Track.Clear();
                foreach (var point in newRecord.Track)
                    Track.Add(point);
            }
            //if (Track == null || newRecord.Track == null)
            //{
            //    Track = newRecord.Track == null ? null : new ObservableCollection<TableTrackArchive>(newRecord.Track);
            //    return;
            //}
            //Track.Clear();
            //foreach (var point in newRecord.Track)
            //    Track.Add(point);


            //Track = Track ?? newRecord.Track;
            //if (Track != newRecord.Track)
            //{
            //    Track.Clear();
            //    foreach (var point in newRecord.Track)
            //        Track.Add(point);
            //}
        }

        public TableResArchive Clone() => new TableResArchive(FrequencyMHz, BandMHz,  Pulse, Series, TargetType, AntennaMaster, AntennaSlave, DateTimeStart, DateTimeStop, Track, Note, Id);

        private void Pulse_PropertyChanged(object? sender, PropertyChangedEventArgs e)
        {
            this.OnPropertyChanged(nameof(this.Pulse));
        }

        private void Series_PropertyChanged(object? sender, PropertyChangedEventArgs e)
        {
            this.OnPropertyChanged(nameof(this.Series));
        }

        public bool Equals(TableResArchive? other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return this.id == other.id && this.frequencyMHz.Equals(other.frequencyMHz) && this.bandMHz.Equals(other.bandMHz) && this.pulse.Equals(other.pulse) && this.series.Equals(other.series) && this.targetType == other.targetType && this.antennaMaster == other.antennaMaster && this.antennaSlave == other.antennaSlave && Nullable.Equals(this.dateTimeStart, other.dateTimeStart) && Nullable.Equals(this.dateTimeStop, other.dateTimeStop) && this.track.Equals(other.track) && this.note == other.note;
        }

        public override bool Equals(object? obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((TableResArchive)obj);
        }

        public override int GetHashCode()
        {
            var hashCode = new HashCode();
            hashCode.Add(this.id);
            hashCode.Add(this.frequencyMHz);
            hashCode.Add(this.bandMHz);
            hashCode.Add(this.pulse);
            hashCode.Add(this.series);
            hashCode.Add(this.targetType);
            hashCode.Add(this.antennaMaster);
            hashCode.Add(this.antennaSlave);
            hashCode.Add(this.dateTimeStart);
            hashCode.Add(this.dateTimeStop);
            hashCode.Add(this.track);
            hashCode.Add(this.note);
            return hashCode.ToHashCode();
        }
    }
}
