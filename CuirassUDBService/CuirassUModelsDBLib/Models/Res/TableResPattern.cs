﻿namespace CuirassUModelsDBLib
{
    using System.ComponentModel;
    using System.Runtime.Serialization;
    using System;

    /// <summary>
    /// Шаблоны ИРИ 
    /// </summary>
    [DataContract]
    [KnownType(typeof(AbstractCommonTable))]
    [InfoTable(NameTable.TableResPattern)]
    public class TableResPattern : AbstractCommonTable, IEquatable<TableResPattern>
    {
        private int id;

        private double frequencyMinMHz;

        private double frequencyMaxMHz;

        private ParamDP pulse = new ParamDP();

        private ParamDP series = new ParamDP();

        private string name = string.Empty;

        private ModulationType modulationType;

        private WorkingMode workingMode;

        private ClassTarget targetClass;

        private byte[]? imageByte;

        [DataMember]
        [Category("ID")]
        [DisplayName(nameof(Id))]
        [Browsable(false)]
        public override int Id
        {
            get => this.id;
            set
            {
                if (value == this.id) return;
                this.id = value;
                this.OnPropertyChanged();
            }
        }

        [DataMember]
        [Category("Диапазон")]
        [DisplayName(nameof(FrequencyMinMHz))]
        public double FrequencyMinMHz
        {
            get => this.frequencyMinMHz;
            set
            {
                if (value.Equals(this.frequencyMinMHz)) return;
                this.frequencyMinMHz = value;
                this.OnPropertyChanged();
            }
        }

        [DataMember]
        [Category("Диапазон")]
        [DisplayName(nameof(FrequencyMaxMHz))]
        public double FrequencyMaxMHz
        {
            get => this.frequencyMaxMHz;
            set
            {
                if (value.Equals(this.frequencyMaxMHz)) return;
                this.frequencyMaxMHz = value;
                this.OnPropertyChanged();
            }
        }

        [DataMember]
        [Category("Импульс")]
        [DisplayName(" ")]
        [TypeConverter(typeof(ExpandableObjectConverter))]
        public ParamDP Pulse
        {
            get => this.pulse;
            set
            {
                if (Equals(value, this.pulse)) return;
                this.Pulse.PropertyChanged -= Pulse_PropertyChanged;
                this.pulse = value;
                this.Pulse.PropertyChanged += Pulse_PropertyChanged;

                this.OnPropertyChanged();
            }
        }

        private void Pulse_PropertyChanged(object? sender, PropertyChangedEventArgs e)
        {
            this.OnPropertyChanged(nameof(this.Pulse));
        }

        [DataMember]
        [Category("Серия")]
        [DisplayName(" ")]
        [TypeConverter(typeof(ExpandableObjectConverter))]
        public ParamDP Series
        {
            get => this.series;
            set
            {
                if (Equals(value, this.series)) return;
                this.Series.PropertyChanged -= Series_PropertyChanged;
                this.series = value;
                this.Series.PropertyChanged += Series_PropertyChanged;
                this.OnPropertyChanged();
            }
        }

        private void Series_PropertyChanged(object? sender, PropertyChangedEventArgs e)
        {
            this.OnPropertyChanged(nameof(this.Series));
        }

        [DataMember]
        [Category("Общие")]
        [DisplayName(nameof(Name))]
        public string Name
        {
            get => this.name;
            set
            {
                if (value == this.name) return;
                this.name = value;
                this.OnPropertyChanged();
            }
        }

        [DataMember]
        [Category("Общие")]
        [DisplayName(nameof(ModulationType))]
        public ModulationType ModulationType
        {
            get => this.modulationType;
            set
            {
                if (value == this.modulationType) return;
                this.modulationType = value;
                this.OnPropertyChanged();
            }
        }

        [DataMember]
        [Category("Общие")]
        [DisplayName(nameof(WorkingMode))]
        public WorkingMode WorkingMode
        {
            get => this.workingMode;
            set
            {
                if (value == this.workingMode) return;
                this.workingMode = value;
                this.OnPropertyChanged();
            }
        }

        [DataMember]
        [Category("Общие")]
        [DisplayName(nameof(TargetClass))]
        public ClassTarget TargetClass
        {
            get => this.targetClass;
            set
            {
                if (value == this.targetClass) return;
                this.targetClass = value;
                this.OnPropertyChanged();
            }
        }

        [DataMember]
        [Category("Общие")]
        [DisplayName(nameof(ImageByte))]
        public byte[]? ImageByte
        {
            get => this.imageByte;
            set
            {
                if (Equals(value, this.imageByte)) return;
                this.imageByte = value;
                this.OnPropertyChanged();
            }
        }

        public TableResPattern()
        {
            if (this.Pulse != null) this.Pulse.PropertyChanged += Pulse_PropertyChanged;
            if (this.Series != null) this.Series.PropertyChanged += Series_PropertyChanged;
        }
        public TableResPattern(double frequencyMinMHz, double frequencyMaxMHz, ParamDP pulse, ParamDP series, string name, ModulationType modulationType, WorkingMode workingMode, ClassTarget targetClass, byte[]? imageByte, int id = 0)
        {
            FrequencyMinMHz = frequencyMinMHz;
            FrequencyMaxMHz = frequencyMaxMHz;
            Pulse = pulse.Clone();
            Series = series.Clone();
            Name = name;
            ModulationType = modulationType;
            WorkingMode = workingMode;
            TargetClass = targetClass;
            ImageByte = imageByte;
            Id = id;
        }

        public override void Update(AbstractCommonTable record)
        {
            var newRecord = (TableResPattern)record;
            FrequencyMinMHz = newRecord.FrequencyMinMHz;
            FrequencyMaxMHz = newRecord.FrequencyMaxMHz;

            //if (newRecord.Pulse == null || Pulse == null)
            //{
            //    Pulse = newRecord.Pulse;
            //}
            //else
            //{
                Pulse.Update(newRecord.Pulse);
            //}

            //if (newRecord.Series == null || Series == null)
            //{
            //    Series = newRecord.Series;
            //}
            //else
            //{
                Series.Update(newRecord.Series);
            //}
            Name = newRecord.Name;
            ModulationType = newRecord.ModulationType;
            WorkingMode = newRecord.WorkingMode;
            TargetClass = newRecord.TargetClass;
            ImageByte = newRecord.ImageByte;
        }

        public TableResPattern Clone() => new TableResPattern(FrequencyMinMHz, FrequencyMaxMHz, Pulse, Series, Name, ModulationType, WorkingMode, TargetClass, ImageByte, Id);

        public bool Equals(TableResPattern? other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return this.id == other.id && this.frequencyMinMHz.Equals(other.frequencyMinMHz) && this.frequencyMaxMHz.Equals(other.frequencyMaxMHz) && this.pulse.Equals(other.pulse) && this.series.Equals(other.series) && this.name == other.name && this.modulationType == other.modulationType && this.workingMode == other.workingMode && this.targetClass == other.targetClass && Equals(this.imageByte, other.imageByte);
        }

        public override bool Equals(object? obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((TableResPattern)obj);
        }

        public override int GetHashCode()
        {
            var hashCode = new HashCode();
            hashCode.Add(this.id);
            hashCode.Add(this.frequencyMinMHz);
            hashCode.Add(this.frequencyMaxMHz);
            hashCode.Add(this.pulse);
            hashCode.Add(this.series);
            hashCode.Add(this.name);
            hashCode.Add((int)this.modulationType);
            hashCode.Add((int)this.workingMode);
            hashCode.Add((int)this.targetClass);
            hashCode.Add(this.imageByte);
            return hashCode.ToHashCode();
        }
    }
}
