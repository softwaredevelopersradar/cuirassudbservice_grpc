﻿namespace CuirassUModelsDBLib
{
    using System;
    using System.Collections.ObjectModel;
    using System.ComponentModel;
    using System.Runtime.Serialization;

    /// <summary>
    /// ИРИ ЦР
    /// </summary>
    [DataContract]
    [KnownType(typeof(AbstractCommonTable))]
    [InfoTable(NameTable.TableResDistribution)]
    public class TableResDistribution : AbstractCommonTable, IEquatable<TableResDistribution>
    {
        private int id;

        private double frequencyMHz;

        private double bandMHz;

        private float? bearing;

        private ParamDP pulse = new ParamDP();

        private ParamDP series = new ParamDP();

        private string? targetType;

        private byte antennaMaster;

        private byte antennaSlave;

        //private ReceiveSignal receiverL;

        //private ReceiveSignal receiver1;

        //private ReceiveSignal receiver2;

        //private ReceiveSignal receiver3;

        private float? receiverL;

        private float? receiver1;

        private float? receiver2;

        private float? receiver3;

        private ObservableCollection<TableTrackDistribution> track = new();

        private string? note;

        [DataMember]
        [Category("ID")]
        [DisplayName(nameof(Id))]
        [Browsable(true)]
        public override int Id
        {
            get => this.id;
            set
            {
                if (value == this.id) return;
                this.id = value;
                this.OnPropertyChanged();
            }
        }

        [DataMember]
        [Category("Общие")]
        [DisplayName(nameof(FrequencyMHz))]
        public double FrequencyMHz
        {
            get => this.frequencyMHz;
            set
            {
                if (value.Equals(this.frequencyMHz)) return;
                this.frequencyMHz = value;
                this.OnPropertyChanged();
            }
        }

        [DataMember]
        [Category("Общие")]
        [DisplayName(nameof(BandMHz))]
        public double BandMHz
        {
            get => this.bandMHz;
            set
            {
                if (value.Equals(this.bandMHz)) return;
                this.bandMHz = value;
                this.OnPropertyChanged();
            }
        }

        [DataMember]
        [Category("Общие")]
        [DisplayName(nameof(Bearing))]
        public float? Bearing
        {
            get => this.bearing;
            set
            {
                if (value.Equals(this.bearing)) return;
                this.bearing = value;
                this.OnPropertyChanged();
            }
        }

        [DataMember]
        [Category(nameof(ParamDP))]
        [DisplayName(" ")]
        [TypeConverter(typeof(ExpandableObjectConverter))]
        public ParamDP Pulse
        {
            get => this.pulse;
            set
            {
                if (Equals(value, this.pulse)) return;
                this.Pulse.PropertyChanged -= Pulse_PropertyChanged;
                this.pulse = value;
                this.Pulse.PropertyChanged += Pulse_PropertyChanged;
                this.OnPropertyChanged();
            }
        }

        [DataMember]
        [Category(nameof(ParamDP))]
        [DisplayName(" ")]
        [TypeConverter(typeof(ExpandableObjectConverter))]
        public ParamDP Series
        {
            get => this.series;
            set
            {
                if (Equals(value, this.series)) return;
                this.Series.PropertyChanged -= Series_PropertyChanged;
                this.series = value;
                this.Series.PropertyChanged += Series_PropertyChanged;
                this.OnPropertyChanged();
            }
        }

        [DataMember]
        [Category("Общие")]
        [DisplayName(nameof(TargetType))]
        public string? TargetType
        {
            get => this.targetType;
            set
            {
                if (value == this.targetType) return;
                this.targetType = value;
                this.OnPropertyChanged();
            }
        }

        [DataMember]
        [Category("Общие")]
        [DisplayName(nameof(AntennaMaster))]
        public byte AntennaMaster
        {
            get => this.antennaMaster;
            set
            {
                if (value == this.antennaMaster) return;
                this.antennaMaster = value;
                this.OnPropertyChanged();
            }
        }

        [DataMember]
        [Category("Общие")]
        [DisplayName(nameof(AntennaSlave))]
        public byte AntennaSlave
        {
            get => this.antennaSlave;
            set
            {
                if (value == this.antennaSlave) return;
                this.antennaSlave = value;
                this.OnPropertyChanged();
            }
        }

        //[DataMember]
        //[Category("Общие")]
        //[DisplayName(nameof(ReceiverL))]
        //public ReceiveSignal ReceiverL
        //{
        //    get => this.receiverL;
        //    set
        //    {
        //        if (value == this.receiverL) return;
        //        this.receiverL = value;
        //        this.OnPropertyChanged();
        //    }
        //}

        //[DataMember]
        //[Category("Общие")]
        //[DisplayName(nameof(Receiver1))]
        //public ReceiveSignal Receiver1
        //{
        //    get => this.receiver1;
        //    set
        //    {
        //        if (value == this.receiver1) return;
        //        this.receiver1 = value;
        //        this.OnPropertyChanged();
        //    }
        //}

        //[DataMember]
        //[Category("Общие")]
        //[DisplayName(nameof(Receiver2))]
        //public ReceiveSignal Receiver2
        //{
        //    get => this.receiver2;
        //    set
        //    {
        //        if (value == this.receiver2) return;
        //        this.receiver2 = value;
        //        this.OnPropertyChanged();
        //    }
        //}

        //[DataMember]
        //[Category("Общие")]
        //[DisplayName(nameof(Receiver3))]
        //public ReceiveSignal Receiver3
        //{
        //    get => this.receiver3;
        //    set
        //    {
        //        if (value == this.receiver3) return;
        //        this.receiver3 = value;
        //        this.OnPropertyChanged();
        //    }
        //}

        [DataMember]
        [Category("Общие")]
        [DisplayName(nameof(ReceiverL))]
        public float? ReceiverL
        {
            get => this.receiverL;
            set
            {
                if (value == this.receiverL) return;
                this.receiverL = value;
                this.OnPropertyChanged();
            }
        }

        [DataMember]
        [Category("Общие")]
        [DisplayName(nameof(Receiver1))]
        public float? Receiver1
        {
            get => this.receiver1;
            set
            {
                if (value == this.receiver1) return;
                this.receiver1 = value;
                this.OnPropertyChanged();
            }
        }

        [DataMember]
        [Category("Общие")]
        [DisplayName(nameof(Receiver2))]
        public float? Receiver2
        {
            get => this.receiver2;
            set
            {
                if (value == this.receiver2) return;
                this.receiver2 = value;
                this.OnPropertyChanged();
            }
        }

        [DataMember]
        [Category("Общие")]
        [DisplayName(nameof(Receiver3))]
        public float? Receiver3
        {
            get => this.receiver3;
            set
            {
                if (value == this.receiver3) return;
                this.receiver3 = value;
                this.OnPropertyChanged();
            }
        }

        [DataMember]
        public ObservableCollection<TableTrackDistribution> Track
        {
            get => this.track;
            set
            {
                if (Equals(value, this.track)) return;
                this.track = value;
                this.OnPropertyChanged();
            }
        }

        [DataMember]
        [Category("Прочее")]
        [DisplayName(nameof(Note))]
        public string? Note
        {
            get => this.note;
            set
            {
                if (value == this.note) return;
                this.note = value;
                this.OnPropertyChanged();
            }
        }

        public TableResDistribution() { }
        public TableResDistribution(double frequencyMHz, double bandMHz, float? bearing, ParamDP pulse, ParamDP series, string? targetType, byte antennaMaster, byte antennaSlave, float? l, float? r1, float? r2, float? r3, ObservableCollection<TableTrackDistribution> track, string? note, int id = 0)
        {
            FrequencyMHz = frequencyMHz;
            BandMHz = bandMHz;
            Bearing = bearing;
            Pulse = pulse.Clone();
            Series = series.Clone();
            TargetType = targetType;
            AntennaMaster = antennaMaster;
            AntennaSlave = antennaSlave;
            ReceiverL = l;
            Receiver1 = r1;
            Receiver2 = r2;
            Receiver2 = r3;
            Track = new ObservableCollection<TableTrackDistribution>(track);
            Note = note;
            Id = id;
        }

        public override void Update(AbstractCommonTable record)
        {
            var newRecord = (TableResDistribution)record;
            FrequencyMHz = newRecord.FrequencyMHz;
            BandMHz = newRecord.BandMHz;
            Bearing = newRecord.Bearing;
            //if (newRecord.Pulse == null || Pulse == null)
            //{
            //    Pulse = newRecord.Pulse;
            //}
            //else
            //{
                Pulse.Update(newRecord.Pulse);
            //}

            //if (newRecord.Series == null || Series == null)
            //{
            //    Series = newRecord.Series;
            //}
            //else
            //{
                Series.Update(newRecord.Series);
            //}

            TargetType = newRecord.TargetType;
            AntennaMaster = newRecord.AntennaMaster;
            AntennaSlave = newRecord.AntennaSlave;
            ReceiverL = newRecord.ReceiverL;
            Receiver1 = newRecord.Receiver1;
            Receiver2 = newRecord.Receiver2;
            Receiver3 = newRecord.Receiver3;
            Note = newRecord.Note;

            //TODO: check
            //if (Track == null || newRecord.Track == null)
            //{
            //    Track = newRecord.Track == null ? null : new ObservableCollection<TableTrackDistribution>(newRecord.Track);
            //    return;
            //}

            //Track.Clear();
            //foreach (var point in newRecord.Track)
            //    Track.Add(point);

            Track = Track ?? newRecord.Track;
            if (Track != newRecord.Track)
            {
                Track.Clear();
                foreach (var point in newRecord.Track)
                    Track.Add(point);
            }
        }

        public TableResDistribution Clone() => new TableResDistribution(FrequencyMHz, BandMHz, Bearing, Pulse, Series, TargetType, AntennaMaster, AntennaSlave, ReceiverL, Receiver1, Receiver2, Receiver3, Track, Note, Id);


        private void Pulse_PropertyChanged(object? sender, PropertyChangedEventArgs e)
        {
            this.OnPropertyChanged(nameof(this.Pulse));
        }

        private void Series_PropertyChanged(object? sender, PropertyChangedEventArgs e)
        {
            this.OnPropertyChanged(nameof(this.Series));
        }

        public bool Equals(TableResDistribution? other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return this.id == other.id && this.frequencyMHz.Equals(other.frequencyMHz) && this.bandMHz.Equals(other.bandMHz) && Nullable.Equals(this.bearing, other.bearing) && this.pulse.Equals(other.pulse) && this.series.Equals(other.series) && this.targetType == other.targetType && this.antennaMaster == other.antennaMaster && this.antennaSlave == other.antennaSlave && this.receiverL == other.receiverL && this.receiver1 == other.receiver1 && this.receiver2 == other.receiver2 && this.receiver3 == other.receiver3 && this.track.Equals(other.track) && this.note == other.note;
        }

        public override bool Equals(object? obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((TableResDistribution)obj);
        }

        public override int GetHashCode()
        {
            var hashCode = new HashCode();
            hashCode.Add(this.id);
            hashCode.Add(this.frequencyMHz);
            hashCode.Add(this.bandMHz);
            hashCode.Add(this.bearing);
            hashCode.Add(this.pulse);
            hashCode.Add(this.series);
            hashCode.Add(this.targetType);
            hashCode.Add(this.antennaMaster);
            hashCode.Add(this.antennaSlave);
            hashCode.Add(this.receiverL);
            hashCode.Add(this.receiver1);
            hashCode.Add(this.receiver2);
            hashCode.Add(this.receiver3);
            hashCode.Add(this.track);
            hashCode.Add(this.note);
            return hashCode.ToHashCode();
        }
    }
}
