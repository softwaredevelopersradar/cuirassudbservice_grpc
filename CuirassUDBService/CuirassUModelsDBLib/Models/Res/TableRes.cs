﻿namespace CuirassUModelsDBLib
{
    using System;
    using System.ComponentModel;
    using System.Runtime.Serialization;

    /// <summary>
    /// ИРИ 
    /// </summary>
    [DataContract]
    [KnownType(typeof(AbstractCommonTable))]
    [InfoTable(NameTable.TableRes)]
    public class TableRes : AbstractCommonTable, IEquatable<TableRes>
    {
        private int id;

        private double frequencyMHz;

        private double bandMHz;

        private float? bearing;

        private float? standardDeviation;

        private ParamDP pulse = new ParamDP();

        private ParamDP series = new ParamDP();

        private string? targetType;

        private ModulationType modulationType;

        private byte antennaMaster;

        private byte antennaSlave;

        private double amplitudeMaster;

        private double amplitudeSlave;

        private string? note;

        [DataMember]
        [Category("ID")]
        [DisplayName(nameof(Id))]
        [Browsable(true)]
        public override int Id
        {
            get => this.id;
            set
            {
                if (value == this.id) return;
                this.id = value;
                this.OnPropertyChanged();
            }
        }

        [DataMember]
        [Category("Общие")]
        [DisplayName(nameof(FrequencyMHz))]
        public double FrequencyMHz
        {
            get => this.frequencyMHz;
            set
            {
                if (value.Equals(this.frequencyMHz)) return;
                this.frequencyMHz = value;
                this.OnPropertyChanged();
            }
        }

        [DataMember]
        [Category("Общие")]
        [DisplayName(nameof(BandMHz))]
        public double BandMHz
        {
            get => this.bandMHz;
            set
            {
                if (value.Equals(this.bandMHz)) return;
                this.bandMHz = value;
                this.OnPropertyChanged();
            }
        }

        [DataMember]
        [Category("Общие")]
        [DisplayName(nameof(Bearing))]
        public float? Bearing
        {
            get => this.bearing;
            set
            {
                if (value.Equals(this.bearing)) return;
                this.bearing = value;
                this.OnPropertyChanged();
            }
        }

        [DataMember]
        [Category(nameof(ParamDP))]
        [DisplayName(" ")]
        [TypeConverter(typeof(ExpandableObjectConverter))]
        
        public ParamDP Pulse
        {
            get => this.pulse;
            set
            {
                if (Equals(value, this.pulse)) return;
                this.Pulse.PropertyChanged -= Pulse_PropertyChanged;
                this.pulse = value;
                this.Pulse.PropertyChanged += Pulse_PropertyChanged;
                this.OnPropertyChanged();
            }
        }

        [DataMember]
        [Category(nameof(ParamDP))]
        [DisplayName(" ")]
        [TypeConverter(typeof(ExpandableObjectConverter))]
        public ParamDP Series
        {
            get => this.series;
            set
            {
                if (Equals(value, this.series)) return;
                this.Series.PropertyChanged -= Series_PropertyChanged;
                this.series = value;
                this.Series.PropertyChanged += Series_PropertyChanged;
                this.OnPropertyChanged();
            }
        }

        [DataMember]
        [Category("Общие")]
        [DisplayName(nameof(TargetType))]
        public string? TargetType
        {
            get => this.targetType;
            set
            {
                if (value == this.targetType) return;
                this.targetType = value;
                this.OnPropertyChanged();
            }
        }

        [DataMember]
        [Category("Общие")]
        [DisplayName(nameof(ModulationType))]
        public ModulationType ModulationType
        {
            get => this.modulationType;
            set
            {
                if (value == this.modulationType) return;
                this.modulationType = value;
                this.OnPropertyChanged();
            }
        }

        [DataMember]
        [Category("Общие")]
        [DisplayName(nameof(AntennaMaster))]
        public byte AntennaMaster
        {
            get => this.antennaMaster;
            set
            {
                if (value == this.antennaMaster) return;
                this.antennaMaster = value;
                this.OnPropertyChanged();
            }
        }

        [DataMember]
        [Category("Общие")]
        [DisplayName(nameof(AntennaSlave))]
        public byte AntennaSlave
        {
            get => this.antennaSlave;
            set
            {
                if (value == this.antennaSlave) return;
                this.antennaSlave = value;
                this.OnPropertyChanged();
            }
        }

        [DataMember]
        [Category("Общие")]
        [DisplayName(nameof(AmplitudeMaster))]
        public double AmplitudeMaster
        {
            get => this.amplitudeMaster;
            set
            {
                if (value.Equals(this.amplitudeMaster)) return;
                this.amplitudeMaster = value;
                this.OnPropertyChanged();
            }
        }

        [DataMember]
        [Category("Общие")]
        [DisplayName(nameof(AmplitudeSlave))]
        public double AmplitudeSlave
        {
            get => this.amplitudeSlave;
            set
            {
                if (value.Equals(this.amplitudeSlave)) return;
                this.amplitudeSlave = value;
                this.OnPropertyChanged();
            }
        }

        [DataMember]
        [Category("Прочее")]
        [DisplayName(nameof(Note))]
        public string? Note
        {
            get => this.note;
            set
            {
                if (value == this.note) return;
                this.note = value;
                this.OnPropertyChanged();
            }
        }

        [DataMember]
        public float? StandardDeviation
        {
            get => this.standardDeviation;
            set
            {
                if (Nullable.Equals(value, this.standardDeviation)) return;
                this.standardDeviation = value;
                this.OnPropertyChanged();
            }
        }

        public TableRes() { }
        public TableRes(double frequencyMHz, double bandMHz, float? bearing,float? standardDeviation, ParamDP pulse, ParamDP series, string? targetType, ModulationType modulationType, byte antennaMaster, byte antennaSlave, double amplitudeMaster, double amplitudeSlave, string? note, int id = 0) 
        {
            FrequencyMHz = frequencyMHz;
            BandMHz = bandMHz;
            Bearing = bearing;
            StandardDeviation = standardDeviation;
            Pulse = pulse.Clone();
            Series = series.Clone();
            TargetType = targetType;
            ModulationType = modulationType;
            AntennaMaster = antennaMaster;
            AntennaSlave = antennaSlave;
            AmplitudeMaster = amplitudeMaster;
            AmplitudeSlave = amplitudeSlave;
            Note = note;
            Id = id;
        }

        public override void Update(AbstractCommonTable record)
        {
            var newRecord = (TableRes)record;
            FrequencyMHz = newRecord.FrequencyMHz;
            BandMHz = newRecord.BandMHz;
            Bearing = newRecord.Bearing;
            this.StandardDeviation = newRecord.StandardDeviation;
            //if (newRecord.Pulse == null || Pulse == null)
            //{
            //    Pulse = newRecord.Pulse;
            //}
            //else
            //{
                Pulse.Update(newRecord.Pulse);
            //}

            //if (newRecord.Series == null || Series == null)
            //{
            //    Series = newRecord.Series;
            //}
            //else
            //{
                Series.Update(newRecord.Series);
            //}
            TargetType = newRecord.TargetType;
            ModulationType = newRecord.ModulationType;
            AntennaMaster = newRecord.AntennaMaster;
            AntennaSlave = newRecord.AntennaSlave;
            AmplitudeMaster = newRecord.AmplitudeMaster;
            AmplitudeSlave = newRecord.AmplitudeSlave;
            Note = newRecord.Note;
        }

        public TableRes Clone() => new TableRes(FrequencyMHz, BandMHz, Bearing, this.StandardDeviation, Pulse, Series, TargetType, ModulationType, AntennaMaster, AntennaSlave, AmplitudeMaster, AmplitudeSlave, Note, Id);


        private void Pulse_PropertyChanged(object? sender, PropertyChangedEventArgs e) 
        {
            this.OnPropertyChanged(nameof(this.Pulse));
        }

        private void Series_PropertyChanged(object? sender, PropertyChangedEventArgs e) 
        {
            this.OnPropertyChanged(nameof(this.Series));
        }

        public bool Equals(TableRes? other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return this.id == other.id && this.frequencyMHz.Equals(other.frequencyMHz) && this.bandMHz.Equals(other.bandMHz) && Nullable.Equals(this.bearing, other.bearing) && Nullable.Equals(this.standardDeviation, other.standardDeviation) && this.pulse.Equals(other.pulse) && this.series.Equals(other.series) && this.targetType == other.targetType && this.modulationType == other.modulationType && this.antennaMaster == other.antennaMaster && this.antennaSlave == other.antennaSlave && this.amplitudeMaster.Equals(other.amplitudeMaster) && this.amplitudeSlave.Equals(other.amplitudeSlave) && this.note == other.note;
        }

        public override bool Equals(object? obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((TableRes)obj);
        }

        public override int GetHashCode()
        {
            var hashCode = new HashCode();
            hashCode.Add(this.id);
            hashCode.Add(this.frequencyMHz);
            hashCode.Add(this.bandMHz);
            hashCode.Add(this.bearing);
            hashCode.Add(this.standardDeviation);
            hashCode.Add(this.pulse);
            hashCode.Add(this.series);
            hashCode.Add(this.targetType);
            hashCode.Add((int)this.modulationType);
            hashCode.Add(this.antennaMaster);
            hashCode.Add(this.antennaSlave);
            hashCode.Add(this.amplitudeMaster);
            hashCode.Add(this.amplitudeSlave);
            hashCode.Add(this.note);
            return hashCode.ToHashCode();
        }
    }
}
