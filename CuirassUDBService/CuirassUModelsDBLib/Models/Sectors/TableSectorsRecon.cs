﻿namespace CuirassUModelsDBLib
{
    using System;
    using System.ComponentModel;
    using System.Runtime.Serialization;

    /// <summary>
    /// Сектора РР
    /// </summary>
    [DataContract]
    [InfoTable(NameTable.TableSectorsRecon)]
    public class TableSectorsRecon : AbstractCommonTable, IEquatable<TableSectorsRecon>
    {
        private int id;

        private short angleMin;

        private short angleMax;

        private int antennaIndexStart;

        private int antennaIndexEnd;

        private string note = string.Empty;

        private bool isActive;

        [DataMember]
        [Category("ID")]
        [DisplayName(nameof(Id)), Browsable(false)]
        public override int Id
        {
            get => this.id;
            set
            {
                if (value == this.id) return;
                this.id = value;
                this.OnPropertyChanged();
            }
        }

        [DataMember]
        [Category("Сектор")]
        [DisplayName(nameof(AngleMin))]
        public short AngleMin
        {
            get => this.angleMin;
            set
            {
                if (value == this.angleMin) return;
                this.angleMin = value;
                this.OnPropertyChanged();
            }
        }

        [DataMember]
        [Category("Сектор")]
        [DisplayName(nameof(AngleMax))]
        public short AngleMax
        {
            get => this.angleMax;
            set
            {
                if (value == this.angleMax) return;
                this.angleMax = value;
                this.OnPropertyChanged();
            }
        }

        [DataMember]
        [Category("Сектор")]
        [DisplayName(nameof(AntennaIndexStart)), Browsable(false)]
        public int AntennaIndexStart
        {
            get => this.antennaIndexStart;
            set
            {
                if (value == this.antennaIndexStart) return;
                this.antennaIndexStart = value;
                this.OnPropertyChanged();
            }
        }

        [DataMember]
        [Category("Сектор")]
        [DisplayName(nameof(AntennaIndexEnd)), Browsable(false)]
        public int AntennaIndexEnd
        {
            get => this.antennaIndexEnd;
            set
            {
                if (value == this.antennaIndexEnd) return;
                this.antennaIndexEnd = value;
                this.OnPropertyChanged();
            }
        }

        [DataMember]
        [Category("Прочее")]
        [DisplayName(nameof(Note))]
        public string Note
        {
            get => this.note;
            set
            {
                if (value == this.note) return;
                this.note = value;
                this.OnPropertyChanged();
            }
        }

        [DataMember]
        [Category("Прочее")]
        [DisplayName(nameof(IsActive)), Browsable(false)]
        public bool IsActive
        {
            get => this.isActive;
            set
            {
                if (value == this.isActive) return;
                this.isActive = value;
                this.OnPropertyChanged();
            }
        }

        public TableSectorsRecon() { }
        public TableSectorsRecon(short angleMin , short angleMax, int antennaIndexStart, int antennaIndexEnd, string note, bool isActive, int id = 0)
        {
            AngleMin = angleMin;
            AngleMax = angleMax;
            AntennaIndexStart = antennaIndexStart;
            AntennaIndexEnd = antennaIndexEnd;
            Note = note;
            IsActive = isActive;
            Id = id;
        }

        public TableSectorsRecon Clone() => new TableSectorsRecon(AngleMin, AngleMax, AntennaIndexStart, AntennaIndexEnd, Note, IsActive, Id);
        
        public override void Update(AbstractCommonTable record)
        {
            var newRecord = (TableSectorsRecon)record;
            AngleMin = newRecord.AngleMin;
            AngleMax = newRecord.AngleMax;
            AntennaIndexStart = newRecord.AntennaIndexStart;
            AntennaIndexEnd = newRecord.AntennaIndexEnd;
            Note = newRecord.Note;
            IsActive = newRecord.IsActive;
        }

        public bool Equals(TableSectorsRecon? other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return id == other.id && angleMin == other.angleMin && angleMax == other.angleMax && antennaIndexStart == other.antennaIndexStart && antennaIndexEnd == other.antennaIndexEnd && note == other.note && isActive == other.isActive;
        }

        public override bool Equals(object? obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((TableSectorsRecon)obj);
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(id, angleMin, angleMax, antennaIndexStart, antennaIndexEnd, note, isActive);
        }
    }
}
