﻿

namespace CuirassUModelsDBLib.InheritorsExceptions
{
    public interface IExceptionService : IExceptionDb
    {
        int IdClient { get; }
    }
}
