﻿using CuirassUModelsDBLib.Errors;

namespace CuirassUModelsDBLib.InheritorsExceptions
{
    public interface IExceptionDb
    {
        EnumDBError Error { get; }

        string Message { get; }
    }
}
