﻿using System.Runtime.Serialization;

namespace CuirassUModelsDBLib.Errors
{
    public enum EnumClientError : byte
    {
        None,
        UnknownError,
        IncorrectEndpoint,
        NoConnection,
    }

    public enum EnumServerError : byte
    {
        ClientAbsent,
        UnknownError
    }

    [DataContract]
    public enum EnumDBError : byte
    {
        [EnumMember]
        UnknownError,
        [EnumMember]
        RecordExist,
        [EnumMember]
        RecordNotFound,
        [EnumMember]
        SuchAspIsAbsent,
        [EnumMember]
        NoColumnAsp,
        [EnumMember]
        None,
        [EnumMember]
        SuchOwnFreqIsAbsent,
        [EnumMember]
        SuchResIsAbsent,
    }
}
