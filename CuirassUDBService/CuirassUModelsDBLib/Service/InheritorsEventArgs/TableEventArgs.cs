﻿using System;
using System.Collections.Generic;

namespace CuirassUModelsDBLib.InheritorsEventArgs
{
    public class TableEventArgs<T> : EventArgs where T : AbstractCommonTable
    {
        public List<T> Table { get; protected set; }

        public TableEventArgs(ClassDataCommon lData)
        {
            Table = lData.ToList<T>();
        }

        public TableEventArgs(List<T> lData)
        {
            Table = lData;
        }
    }
}
