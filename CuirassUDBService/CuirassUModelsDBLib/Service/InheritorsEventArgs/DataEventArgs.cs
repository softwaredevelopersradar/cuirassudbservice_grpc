﻿using System;
using System.Runtime.Serialization;

namespace CuirassUModelsDBLib.InheritorsEventArgs
{
    [DataContract]
    public class DataEventArgs : EventArgs
    { 
        [DataMember]
        public ClassDataCommon AbstractData { get; protected set; }

        [DataMember]
        public NameTable Name { get; protected set; }

        public DataEventArgs(NameTable nameTable, ClassDataCommon data)
        {
            Name = nameTable;
            AbstractData = data;
        }

    }
}
