﻿using System;
using System.Runtime.Serialization;

namespace CuirassUModelsDBLib.InheritorsEventArgs
{
    [DataContract]
    public class RecordEventArgs
    {
        [DataMember]
        public AbstractCommonTable AbstractRecord { get; protected set; }

        [DataMember]
        public NameTable Name { get; protected set; }

        [DataMember]
        public NameChangeOperation NameAction { get; protected set; }


        public RecordEventArgs(NameTable nameTable, AbstractCommonTable record, NameChangeOperation action)
        {
            Name = nameTable;
            AbstractRecord = record;
            NameAction = action;
        }
    }
}
