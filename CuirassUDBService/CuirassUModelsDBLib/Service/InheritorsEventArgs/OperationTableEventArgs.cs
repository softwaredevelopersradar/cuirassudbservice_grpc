﻿using System;
using System.Runtime.Serialization;
using CuirassUModelsDBLib.Errors;

namespace CuirassUModelsDBLib.InheritorsEventArgs
{
    [DataContract]
    public class OperationTableEventArgs : AStandardEventArgs
    {
        [DataMember]
        public NameTableOperation Operation { get; private set; }

        [DataMember]
        public NameTable TableName { get; protected set; }

        [DataMember]
        public override string GetMessage { get; protected set; }

        [DataMember]
        public EnumDBError TypeError { get; protected set; }

        public OperationTableEventArgs(NameTableOperation act, EnumDBError typeError)
        {
            DateTime time = DateTime.Now;
            GetMessage = $"{time.ToLongTimeString()}:{time.Millisecond} ";
            GetMessage += $"Operation: {act.ToString()} ";
            Operation = act;
            TypeError = typeError;
            GetMessage += DBError.GetDefenition(typeError);
        }

        public OperationTableEventArgs(NameChangeOperation act, EnumDBError typeError)
        {
            DateTime time = DateTime.Now;
            GetMessage = $"{time.ToLongTimeString()}:{time.Millisecond} ";
            GetMessage += $"Operation: {act.ToString()} ";
            Operation = (NameTableOperation)Enum.Parse(typeof(NameTableOperation), act.ToString());
            TypeError = typeError;
            GetMessage += DBError.GetDefenition(typeError);
        }

        public OperationTableEventArgs(NameTableOperation act, string message)
        {
            DateTime time = DateTime.Now;
            GetMessage = $"{time.ToLongTimeString()}:{time.Millisecond} ";
            GetMessage += $"Operation: {act.ToString()} ";
            Operation = act;
            GetMessage += message;
            TypeError = EnumDBError.UnknownError;
        }

        public OperationTableEventArgs(NameChangeOperation act, string message)
        {
            DateTime time = DateTime.Now;
            GetMessage = $"{time.ToLongTimeString()}:{time.Millisecond} ";
            GetMessage += $"Operation: {act.ToString()} ";
            Operation = (NameTableOperation)Enum.Parse(typeof(NameTableOperation), act.ToString());
            GetMessage += message;
            TypeError = EnumDBError.UnknownError;
        }

        public OperationTableEventArgs(NameTableOperation act, EnumDBError typeError, string message) : this(act, message)
        {
            TypeError = typeError;
        }

        public OperationTableEventArgs(NameChangeOperation act,EnumDBError typeError, string message) : this(act, message)
        {
            TypeError = typeError;
        }

    }
}
