﻿namespace ServerDBCuirassULib
{
    using System;
    using System.Collections.Concurrent;
    using System.Collections.Generic;
    using System.Threading.Tasks;

    using CuirassUDbPackage;

    using CuirassUModelsDBLib;
    using CuirassUModelsDBLib.Errors;
    using CuirassUModelsDBLib.InheritorsEventArgs;
    using CuirassUModelsDBLib.InheritorsExceptions;

    using Grpc.Core;

    using OperationTablesLib;

    public partial class ProtoServer : CuirassUDbTransmission.CuirassUDbTransmissionBase
    {
        #region Clients
        private static readonly ConcurrentDictionary<int, ClientService> _clients = new ConcurrentDictionary<int, ClientService>();
        private static readonly object _clientLocker = new object();
        private static int _nextId = 1;
        #endregion

        /// <summary>
        /// словарь, который хранит String - имя таблицы, 
        /// ITableAction - объект класса, который реализует интерфейс(ITableAction - действия над таблицей) 
        /// </summary>
        private static Dictionary<NameTable, ITableAction>? dicOperTables;


        public ProtoServer()
        {
            //_clients = new Dictionary<string, DateTime>();
            if (dicOperTables != null) return;
            //Operation.OnReceiveData += HandlerUpData;
            //Operation.OnAddRange += HandlerAddRange;                
            //Operation.OnReceiveRecord += HandlerUpRecord;
            //Operation.OnSendMessToHost += HandlerMessToHostOperation;

            dicOperTables = new Dictionary<NameTable, ITableAction>
            {
                { NameTable.TableSectorsRecon, new OperationTableDb<TableSectorsRecon>()},
                { NameTable.TableFreqForbidden, new OperationTableDb<TableFreqForbidden>()},
                { NameTable.TableFreqRangesRecon, new OperationTableDb<TableFreqRangesRecon>()},
                { NameTable.GlobalPropertiesRRS, new OperationGlobalPropertiesRRS() },
                { NameTable.GlobalProperties, new OperationGlobalProperties() },
                { NameTable.TableRes, new OperationTempTable<TableRes>()},
                { NameTable.TableResPattern, new OperationTableDb<TableResPattern>()},
                { NameTable.TableResArchive, new OperationTableResArchive()},
                { NameTable.TableTrackArchive, new OperationTableDb<TableTrackArchive>()},
                { NameTable.TableRemotePoints, new OperationTableDb<TableRemotePoints>()},
                { NameTable.TableResDistribution, new OperationTableResDistribution()},
                { NameTable.TableTrackDistribution, new OperationTableDb<TableTrackDistribution>()}
            };
        }

        public override Task<IdMessage> Connect(PingRequest name, ServerCallContext context)
        {
            lock (_clientLocker)
            {
                ClientService client = new ClientService()
                {
                    ID = _nextId,
                    Name = name.ClientName
                };
                _nextId++;
                _clients.TryAdd(client.ID, client);

                SendStateClient(client.Name, ServerEventArgs.ActClient.Connect);

                return Task.FromResult(new IdMessage() { Id = client.ID });
            }
        }

        public override Task<PingResponse> Ping(IdMessage request, ServerCallContext context)
        {
            if (_clients.ContainsKey(request.Id))
            {
                return Task.FromResult(new PingResponse() { IsFirstPingRequest = true });
            }
            return Task.FromResult(new PingResponse() { IsFirstPingRequest = false });
        }

        public override Task<DefaultResponse> Disconnect(IdMessage message, ServerCallContext context)
        {
            if (_clients.ContainsKey(message.Id))
            {
                var result = _clients.TryRemove(message.Id, out ClientService? removedClient);
                if (result)
                {
                    SendStateClient(removedClient?.Name ?? string.Empty, ServerEventArgs.ActClient.Disconnect);
                    //Operation.OnReceiveData -= UpDataHandler;
                }
                return Task.FromResult(new DefaultResponse() { IsSucceeded = true });
            }
            return Task.FromResult(new DefaultResponse() { IsSucceeded = true });
        }


        #region Events
        public static event EventHandler<OperationTableEventArgs> OnReceiveMsgDB = (object? obj, OperationTableEventArgs eventArgs) => { };
        public static event EventHandler<OperationTableEventArgs> OnReceiveErrorDB = (object? obj, OperationTableEventArgs eventArgs) => { };

        public static event EventHandler<ServerEventArgs> OnStateClient = (object? obj, ServerEventArgs eventArgs) => { };
        public static event EventHandler<ServerEventArgs> OnErrorClient = (object? obj, ServerEventArgs eventArgs) => { };

        public static event EventHandler<string> OnMessToHost = (obj, eventArgs) => { };
        #endregion

        private void SendStateClient(string ClientName, ServerEventArgs.ActClient act)
        {
            OnStateClient(this, new ServerEventArgs(DateTime.Now.ToShortTimeString(), ClientName, act));
        }

        private void SendErrorOfClient(EnumServerError error, string Mess)
        {
            OnErrorClient(this, new ServerEventArgs(DateTime.Now.ToShortTimeString(), error, Mess));
        }
        private void SendMessToHost(int idClient, NameTableOperation act, string Mess)
        {
            OnReceiveMsgDB(this, new OperationTableEventArgs(act, EnumDBError.None, Mess));
        }

        private void SendMessToHost(int idClient, NameChangeOperation act, string Mess)
        {
            OnReceiveMsgDB(this, new OperationTableEventArgs(act, EnumDBError.None, Mess));
        }
        private void SendErrorToHost(OperationTableEventArgs eventArgs)
        {
            OnReceiveErrorDB(this, eventArgs);
        }

        private void SendError(ExceptionLocalDB error, NameTableOperation operation)
        {
            OperationTableEventArgs eventArgs = new OperationTableEventArgs(operation, error.Error, error.Message);

            SendErrorToHost(eventArgs);

            //if (_clients.ContainsKey(error.IdClient))
            //{
            //    _clients[error.IdClient].OperContext.GetCallbackChannel<IServerCallback>().ErrorCallback(eventArgs);
            //    return;
            //}
        }
    }
}
