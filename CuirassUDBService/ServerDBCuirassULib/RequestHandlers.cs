﻿namespace ServerDBCuirassULib
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Runtime.CompilerServices;
    using System.Threading;
    using System.Threading.Tasks;

    using CuirassUDbPackage;

    using CuirassUModelsDBLib;
    using CuirassUModelsDBLib.Errors;
    using CuirassUModelsDBLib.InheritorsEventArgs;
    using CuirassUModelsDBLib.InheritorsExceptions;

    using Grpc.Core;

    using OperationTablesLib;

    using ProtoCuirassULib;

    public partial class ProtoServer : CuirassUDbTransmission.CuirassUDbTransmissionBase
    {
        public override Task<DefaultResponse> ClearTable(NameTableMessage request, ServerCallContext context)
        {
            var nameTable = request.Table.ConvertToDBModel();
            //var IdClient = GetClientId(context);
            var IdClient = 1;

            if (dicOperTables == null) 
                throw new InvalidOperationException("dicOperTables is null");
            lock (dicOperTables[nameTable])
            {
                try
                {
                    SendMessToHost(IdClient, NameTableOperation.Clear, nameTable.ToString());
                    //if (!_clients.ContainsKey(IdClient))
                    //{
                    //    SendErrorOfClient(Errors.EnumServerError.ClientAbsent, "");
                    //    return Task.FromResult(new DefaultResponse() { IsSucceeded = false });
                    //}

                    Dictionary<NameTable, int> keyValuePairs = new Dictionary<NameTable, int>();

                    foreach (var table in dicOperTables)
                    {
                        if (table.Key == nameTable)
                            continue;
                        keyValuePairs.Add(table.Key, table.Value.Load(IdClient).ListRecords.Count);
                    }

                    dicOperTables[nameTable].Clear(IdClient);

                    foreach (var table in keyValuePairs)
                    {
                        if (dicOperTables[table.Key].Load(IdClient).ListRecords.Count != table.Value)
                        {
                            //Task.Delay(100);
                            dicOperTables[table.Key].UpDate(IdClient);
                        }
                    }
                    SendMessToHost(IdClient, NameTableOperation.Clear, "Ok");
                    return Task.FromResult(new DefaultResponse() { IsSucceeded = true });
                }
                catch (ExceptionLocalDB except)
                {
                    SendError(except, NameTableOperation.Clear);
                    throw new RpcException(new Status(StatusCode.Aborted, except.Message));
                    //return Task.FromResult(new DefaultResponse() { IsSucceeded = false });
                    //SendError(except, NameTableOperation.Clear);
                }
                catch (Exception error)
                {
                    SendErrorOfClient(EnumServerError.UnknownError, error.Message);
                    throw new RpcException(new Status(StatusCode.Aborted, error.Message));
                    //return Task.FromResult(new DefaultResponse() { IsSucceeded = false });
                }
            }
        }

        public override Task<LoadMessage> LoadTable(NameTableMessage request, ServerCallContext context)
        {
            var nameTable = TypesConverter.ConvertToDBModel(request.Table);
            var result = new LoadMessage() { Table = request.Table };

            //var IdClient = GetClientId(context);
            var IdClient = 1;
            try
            {
                SendMessToHost(IdClient, NameTableOperation.Load, nameTable.ToString());
                //if (!_clients.ContainsKey(IdClient))
                //{
                //    SendErrorOfClient(Errors.EnumServerError.ClientAbsent, "");
                //    return Task.FromResult(result); //отправлять ошибку
                //}
                if (dicOperTables == null)
                    throw new InvalidOperationException("dicOperTables is null");
                ClassDataCommon data = dicOperTables[nameTable].Load(IdClient);
                result.Records.AddRange(TypesConverter.ConvertToProto(data, nameTable)); //проверить

                return Task.FromResult(result);
            }
            catch (ExceptionLocalDB except)
            {
                SendError(except, NameTableOperation.Load);
                throw new RpcException(new Status(StatusCode.Aborted, except.Message));
                // return Task.FromResult(result); //отправлять ошибку

                //SendErrorToHost(new OperationTableEventArgs(NameTableOperation.Load, except.Error, except.Message));

                //throw new FaultException<InheritorsException.ExceptionWCF>(new InheritorsException.ExceptionWCF(except));
            }
            catch (Exception error)
            {
                throw new RpcException(new Status(StatusCode.Aborted, error.Message));
                // return Task.FromResult(result); //отправлять ошибку

                //SendErrorToHost(new OperationTableEventArgs(NameTableOperation.Load, error.Message));

                //throw new FaultException<InheritorsException.ExceptionWCF>(new InheritorsException.ExceptionWCF(error.Message));
            }

        }



        public override Task<DefaultResponse> ChangeRecord(ChangeRecordMessage request, ServerCallContext context)
        {
            var nameTable = TypesConverter.ConvertToDBModel(request.Table);
            var nameOperation = request.Operation.ConvertToDBModel();
            var record = request.Record.ConvertToDBModel(nameTable);
            NameTableOperation operation = NameTableOperation.None;
            //var IdClient = GetClientId(context);
            var IdClient = 1;
            if (dicOperTables == null)
                throw new InvalidOperationException("dicOperTables is null");
            lock (dicOperTables[nameTable])
            {
                try
                {
                    if (nameTable == NameTable.TableRes || nameTable == NameTable.TableResDistribution || nameTable == NameTable.TableTrackDistribution)
                    {
                        SendMessToHost(IdClient, nameOperation, nameTable.ToString() + ", Id=" + record.Id);
                    }
                    else
                    {
                        SendMessToHost(IdClient, nameOperation, nameTable.ToString());
                    }

                    //if (!_clients.ContainsKey(IdClient))
                    //{
                    //    SendErrorOfClient(Errors.EnumServerError.ClientAbsent, "");
                    //    return Task.FromResult(new DefaultResponse() { IsSucceeded = false });
                    //}

                    switch (nameOperation)
                    {
                        case NameChangeOperation.Add:

                            operation = NameTableOperation.Add;
                            dicOperTables[nameTable].Add(record, IdClient);

                            break;
                        case NameChangeOperation.Change:
                            operation = NameTableOperation.Change;
                            dicOperTables[nameTable].Change(record, IdClient);
                            
                            break;
                        case NameChangeOperation.Delete:
                            operation = NameTableOperation.Delete;
                            Action actionDelete = () => dicOperTables[nameTable].Delete(record, IdClient);
                            
                            //хранит кол-во записей
                            Dictionary<NameTable, int> keyValuePairs = new Dictionary<NameTable, int>();

                            foreach (var table in dicOperTables)
                            {
                                if (table.Key == nameTable)
                                    continue;
                                keyValuePairs.Add(table.Key, table.Value.Load(IdClient).ListRecords.Count);
                            }

                            actionDelete();

                            foreach (var table in keyValuePairs)
                            {
                                if (dicOperTables[table.Key].Load(IdClient).ListRecords.Count != table.Value)
                                {
                                    //Task.Delay(100);
                                    dicOperTables[table.Key].UpDate(IdClient);
                                }
                            }

                            break;
                    }

                    Task.Run(() => SendMessToHost(IdClient, nameOperation, "Ok"));
                    return Task.FromResult(new DefaultResponse() { IsSucceeded = true });
                }
                catch (ExceptionLocalDB except)
                {
                    SendError(except, operation);
                    throw new RpcException(new Status(StatusCode.Aborted, except.Message));
                    //return Task.FromResult(new DefaultResponse() { IsSucceeded = false });
                }
                catch (Exception error)
                {
                    SendErrorOfClient(EnumServerError.UnknownError, error.Message);
                    throw new RpcException(new Status(StatusCode.Aborted, error.Message));
                    //return Task.FromResult(new DefaultResponse() { IsSucceeded = false });
                }
            }

        }


        public override Task<DefaultResponse> ChangeRange(ChangeRangeMessage request, ServerCallContext context)
        {
            var nameTable = TypesConverter.ConvertToDBModel(request.Table);
            var nameOperation = request.Operation.ConvertToDBModel();
            var records = request.Records.ConvertToDBModel(nameTable);
            NameTableOperation operation = NameTableOperation.None;
            //var IdClient = GetClientId(context);
            var IdClient = 1;
            if (dicOperTables == null)
                throw new InvalidOperationException("dicOperTables is null");
            lock (dicOperTables[nameTable])
            {
                try
                {
                    SendMessToHost(IdClient, nameOperation, nameTable.ToString());

                    //if (!_clients.ContainsKey(IdClient))
                    //{
                    //    SendErrorOfClient(Errors.EnumServerError.ClientAbsent, "");
                    //    return Task.FromResult(new DefaultResponse() { IsSucceeded = false });
                    //}

                    switch (nameOperation)
                    {
                        case NameChangeOperation.Add:

                            operation = NameTableOperation.AddRange;
                            dicOperTables[nameTable].AddRange(records, IdClient);
                            break;

                        case NameChangeOperation.Change:
                            operation = NameTableOperation.ChangeRange;
                            dicOperTables[nameTable].ChangeRange(records, IdClient);
                            break;
                        case NameChangeOperation.Delete:
                            operation = NameTableOperation.RemoveRange;
                            //хранит кол-во записей
                            Dictionary<NameTable, int> keyValuePairs = new Dictionary<NameTable, int>();

                            foreach (var table in dicOperTables)
                            {
                                if (table.Key == nameTable)
                                    continue;
                                keyValuePairs.Add(table.Key, table.Value.Load(IdClient).ListRecords.Count);
                            }

                            dicOperTables[nameTable].RemoveRange(records, IdClient);

                            foreach (var table in keyValuePairs)
                            {

                                if (dicOperTables[table.Key].Load(IdClient).ListRecords.Count != table.Value)
                                {
                                    //Task.Delay(100);
                                    dicOperTables[table.Key].UpDate(IdClient);
                                }
                            }
                            break;
                    }

                    Task.Run(() => SendMessToHost(IdClient, nameOperation, "Ok"));
                    return Task.FromResult(new DefaultResponse() { IsSucceeded = true });
                }
                catch (ExceptionLocalDB except)
                {
                    SendError(except, operation);
                    throw new RpcException(new Status(StatusCode.Aborted, except.Message));
                    //return Task.FromResult(new DefaultResponse() { IsSucceeded = false });
                }
                catch (Exception error)
                {
                    SendErrorOfClient(EnumServerError.UnknownError, error.Message);
                    throw new RpcException(new Status(StatusCode.Aborted, error.Message));
                    //return Task.FromResult(new DefaultResponse() { IsSucceeded = false });
                }
            }

        }



        private EventHandler<DataEventArgs>? UpDataHandler = null;

        public override async Task Subscribe(DefaultRequest request, IServerStreamWriter<LoadMessage> responseStream, ServerCallContext context)
        {
            try
            {
                //UpDataHandler = async (sender, args) => await this.HandlerUpData(responseStream, args).ConfigureAwait(false);
                UpDataHandler = async (sender, args) => await this.HandlerUpData(responseStream, args).ConfigureAwait(false);
                Operation.OnReceiveData += UpDataHandler;
                
                await AwaitCancellation(context.CancellationToken, UpDataHandler).ConfigureAwait(false);
                var t = 9;
            }
            catch (OperationCanceledException)
            {
                //transferStatusMessage.Status = TransferStatus.Cancelled;
            }
            catch (RpcException rpcEx)
            {
                //if (rpcEx.StatusCode == StatusCode.Cancelled)
                //{
                //    transferStatusMessage.Status = TransferStatus.Cancelled;
                //}
                //else
                //{
                //    _logger.LogError($"Exception while processing image file '{ImageFileName}'. Exception: '{requestStream}'");
                //    transferStatusMessage.Status = TransferStatus.Failure;
                //}
            }
            catch (Exception ex)
            {
                Console.WriteLine("Exception on Server:" + ex.Message);
            }
        }

        //private static Task AwaitCancellation(CancellationToken token, EventHandler<DataEventArgs> func)
        //{
        //    var completion = new TaskCompletionSource<object?>();
        //    token.Register(() => {
        //        completion.SetResult(null);
        //        Operation.OnReceiveData -= func;
        //        //Task.Run(() => SendStateClient(removedClient?.Name ?? string.Empty, ServerEventArgs.ActClient.Disconnect);

        //    });
        //    return completion.Task;
        //}

        private Task AwaitCancellation(CancellationToken token, EventHandler<DataEventArgs> func)
        {
            var completion = new TaskCompletionSource<object?>();
            
            token.Register(() => {
                completion.SetResult(null);
                Operation.OnReceiveData -= func;
                //Task.Run(() => SendStateClient(removedClient?.Name ?? string.Empty, ServerEventArgs.ActClient.Disconnect);

            });
            return completion.Task;
        }
        //private async Task AwaitCancellation(CancellationToken token, EventHandler<DataEventArgs> func)
        //{
        //    var completion = new TaskCompletionSource<object?>();
        //    token.Register(() => {
        //            completion.SetResult(null);
        //            Operation.OnReceiveData -= func;
        //            //Task.Run(() => SendStateClient(removedClient?.Name ?? string.Empty, ServerEventArgs.ActClient.Disconnect);

        //        });
        //    //return completion.Task;
        //}


        private async Task HandlerUpData(IServerStreamWriter<LoadMessage> responseStream, DataEventArgs eventArgs)
        {
            try
            {
                var result = new LoadMessage() { Table = eventArgs.Name.ConvertToProto() };
                result.Records.AddRange(eventArgs.AbstractData.ConvertToProto(eventArgs.Name));

                await responseStream.WriteAsync(result);
                await Task.Delay(TimeSpan.FromMilliseconds(1));

            }
            catch (RpcException ex)
            {
                Console.WriteLine(ex);
            }
            catch (Exception excp)
            {
                SendErrorToHost(new OperationTableEventArgs(NameTableOperation.Update, excp.Message + $" {excp.InnerException?.Message} "));
            }
        }


        //private int GetClientId(ServerCallContext context)
        //{
        //    var metadataValue = context.RequestHeaders.FirstOrDefault(e => e.Key == "clientid").Value;
        //    var id = metadataValue != null ? Convert.ToInt32(metadataValue) : 0;
        //    return id;
        //}

       
    }
}
