﻿using CuirassUModelsDBLib;

namespace OperationTablesLib
{
    public interface ITableAction
    {
        void Add(AbstractCommonTable record, int idClient);

        void AddRange(ClassDataCommon data, int idClient);

        void RemoveRange(ClassDataCommon data, int idClient);

        void Delete(AbstractCommonTable record, int idClient);

        void Clear(int idClient);

        void Change(AbstractCommonTable record, int idClient);

        void ChangeRange(ClassDataCommon data, int idClient);

        void UpDate(int idClient);

        ClassDataCommon Load(int idClient);
    }
}
