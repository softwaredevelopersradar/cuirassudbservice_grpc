﻿
namespace OperationTablesLib
{
    using System;
    using System.Collections.Generic;

    using CuirassUModelsDBLib;
    using CuirassUModelsDBLib.Errors;
    using CuirassUModelsDBLib.InheritorsExceptions;

    using Microsoft.EntityFrameworkCore;
    public class OperationTableResDistribution : OperationTableDb<TableResDistribution>
    {
        public override void Change(AbstractCommonTable record, int idClient)
        {
            try
            {
                lock (DataBase)
                {
                    DbSet<TableResDistribution> tableSource = DataBase.GetTable<TableResDistribution>(Name);

                    TableResDistribution? rec = tableSource.Find(record.GetKey());
                    if (rec == null)
                        throw new ExceptionLocalDB(idClient, EnumDBError.RecordNotFound);


                    tableSource.Remove(rec);
                    DataBase.SaveChanges();
                    (rec as AbstractCommonTable).Update(record);
                    tableSource.Add(rec);
                    DataBase.SaveChanges();
                }

                UpDate(idClient);

                return;
            }
            catch (ExceptionLocalDB exception)
            {
                throw exception;
            }
            catch (DbUpdateException exDb)
            {
                throw new ExceptionLocalDB(idClient, exDb.InnerException?.Message ?? string.Empty);
            }
            catch (Exception ex)
            {
                throw new ExceptionLocalDB(idClient, ex.Message);
            }
        }


        public override void AddRange(ClassDataCommon data, int idClient)
        {
            try
            {
                List<TableResDistribution> AddRange = new List<TableResDistribution>();
                lock (DataBase)
                {
                    DbSet<TableResDistribution> Table = DataBase.GetTable<TableResDistribution>(Name);
                    foreach (var record in data.ListRecords)
                    {
                        var rec = Table.Find(record.GetKey());
                        if (rec != null)
                        {
                            rec.Update(record);
                            Table.Update(rec);
                        }
                        else
                        {
                            Table.Add(record as TableResDistribution ?? throw new InvalidOperationException());
                            AddRange.Add(record as TableResDistribution ?? throw new InvalidOperationException());
                        }
                    }
                    DataBase.SaveChanges();
                }
                UpDate(idClient);
                //SendRange(this, new DataEventArgs(Name, ClassDataCommon.ConvertToListAbstractCommonTable(AddRange)));
            }
            catch (ExceptionLocalDB exception)
            {
                throw exception;
            }
            catch (Exception ex)
            {
                throw new ExceptionLocalDB(idClient, ex.Message);
            }
        }
    }
}
