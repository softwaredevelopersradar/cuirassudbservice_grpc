﻿namespace OperationTablesLib
{
    using CuirassUModelsDBLib;
    using Microsoft.EntityFrameworkCore;
    public class OperationGlobalPropertiesRRS : OperationTableDb<GlobalPropertiesRRS>
    {
        public override void Add(AbstractCommonTable record, int idClient)
        {
            GlobalPropertiesRRS? recFind;
            lock (DataBase)
            {
                DbSet<GlobalPropertiesRRS> Table = DataBase.GetTable<GlobalPropertiesRRS>(Name);
                recFind = Table.Find(record.GetKey());
            }
            if (recFind != null)
                base.Change(record, idClient);
            else
                base.Add(record, idClient);
        }
    }
}
