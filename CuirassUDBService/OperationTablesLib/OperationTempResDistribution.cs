﻿namespace OperationTablesLib
{
    using System;
    using System.Linq;
    using CuirassUModelsDBLib;
    using CuirassUModelsDBLib.InheritorsExceptions;
    using CuirassUModelsDBLib.Errors;

    public class OperationTempResDistribution : OperationTempTable<TableResDistribution>
    {
        public override void RemoveRange(ClassDataCommon data, int idClient)
        {
            try
            {
                foreach (var rec in data.ListRecords)
                {
                    int key = (int)(rec.GetKey().FirstOrDefault() ?? throw new InvalidOperationException());
                    if (TempTable.ContainsKey(key))
                    {
                        TempTable.Remove(key);
                        ClearTrajectory(key);
                    }
                }
                UpDate(idClient);

            }
            catch (ExceptionLocalDB exception)
            {
                throw exception;
            }
            catch (Exception ex)
            {
                throw new ExceptionLocalDB(idClient, ex.Message);
            }
        }


        public override void Clear(int idClient)
        {
            try
            {
                foreach (var record in TempTable)
                {
                    ClearTrajectory(record.Key);
                }

                TempTable.Clear();
                UpDate(idClient);
            }
            catch (ExceptionLocalDB exception)
            {
                throw exception;
            }
            catch (Exception ex)
            {
                throw new ExceptionLocalDB(idClient, ex.Message);
            }
        }


        public override void Delete(AbstractCommonTable record, int idClient)
        {
            try
            {
                int key = (int)(record.GetKey().FirstOrDefault() ?? throw new InvalidOperationException());
                if (!TempTable.ContainsKey(key))
                {
                    throw new ExceptionLocalDB(idClient, EnumDBError.RecordNotFound);
                }

                TempTable.Remove(key);
                UpRecord(idClient, record, NameChangeOperation.Delete);
                //UpDate(idClient);

                ClearTrajectory(key);
            }
            catch (ExceptionLocalDB exception)
            {
                throw exception;
            }
            catch (Exception ex)
            {
                throw new ExceptionLocalDB(idClient, ex.Message);
            }
        }


        private void ClearTrajectory(int key)
        {
            var listRemove = OperationTempTrackDistribution.TempTable.Where(t => t.Value.TableResId == key).ToList();
            foreach (var rec in listRemove)
            {
                OperationTempTrackDistribution.TempTable.Remove(rec);
            }
        }
    }
}
