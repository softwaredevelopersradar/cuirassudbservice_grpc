﻿using System;
using CuirassUModelsDBLib;
using Microsoft.EntityFrameworkCore;

namespace OperationTablesLib
{
    public class TablesContext : DbContext
    {
        #region DbSet
        public DbSet<TableSectorsRecon>? TSectorsRecon { get; set; }
        public DbSet<GlobalPropertiesRRS>? TGlobalPropertiesRRS { get; set; }
        public DbSet<GlobalProperties>? TGlobalProperties { get; set; }
        public DbSet<TableFreqForbidden>? TFreqForbidden { get; set; }
        public DbSet<TableFreqRangesRecon>? TFreqRangesRecon { get; set; }
        public DbSet<TableResArchive>? TResArchive { get; set; }
        public DbSet<TableTrackArchive>? TTrackArchive { get; set; }
        public DbSet<TableResPattern>? TResPattern { get; set; }
        public DbSet<TableRemotePoints>? TRemotePoints { get; set; }
        public DbSet<TableResDistribution>? TResDistribution { get; set; }
        public DbSet<TableTrackDistribution>? TTrackDistribution { get; set; }
        #endregion

        public DbSet<T> GetTable<T>(NameTable name) where T : class
        {
            switch (name)
            {
                case NameTable.TableFreqForbidden:
                    return this.TFreqForbidden as DbSet<T> ?? throw new InvalidOperationException(" DbSet is null!");
                case NameTable.TableFreqRangesRecon:
                    return this.TFreqRangesRecon as DbSet<T> ?? throw new InvalidOperationException(" DbSet is null!");
                case NameTable.TableSectorsRecon:
                    return this.TSectorsRecon as DbSet<T> ?? throw new InvalidOperationException(" DbSet is null!");
                case NameTable.GlobalPropertiesRRS:
                    return this.TGlobalPropertiesRRS as DbSet<T> ?? throw new InvalidOperationException(" DbSet is null!");
                case NameTable.GlobalProperties:
                    return this.TGlobalProperties as DbSet<T> ?? throw new InvalidOperationException(" DbSet is null!");
                case NameTable.TableResArchive:
                    return TResArchive as DbSet<T> ?? throw new InvalidOperationException(" DbSet is null!");
                case NameTable.TableTrackArchive:
                    return TTrackArchive as DbSet<T> ?? throw new InvalidOperationException(" DbSet is null!");
                case NameTable.TableResPattern:
                    return TResPattern as DbSet<T> ?? throw new InvalidOperationException(" DbSet is null!");
                case NameTable.TableRemotePoints:
                    return TRemotePoints as DbSet<T> ?? throw new InvalidOperationException(" DbSet is null!");
                case NameTable.TableResDistribution:
                    return TResDistribution as DbSet<T> ?? throw new InvalidOperationException(" DbSet is null!");
                case NameTable.TableTrackDistribution:
                    return TTrackDistribution as DbSet<T> ?? throw new InvalidOperationException(" DbSet is null!");
                default:
                    throw new InvalidOperationException(" DbSet is null!");
            }
        }

        public TablesContext()
        {
            try
            {
               SQLitePCL.Batteries.Init();
            }
            catch (Exception ex)
            {
                Console.Write($"Error: {ex.Message}");
            }

            Database.EnsureCreated();
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.EnableSensitiveDataLogging(true);
            var temp = System.IO.Directory.GetCurrentDirectory();
            var location = System.Reflection.Assembly.GetExecutingAssembly();
            var path = "Filename=" + System.IO.Directory.GetCurrentDirectory() + "\\CuirassU_DB.db";
            optionsBuilder.UseSqlite(path);
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<TableResPattern>().Property(rec => rec.Id).ValueGeneratedOnAdd();
            modelBuilder.Entity<TableResArchive>().Property(rec => rec.Id).ValueGeneratedOnAdd();
            modelBuilder.Entity<TableTrackArchive>().Property(rec => rec.Id).ValueGeneratedOnAdd();
            modelBuilder.Entity<TableTrackArchive>().OwnsOne(t => t.Coordinates);
            modelBuilder.Entity<TableResArchive>().OwnsOne(t => t.Pulse);
            modelBuilder.Entity<TableResArchive>().OwnsOne(t => t.Series);
            modelBuilder.Entity<TableResArchive>().HasMany(rec => rec.Track).WithOne().OnDelete(DeleteBehavior.Cascade);
            modelBuilder.Entity<TableResPattern>().OwnsOne(t => t.Pulse);
            modelBuilder.Entity<TableResPattern>().OwnsOne(t => t.Series);

            modelBuilder.Entity<TableFreqForbidden>().OwnsOne(t => t.Pulse);

            modelBuilder.Entity<TableRemotePoints>().Property(rec => rec.Id).ValueGeneratedOnAdd();
            modelBuilder.Entity<TableRemotePoints>().OwnsOne(t => t.Coordinates);

            modelBuilder.Entity<GlobalPropertiesRRS>().OwnsOne(t => t.Location);

            modelBuilder.Entity<GlobalProperties>().OwnsOne(t => t.Location);
            modelBuilder.Entity<GlobalProperties>().OwnsOne(t => t.Calibration);
            modelBuilder.Entity<GlobalProperties>().OwnsOne(t => t.IQData);
            modelBuilder.Entity<GlobalProperties>().OwnsOne(t => t.RadioIntelegence);
            modelBuilder.Entity<GlobalProperties>().OwnsOne(t => t.CompassOrientation);

            modelBuilder.Entity<TableResDistribution>().Property(rec => rec.Id).ValueGeneratedOnAdd();
            modelBuilder.Entity<TableTrackDistribution>().Property(rec => rec.Id).ValueGeneratedOnAdd();
            modelBuilder.Entity<TableTrackDistribution>().OwnsOne(t => t.Coordinates);
            modelBuilder.Entity<TableResDistribution>().OwnsOne(t => t.Pulse);
            modelBuilder.Entity<TableResDistribution>().OwnsOne(t => t.Series);
            modelBuilder.Entity<TableResDistribution>().HasMany(rec => rec.Track).WithOne().OnDelete(DeleteBehavior.Cascade);


            modelBuilder.Entity<GlobalPropertiesRRS>(b =>
            {
                b.HasData(new
                {
                    Id = 1
                });

                b.OwnsOne(e => e.Location).HasData(new
                {
                    GlobalPropertiesRRSId = 1,
                    CoordinateSystem = CoordinateSystem.MGRS,
                    Altitude = (double)-1,
                    Latitude = (double)500,
                    Longitude = (double)500,
                    CourseAngle = (float)0,
                    CompassAngle = (float)0
                }); 
            });

            modelBuilder.Entity<GlobalProperties>(b =>
            {
                b.HasData(new
                  {
                      Id = 1
                  });

                b.OwnsOne(e => e.Calibration).HasData(new
                  {
                      GlobalPropertiesId = 1,
                      Type = CalibrationTypes.Generator
                  });

                b.OwnsOne(e => e.Location).HasData(new
                   {
                       GlobalPropertiesId = 1,
                       Altitude = (double)-1,
                       Latitude = (double)500,
                       Longitude = (double)500
                   });

                b.OwnsOne(e => e.RadioIntelegence).HasData(new
                   {
                       GlobalPropertiesId = 1,
                       CourseAngle = (float)0,
                       SynchronizeType = SynchronizeTypes.GPS,
                       NarrowFilterWidth = NarrowFilterWidth.FiftyMHz,
                       DurationMin = (float)2000,
                       DurationMax = (float)2000,
                       PeriodMin = (float)10000,
                       PeriodMax = (float)10000,
                       SwitchingTime = (short)100,
                       NumberOfReviews = (short)10,
                       DeviceAnalysisType = DeviceAnalysisTypes.Master,
                       AutoCourseAngle = true

                });

                b.OwnsOne(e => e.IQData).HasData(new
                 {
                     GlobalPropertiesId = 1,
                     TimeRecord = (float)8,
                     PathFolderNbFiles = string.Empty,
                     WaitForStrobe = true,
                     CaptureMode = CaptureMode.Auto,
                     InputBuffer = BufferSizes.x32,
                     OutputBuffer = BufferSizes.x32,
                     Recorder = Recorder.Master
                 });

                b.OwnsOne(e => e.CompassOrientation).HasData(new
                   {
                       GlobalPropertiesId = 1,
                       Azimuth = (float)-1,
                       Bank = (float)-1,
                       Pitch = (float)-1
                });
            });
        }
    }
}
