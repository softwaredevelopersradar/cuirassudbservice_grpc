﻿namespace OperationTablesLib
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using CuirassUModelsDBLib;
    using CuirassUModelsDBLib.Errors;
    using CuirassUModelsDBLib.InheritorsEventArgs;
    using CuirassUModelsDBLib.InheritorsExceptions;

    public class OperationTempTrackDistribution : Operation, ITableAction
    {
        internal static LinkedQueue<KeyValuePair<int, TableTrackDistribution>> TempTable = new LinkedQueue<KeyValuePair<int, TableTrackDistribution>>();

        public OperationTempTrackDistribution() : base()
        {
            IsTemp = true;
            Name = NameTable.TableTrackDistribution;
        }

        public void Add(AbstractCommonTable record, int idClient)
        {
            try
            {
                var castRecord = record as TableTrackDistribution ?? throw new InvalidOperationException( "Record is null!");
                var key = (int)(record.GetKey().FirstOrDefault() ?? throw new InvalidOperationException("Key is null!"));

                var findUav = OperationTempResDistribution.TempTable.Values.Any(t => t.Id == castRecord.TableResId);
                if (!findUav)
                {
                    throw new ExceptionLocalDB(idClient, EnumDBError.SuchResIsAbsent);
                }

                if (TempTable.Any(c => c.Key == key))
                {
                    throw new ExceptionLocalDB(idClient, EnumDBError.RecordExist);
                }
                //var findUAV = OperationTempResDistribution.TempTable.Values.Where(t => t.Id == castRecord.TableResId).ToList().Count;
                //if (findUAV == 0)
                //{
                //    throw new ExceptionLocalDB(idClient, EnumDBError.SuchResIsAbsent);
                //}

                //if (TempTable.Where(c => c.Key == (int)(record.GetKey().FirstOrDefault() ?? throw new InvalidOperationException())).ToList().Count != 0)
                //{
                //    throw new ExceptionLocalDB(idClient, EnumDBError.RecordExist);
                //}

                TempTable.Enqueue(new KeyValuePair<int, TableTrackDistribution>(key, castRecord));

                //if (IsLimited)
                //{
                //    if (TempTable.Count > TableLimit)
                //        TempTable.Dequeue();
                //}

                UpDate(idClient);
            }
            catch (ExceptionLocalDB exception)
            {
                throw exception;
            }
            catch (Exception ex)
            {
                throw new ExceptionLocalDB(idClient, ex.Message);
            }
        }


        public void AddRange(ClassDataCommon data, int idClient)
        {
            try
            {
                foreach (var rec in data.ListRecords)
                {
                    var castRecord = rec as TableTrackDistribution ?? throw new InvalidOperationException("Record is null!");
                    var key = (int)(rec.GetKey().FirstOrDefault() ?? throw new InvalidOperationException("Key is null!"));

                    var foundedRecord = TempTable.FirstOrDefault(c => c.Key == key);
                    if (!foundedRecord.Equals(default(KeyValuePair<int, TableTrackDistribution>)))
                    {
                        foundedRecord.Value.Update(castRecord);
                        //TempTable.Find(foundedRecord).Value.Value.Update(castRecord);
                    }
                    else
                    {
                        TempTable.Enqueue(new KeyValuePair<int, TableTrackDistribution>(key, castRecord));
                        //if (TempTable.Count > 1000)
                        //    TempTable.Dequeue();
                    }
                }
                UpDate(idClient);
            }
            catch (ExceptionLocalDB exception)
            {
                throw exception;
            }
            catch (Exception ex)
            {
                throw new ExceptionLocalDB(idClient, ex.Message);
            }
        }


        public void RemoveRange(ClassDataCommon data, int idClient)
        {
            try
            {
                foreach (var rec in data.ListRecords)
                {
                    var key = (int)(rec.GetKey().FirstOrDefault() ?? throw new InvalidOperationException("Key is null!"));
                    var foundedRecord = TempTable.FirstOrDefault(c => c.Key == key);

                    if (!foundedRecord.Equals(default(KeyValuePair<int, TableTrackDistribution>)))
                    {
                        TempTable.Remove(foundedRecord);
                    }
                }
                UpDate(idClient);
            }
            catch (ExceptionLocalDB exception)
            {
                throw exception;
            }
            catch (Exception ex)
            {
                throw new ExceptionLocalDB(idClient, ex.Message);
            }
        }


        public void Change(AbstractCommonTable record, int idClient)
        {
            try
            {
                var castRecord = record as TableTrackDistribution ?? throw new InvalidOperationException("Record is null!");
                var key = (int)(record.GetKey().FirstOrDefault() ?? throw new InvalidOperationException("Key is null!"));

                var foundedRecord = TempTable.FirstOrDefault(c => c.Key == key);
                if (foundedRecord.Equals(default(KeyValuePair<int, TableTrackDistribution>)))
                {
                    throw new ExceptionLocalDB(idClient, EnumDBError.RecordNotFound);
                }
                //int index = TempTable.Select((item, inx) => new { item, inx })
                //  .First(x => x.item.Equals(foundedRecord)).inx;

                foundedRecord.Value.Update(castRecord); //TODO: check
                //TempTable.Find(foundedRecord).Value.Value.Update(record as TableTrackDistribution); //TODO: check
                UpDate(idClient);
            }
            catch (ExceptionLocalDB exception)
            {
                throw exception;
            }
            catch (Exception ex)
            {
                throw new ExceptionLocalDB(idClient, ex.Message);
            }
        }

        public void ChangeRange(ClassDataCommon data, int idClient)
        {
            try
            {
                foreach (var record in data.ToList<TableTrackDistribution>())
                {
                    var castRecord = record as TableTrackDistribution ?? throw new InvalidOperationException("Record is null!");
                    var key = (int)(record.GetKey().FirstOrDefault() ?? throw new InvalidOperationException("Key is null!"));

                    var foundedRecord = TempTable.FirstOrDefault(c => c.Key == key);
                    if (!foundedRecord.Equals(default(KeyValuePair<int, TableTrackDistribution>)))
                    {
                        foundedRecord.Value.Update(castRecord); //TODO: check
                        //TempTable.Find(foundedRecord).Value.Value.Update(record as TableTrackDistribution); //TODO: check
                    }
                    else
                    {
                        TempTable.Enqueue(new KeyValuePair<int, TableTrackDistribution>(key, castRecord));
                    }
                }
                UpDate(idClient);
            }
            catch (ExceptionLocalDB exception)
            {
                throw exception;
            }
            catch (Exception ex)
            {
                throw new ExceptionLocalDB(idClient, ex.Message);
            }
        }


        public void Clear(int idClient)
        {
            try
            {
                TempTable.Clear();
                UpDate(idClient);
            }
            catch (ExceptionLocalDB exception)
            {
                throw exception;
            }
            catch (Exception ex)
            {
                throw new ExceptionLocalDB(idClient, ex.Message);
            }
        }


        public void Delete(AbstractCommonTable record, int idClient)
        {
            try
            {
                var key = (int)(record.GetKey().FirstOrDefault() ?? throw new InvalidOperationException("Key is null!"));
                var foundedRecord = TempTable.FirstOrDefault(c => c.Key == key);
                if (foundedRecord.Equals(default(KeyValuePair<int, TableTrackDistribution>)))
                {
                    throw new ExceptionLocalDB(idClient, EnumDBError.RecordNotFound);
                }

                TempTable.Remove(foundedRecord);
                UpRecord(idClient, record, NameChangeOperation.Delete);
            }
            catch (ExceptionLocalDB exception)
            {
                throw exception;
            }
            catch (Exception ex)
            {
                throw new ExceptionLocalDB(idClient, ex.Message);
            }
        }


        public void UpDate(int idClient)
        {
            try
            {
                base.SendUpData(this, new DataEventArgs(Name, ClassDataCommon.ConvertToListAbstractCommonTable(TempTable.Select(kv => kv.Value).ToList()) ?? throw new InvalidOperationException("UpDate failed.")));
            }
            catch (ExceptionLocalDB exception)
            {
                throw exception;
            }
            catch (Exception ex)
            {
                throw new ExceptionLocalDB(idClient, ex.Message);

            }
        }


        public void UpRecord(int idClient, AbstractCommonTable record, NameChangeOperation action)
        {
            try
            {
                SendUpRecord(this, new RecordEventArgs(Name, record, action));
            }
            catch (ExceptionLocalDB exception)
            {
                throw exception;
            }
            catch (Exception ex)
            {
                throw new ExceptionLocalDB(idClient, ex.Message);
            }
        }


        public ClassDataCommon Load(int idClient)
        {
            try
            {
                var temp = TempTable.Select(kv => kv.Value).ToList();
                //var temp = TempTable.ToList().Select(kv => kv.Value).ToList();
                return ClassDataCommon.ConvertToListAbstractCommonTable(temp) ?? throw new InvalidOperationException("Load from DB failed!");
            }
            catch (ExceptionLocalDB exception)
            {
                throw exception;
            }
            catch (Exception ex)
            {
                throw new ExceptionLocalDB(idClient, ex.Message);
            }
        }
    }
}
