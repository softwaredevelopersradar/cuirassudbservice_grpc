﻿namespace OperationTablesLib
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    
    using CuirassUModelsDBLib;
    using CuirassUModelsDBLib.Errors;
    using CuirassUModelsDBLib.InheritorsEventArgs;
    using CuirassUModelsDBLib.InheritorsExceptions;

    using Microsoft.EntityFrameworkCore;

    public class OperationTableDb<T> : Operation, ITableAction where T : AbstractCommonTable
    {
        public OperationTableDb()
        {
            object[] attrs = typeof(T).GetCustomAttributes(typeof(InfoTableAttribute), false);
            foreach (InfoTableAttribute info in attrs)
            {
                Name = info.Name;
            }
        }

        public virtual void Add(AbstractCommonTable record, int idClient)
        {
            try
            {
                //if (record == null) throw new InvalidOperationException();
                lock (DataBase)
                {
                    DbSet<T> Table = DataBase.GetTable<T>(Name);
                    if (Table.Find(record.GetKey()) != null)
                    {
                        throw new ExceptionLocalDB(idClient, EnumDBError.RecordExist);
                    }
                    Table.Add(record as T ?? throw new InvalidOperationException());
                    DataBase.SaveChanges();
                }
                UpDate(idClient);
            }
            catch (ExceptionLocalDB exception)
            {
                if (exception.InnerException != null)
                    throw exception.InnerException;
                throw exception;
            }
            catch (DbUpdateException exDb)
            {
                throw new ExceptionLocalDB(idClient, exDb.InnerException?.Message ?? string.Empty);
            }
            catch (Exception ex)
            {
                throw new ExceptionLocalDB(idClient, ex.Message);
            }
        }

        public virtual void AddRange(ClassDataCommon data, int idClient)
        {
            try
            {
                lock (DataBase)
                {
                    DbSet<T> Table = DataBase.GetTable<T>(Name);

                    Table.AddRange(data.ToList<T>());
                    DataBase.SaveChanges();
                }
                UpDate(idClient);
            }
            catch (ExceptionLocalDB exception)
            {
                throw exception;
            }
            catch (DbUpdateException exDb)
            {
                throw new ExceptionLocalDB(idClient, exDb.InnerException?.Message ?? string.Empty);
            }
            catch (Exception ex)
            {
                throw new ExceptionLocalDB(idClient, ex.Message);
            }
        }

        public virtual void Change(AbstractCommonTable record, int idClient)
        {
            try
            {
                lock (DataBase)
                {
                    DbSet<T> Table = DataBase.GetTable<T>(Name);

                    T? rec = Table.Find(record.GetKey());
                    if (rec == null)
                        throw new ExceptionLocalDB(idClient, EnumDBError.RecordNotFound);

                    (rec as AbstractCommonTable).Update(record);
                    Table.Update(rec);
                    DataBase.SaveChanges();
                }
                UpDate(idClient);
                return;
            }
            catch (ExceptionLocalDB exception)
            {
                throw exception;
            }
            catch (DbUpdateException exDb)
            {
                throw new ExceptionLocalDB(idClient, exDb.InnerException?.Message ?? string.Empty);
            }
            catch (Exception ex)
            {
                throw new ExceptionLocalDB(idClient, ex.Message);
            }
        }

        public virtual void ChangeRange(ClassDataCommon data, int idClient)
        {
            try
            {
                lock (DataBase)
                {
                    DbSet<T> Table = DataBase.GetTable<T>(Name);
                    List<T> rangeUpdate = new List<T>();

                    foreach (var record in data.ToList<T>())
                    {
                        T? rec = Table.Find(record.GetKey());
                        if (rec != null)
                        {
                            (rec as AbstractCommonTable).Update(record);
                            rangeUpdate.Add(rec);
                        }
                    }

                    Table.UpdateRange(rangeUpdate);
                    DataBase.SaveChanges();
                }
                UpDate(idClient);
                return;
            }
            catch (ExceptionLocalDB exception)
            {
                throw exception;
            }
            catch (DbUpdateException exDb)
            {
                throw new ExceptionLocalDB(idClient, exDb.InnerException?.Message ?? string.Empty);
            }
            catch (Exception ex)
            {
                throw new ExceptionLocalDB(idClient, ex.Message);
            }
        }

        public virtual void Clear(int idClient)
        {
            try
            {
                lock (DataBase)
                {
                    DbSet<T> Table = DataBase.GetTable<T>(Name);

                    Table.RemoveRange(Table.ToList());
                    DataBase.SaveChanges();
                }
                UpDate(idClient);
            }
            catch (ExceptionLocalDB exception)
            {
                throw exception;
            }
            catch (DbUpdateException exDb)
            {
                throw new ExceptionLocalDB(idClient, exDb.InnerException?.Message ?? string.Empty);
            }
            catch (Exception ex)
            {
                throw new ExceptionLocalDB(idClient, ex.Message);
            }
        }

        public virtual void Delete(AbstractCommonTable record, int idClient)
        {
            try
            {
                lock (DataBase)
                {
                    DbSet<T> Table = DataBase.GetTable<T>(Name);

                    T? rec = Table.Find(record.GetKey());
                    if (rec != null)
                    {
                        Table.Remove(rec);
                        DataBase.SaveChanges();
                    }
                    UpDate(idClient);
                    return;
                }
                throw new ExceptionLocalDB(idClient, EnumDBError.RecordNotFound);
            }
            catch (ExceptionLocalDB exception)
            {
                throw exception;
            }
            catch (DbUpdateException exDb)
            {
                throw new ExceptionLocalDB(idClient, exDb.InnerException?.Message ?? string.Empty);
            }
            catch (Exception ex)
            {
                throw new ExceptionLocalDB(idClient, ex.Message);
            }
        }

        public virtual void RemoveRange(ClassDataCommon data, int idClient)
        {
            try
            {
                lock (DataBase)
                {
                    DbSet<T> Table = DataBase.GetTable<T>(Name);

                    List<T> rangeRemove = new List<T>();
                    foreach (var record in data.ToList<T>())
                    {
                        T? rec = Table.Find(record.GetKey());
                        if (rec != null)
                            rangeRemove.Add(rec);
                    }
                    if (rangeRemove.Count == 0)
                        throw new ExceptionLocalDB(idClient, EnumDBError.RecordNotFound);
                    Table.RemoveRange(rangeRemove);
                    DataBase.SaveChanges();
                }
                UpDate(idClient);
            }
            catch (ExceptionLocalDB exception)
            {
                throw exception;
            }
            catch (DbUpdateException exDb)
            {
                throw new ExceptionLocalDB(idClient, exDb.InnerException?.Message ?? string.Empty);
            }
            catch (Exception ex)
            {
                throw new ExceptionLocalDB(idClient, ex.Message);
            }
        }

        public virtual ClassDataCommon Load(int idClient)
        {
            try
            {
                lock (DataBase)
                {
                    DbSet<T> Table = DataBase.GetTable<T>(Name);

                    // проверка на наличие вложенной таблицы 
                    var properties = typeof(T).GetProperties();
                    foreach (var property in properties)
                    {
                        if (property.PropertyType.GetInterface(typeof(IList).Name) != null && !property.PropertyType.IsArray)
                        {
                            // если найдена, то испльзуем жадную загрузку связных таблиц
                            return ClassDataCommon.ConvertToListAbstractCommonTable(Table.Include(property.Name).ToList()) ?? throw new InvalidOperationException();
                        }
                    }
                    return ClassDataCommon.ConvertToListAbstractCommonTable(Table.ToList()) ?? throw new InvalidOperationException();
                }
            }
            catch (ExceptionLocalDB exception)
            {
                throw exception;
            }
            catch (DbUpdateException exDb)
            {
                throw new ExceptionLocalDB(idClient, exDb.InnerException?.Message ?? string.Empty);
            }
            catch (Exception ex)
            {
                throw new ExceptionLocalDB(idClient, ex.Message);
            }
        }

        public virtual void UpDate(int idClient)
        {
            try
            {
                Task.Run(() => base.SendUpData(this, new DataEventArgs(Name, Load(idClient))));
            }
            catch (ExceptionLocalDB exception)
            {
                throw exception;
            }
            catch (DbUpdateException exDb)
            {
                throw new ExceptionLocalDB(idClient, exDb.InnerException?.Message ?? string.Empty);
            }
            catch (Exception ex)
            {
                throw new ExceptionLocalDB(idClient, ex.Message);
            }
        }

    }
}
