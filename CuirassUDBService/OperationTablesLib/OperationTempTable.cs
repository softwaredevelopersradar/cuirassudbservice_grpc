﻿namespace OperationTablesLib
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using CuirassUModelsDBLib;
    using CuirassUModelsDBLib.Errors;
    using CuirassUModelsDBLib.InheritorsEventArgs;
    using CuirassUModelsDBLib.InheritorsExceptions;

    public class OperationTempTable<T> : Operation, ITableAction where T : AbstractCommonTable
    {
        internal static Dictionary<int, T> TempTable = new Dictionary<int, T>();

        public OperationTempTable() : base()
        {
            IsTemp = true;
            object[] attrs = typeof(T).GetCustomAttributes(typeof(InfoTableAttribute), false);
            foreach (InfoTableAttribute info in attrs)
            {
                Name = info.Name;
            }
            //TempTable = new Dictionary<int, T>();
        }


        public virtual void Add(AbstractCommonTable record, int idClient)
        {
            try
            {
                if (TempTable.ContainsKey((int)(record.GetKey().FirstOrDefault() ?? throw new InvalidOperationException())))
                {
                    throw new ExceptionLocalDB(idClient, EnumDBError.RecordExist);
                }
                TempTable.Add((int)(record.GetKey().FirstOrDefault() ?? throw new InvalidOperationException()), record as T ?? throw new InvalidOperationException());
                UpDate(idClient);
            }
            catch (ExceptionLocalDB exception)
            {
                throw exception;
            }
            catch (Exception ex)
            {
                throw new ExceptionLocalDB(idClient, ex.Message);
            }
        }


        public virtual void AddRange(ClassDataCommon data, int idClient)
        {
            try
            {
                foreach (var rec in data.ListRecords)
                {
                    int key = (int)(rec.GetKey().FirstOrDefault() ?? throw new InvalidOperationException());
                    if (TempTable.ContainsKey(key))
                        TempTable[key].Update(rec as T ?? throw new InvalidOperationException());
                    else
                        TempTable.Add(key, rec as T ?? throw new InvalidOperationException());
                }
                UpAddRange(idClient, data);
                UpDate(idClient);

            }
            catch (ExceptionLocalDB exception)
            {
                throw exception;
            }
            catch (Exception ex)
            {
                throw new ExceptionLocalDB(idClient, ex.Message);
            }
        }


        public virtual void RemoveRange(ClassDataCommon data, int idClient)
        {
            try
            {
                foreach (var rec in data.ListRecords)
                {
                    int key = (int)(rec.GetKey().FirstOrDefault() ?? throw new InvalidOperationException());
                    if (TempTable.ContainsKey(key))
                        TempTable.Remove(key);
                }
                UpDate(idClient);

            }
            catch (ExceptionLocalDB exception)
            {
                throw exception;
            }
            catch (Exception ex)
            {
                throw new ExceptionLocalDB(idClient, ex.Message);
            }
        }


        public virtual void Change(AbstractCommonTable record, int idClient)
        {
            try
            {
                int key = (int)(record.GetKey().FirstOrDefault() ?? throw new InvalidOperationException());
                if (!TempTable.ContainsKey(key))
                {
                    throw new ExceptionLocalDB(idClient, EnumDBError.RecordNotFound);
                }
                TempTable[key].Update(record as T ?? throw new InvalidOperationException());
                UpRecord(idClient, record, NameChangeOperation.Change);
                UpDate(idClient);
            }
            catch (ExceptionLocalDB exception)
            {
                throw exception;
            }
            catch (Exception ex)
            {
                throw new ExceptionLocalDB(idClient, ex.Message);
            }
        }

        public virtual void ChangeRange(ClassDataCommon data, int idClient)
        {
            try
            {
                foreach (var record in data.ToList<T>())
                {
                    int key = (int)(record.GetKey().FirstOrDefault() ?? throw new InvalidOperationException());
                    if (TempTable.ContainsKey(key))
                    {
                        TempTable[key].Update(record as T);
                    }
                    else
                        TempTable.Add(key, record as T);
                }
                UpDate(idClient);
            }
            catch (ExceptionLocalDB exception)
            {
                throw exception;
            }
            catch (Exception ex)
            {
                throw new ExceptionLocalDB(idClient, ex.Message);
            }
        }

        public virtual void Clear(int idClient)
        {
            try
            {
                TempTable.Clear();
                UpDate(idClient);
            }
            catch (ExceptionLocalDB exception)
            {
                throw exception;
            }
            catch (Exception ex)
            {
                throw new ExceptionLocalDB(idClient, ex.Message);
            }
        }


        public virtual void Delete(AbstractCommonTable record, int idClient)
        {
            try
            {
                int key = (int)(record.GetKey().FirstOrDefault() ?? throw new InvalidOperationException());
                if (!TempTable.ContainsKey(key))
                {
                    throw new ExceptionLocalDB(idClient, EnumDBError.RecordNotFound);
                }
                TempTable.Remove(key);
                UpRecord(idClient, record, NameChangeOperation.Delete);
                UpDate(idClient);
            }
            catch (ExceptionLocalDB exception)
            {
                throw exception;
            }
            catch (Exception ex)
            {
                throw new ExceptionLocalDB(idClient, ex.Message);
            }
        }


        public ClassDataCommon Load(int idClient)
        {
            try
            {
                return ClassDataCommon.ConvertToListAbstractCommonTable(TempTable.Values.ToList()) ?? throw new InvalidOperationException();
            }
            catch (ExceptionLocalDB exception)
            {
                throw exception;
            }
            catch (Exception ex)
            {
                throw new ExceptionLocalDB(idClient, ex.Message);
            }
        }


        public void UpDate(int idClient)
        {
            try
            {
                base.SendUpData(this, new DataEventArgs(Name, ClassDataCommon.ConvertToListAbstractCommonTable(TempTable.Values.ToList()) ?? throw new InvalidOperationException()));
            }
            catch (ExceptionLocalDB exception)
            {
                throw exception;
            }
            catch (Exception ex)
            {
                throw new ExceptionLocalDB(idClient, ex.Message);
            }
        }


        public void UpAddRange(int idClient, ClassDataCommon records)
        {
            try
            {
                base.SendRange(this, new DataEventArgs(Name, records));
            }
            catch (ExceptionLocalDB exception)
            {
                throw exception;
            }
            catch (Exception ex)
            {
                throw new ExceptionLocalDB(idClient, ex.Message);
            }
        }


        public void UpRecord(int idClient, AbstractCommonTable record, NameChangeOperation action)
        {
            try
            {
                base.SendUpRecord(this, new RecordEventArgs(Name, record, action));
            }
            catch (ExceptionLocalDB exception)
            {
                throw exception;
            }
            catch (Exception ex)
            {
                throw new ExceptionLocalDB(idClient, ex.Message);
            }
        }
    }
}
