﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Media;
using GrpcDbClientLib;
using CuirassUModelsDBLib;
using CuirassUModelsDBLib.InheritorsEventArgs;
using System.Threading;
using System.Threading.Tasks;

namespace ClientTestAppNet5
{
    using System.Collections.ObjectModel;
    using System.Linq;

    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        ServiceClient? serviceClient;
        string ip = "127.0.0.1";
        int port = 30051;

        public MainWindow()
        {
            InitializeComponent();
            this.Name = "TestClient";
        }

        public void DispatchIfNecessary(Action action)
        {
            if (!Dispatcher.CheckAccess())
                Dispatcher.Invoke(action);
            else
                action.Invoke();
        }

        private void InitClientDB()
        {
            if (this.serviceClient == null) 
                return;
            serviceClient.OnConnect += ClientDB_OnConnect;
            serviceClient.OnDisconnect += HandlerDisconnect;
            serviceClient.OnUpData += HandlerUpData;
            //(serviceClient.Tables[NameTable.TableJammer] as ITableUpdate<TableJammer>).OnUpTable += MainWindow_OnUpTable;
            //(serviceClient.Tables[NameTable.TempSuppressFF] as ITableUpdate<TempSuppressFF>).OnUpTable += MainWindow_OnUpTable1;
            //(serviceClient.Tables[NameTable.TableAnalogResFF] as ITableUpdate<TableAnalogResFF>).OnUpTable += MainWindow_OnUpTable3;
            //(serviceClient.Tables[NameTable.TableResFF] as ITableUpdate<TableResFF>).OnUpTable += MainWindow_OnUpTable2; ;
            //(serviceClient.Tables[NameTable.TableFHSSExcludedJam] as ITableUpdate<TableFHSSExcludedJam>).OnUpTable += MainWindow_OnUpTable4;

        }

        private async void btnCon_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (serviceClient != null)
                    serviceClient.DisconnectAsync();
                    //await serviceClient.DisconnectAsync();
                    
                else
                {
                    serviceClient = new ServiceClient(this.Name, ip, port, 10000);
                    InitClientDB();
                    //await serviceClient.ConnectAsync();
                    serviceClient.ConnectAsync();
                }
            }
            catch (ExceptionClient exceptClient)
            {
                tbMessage.Foreground = System.Windows.Media.Brushes.Red;
                tbMessage.AppendText(exceptClient.Message);
            }
        }

        private void ClientDB_OnConnect(object sender, ClientEventArgs e)
        {
            DispatchIfNecessary(() =>
            {
                btnCon.Content = "Disconnect";
                tbMessage.Foreground = System.Windows.Media.Brushes.Black;
            });


            foreach (NameTable table in Enum.GetValues(typeof(NameTable)))
            {
                var tempTable = serviceClient.Tables[table].Load<AbstractCommonTable>();
                DispatchIfNecessary(() =>
                {
                    tbMessage.AppendText($"Load data from Db. {table.ToString()} count records - {tempTable.Count} \n");
                });
            }
        }

        void HandlerDisconnect(object obj, ClientEventArgs eventArgs)
        {
            DispatchIfNecessary(() =>
            {
                btnCon.Content = "Connect";
                if (eventArgs.GetMessage != "")
                {
                    tbMessage.Foreground = System.Windows.Media.Brushes.Red;
                    tbMessage.AppendText(eventArgs.GetMessage);
                }

            });

           
            serviceClient = null;
        }

        void HandlerUpData(object obj, DataEventArgs eventArgs)
        {
            Dispatcher.BeginInvoke(System.Windows.Threading.DispatcherPriority.Normal, (ThreadStart)delegate ()
            {
                tbMessage.Foreground = System.Windows.Media.Brushes.Black;

                tbMessage.AppendText($"Load data from Db. Tables {eventArgs.Name.ToString()} count records - {eventArgs.AbstractData.ListRecords.Count} \n");
            });
        }

        int index = 1;
        private void ButAdd_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                int nufOfRec = Convert.ToInt32(RecordID.Text);
                Random rand = new Random();
                object record = null;
                switch (Tables.SelectedItem)
                {

                    case NameTable.TableSectorsRecon:
                        record = new TableSectorsRecon
                        {
                            AngleMin = Convert.ToInt16(rand.Next(0, 360)),
                            AngleMax = Convert.ToInt16(rand.Next(0, 360))
                        };
                        break;
                    case NameTable.TableFreqForbidden:
                        record = new TableFreqForbidden
                        {
                            FreqMaxMHz = rand.Next(150000, 250000),
                            FreqMinMHz = rand.Next(30000, 50000),
                            Pulse = new ParamDP() { Duration = rand.Next(10, 1000) }
                        };
                        break;
                    case NameTable.TableFreqRangesRecon:
                        record = new TableFreqRangesRecon
                        {
                            FreqMaxMHz = rand.Next(150000, 250000),
                            FreqMinMHz = rand.Next(30000, 50000),
                        };
                        break;
                    case NameTable.TableRes:
                        record = new TableRes
                        {
                            Id = index,
                            FrequencyMHz = rand.NextDouble(),
                            Bearing = null,
                            //StandardDeviation = 2,
                            Pulse = new ParamDP()
                            {
                                Duration = rand.Next(0, 500),
                                Period = rand.Next(0, 500)
                            },
                            Series = new ParamDP(),
                            TargetType = null,
                            //ModulationType = null,
                            Note = null,
                            //Series = new ParamDP()
                            //{
                            //    Duration = rand.Next(0, 500),
                            //    Period = rand.Next(0, 500)
                            //},
                            //Note = DateTime.Now.ToString(),
                            AntennaMaster = 2,
                            AntennaSlave = 4
                        };
                        break;
                    case NameTable.GlobalPropertiesRRS:
                        record = new GlobalPropertiesRRS
                        {
                            Id = 1,
                            Location = new Location() { Altitude = rand.Next(0, 360), Latitude = rand.Next(0, 360), Longitude = rand.Next(0, 360) }
                        };
                        break;
                    case NameTable.GlobalProperties:
                        record = new GlobalProperties
                        {
                            Id = 1,
                            Location = new Coord() { Altitude = rand.Next(0, 360), Latitude = rand.Next(0, 360), Longitude = rand.Next(0, 360) },
                            Calibration = new Calibration() { Type = CalibrationTypes.Generator },
                            RadioIntelegence = new RadioIntelegence()
                            {
                                CourseAngle = 0,
                                SynchronizeType = SynchronizeTypes.GPS
                            }
                        };
                        break;
                    case NameTable.TableResArchive:
                        record = new TableResArchive
                        {
                            FrequencyMHz = rand.NextDouble(),
                            //Pulse = new ParamDP()
                            //{
                            //    Duration = rand.Next(0, 500),
                            //    Period = rand.Next(0, 500)
                            //},
                            //Series = null,
                            //Series = new ParamDP()
                            //{
                            //Duration = rand.Next(0, 500),
                            //Period = rand.Next(0, 500)
                            //},
                            AntennaMaster = 2,
                            AntennaSlave = 4,
                            //DateTimeStart = DateTime.Now, DateTimeStop = DateTime.Now,
                            // Track = new System.Collections.ObjectModel.ObservableCollection<TableTrackArchive>(),
                            //Note = "", TargetType = ""
                            //Track = new System.Collections.ObjectModel.ObservableCollection<TableTrackArchive>() { new TableTrackArchive() {  Tau1 = rand.Next(0, 500) } }
                        };
                        break;
                    case NameTable.TableResDistribution:
                        record = new TableResDistribution
                        {
                            Id = index,
                            FrequencyMHz = rand.NextDouble(),
                            //Pulse = new ParamDP()
                            //{
                            //    Duration = rand.Next(0, 500),
                            //    Period = rand.Next(0, 500)
                            //},
                            AntennaMaster = 2,
                            AntennaSlave = 4,
                            //Track = new System.Collections.ObjectModel.ObservableCollection<TableTrackDistribution>() { new TableTrackDistribution() { Id = index, Tau1 = rand.Next(0, 500) } }
                        };
                        break;
                    case NameTable.TableResPattern:
                        record = new TableResPattern()
                                     {
                                         
                                         FrequencyMinMHz = rand.NextDouble()
                                     };
                        break;
                    case NameTable.TableRemotePoints:
                        record = new TableRemotePoints()
                        {
                            //Coordinates = new Coord() { Altitude = rand.Next(0, 360), Latitude = rand.Next(0, 360), Longitude = rand.Next(0, 360) },
                            TimeError = (short)rand.Next(0, 500)
                        };
                        break;
                    default:
                        return;
                }
                index++;
                if (record != null)
                    serviceClient?.Tables[(NameTable)Tables.SelectedItem].Add(record);
            }
            catch (ExceptionClient exceptClient)
            {
                tbMessage.Foreground = System.Windows.Media.Brushes.Red;
                tbMessage.AppendText(exceptClient.Message);
            }
        }


        private void SendRange_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                dynamic listForSend = new List<AbstractCommonTable>();
                Random rand = new Random();
                //object record = null;
                switch (Tables.SelectedItem)
                {
                    case NameTable.TableSectorsRecon:
                        listForSend = new List<TableSectorsRecon>();
                        for (int i = 0; i < 8; i++)
                        {
                            TableSectorsRecon record = new TableSectorsRecon
                            {
                                AngleMin = Convert.ToInt16(rand.Next(0, 360)),
                                AngleMax = Convert.ToInt16(rand.Next(0, 360))
                            };
                            listForSend.Add(record);
                        }
                        break;
                    case NameTable.TableFreqForbidden:
                        listForSend = new List<TableFreqForbidden>();
                        for (int i = 0; i < 8; i++)
                        {
                            TableFreqForbidden record = new TableFreqForbidden
                            {
                                FreqMaxMHz = rand.Next(150000, 250000),
                                FreqMinMHz = rand.Next(30000, 50000),
                            };
                            listForSend.Add(record);
                        }
                        break;
                    case NameTable.TableFreqRangesRecon:
                        listForSend = new List<TableFreqRangesRecon>();
                        for (int i = 0; i < 8; i++)
                        {
                            TableFreqRangesRecon record = new TableFreqRangesRecon
                            {
                                //Id = rand.Next(1, 1000),
                                FreqMaxMHz = rand.Next(150000, 250000),
                                FreqMinMHz = rand.Next(30000, 50000),
                            };
                            listForSend.Add(record);
                        }
                        break;
                    case NameTable.TableRes:
                        listForSend = new List<TableRes>();
                        for (int i = 0; i < 8; i++)
                        {
                            TableRes record = new TableRes
                            {
                                Id = index,
                                FrequencyMHz = rand.NextDouble(),
                                Bearing = rand.Next(0, 360),
                                //StandardDeviation = 3,
                                Pulse = new ParamDP()
                                {
                                    Duration = rand.Next(0, 500),
                                    Period = rand.Next(0, 500)
                                },
                                Series = new ParamDP()
                                {
                                    Duration = rand.Next(0, 500),
                                    Period = rand.Next(0, 500)
                                },
                                Note = DateTime.Now.ToString()

                            };
                            index++;
                            listForSend.Add(record);
                        }
                        break;
                    default:
                        return;
                }
                serviceClient?.Tables[(NameTable)Tables.SelectedItem].AddRange(listForSend);
            }
            catch (ExceptionClient exceptClient)
            {
                tbMessage.Foreground = Brushes.Red;
                tbMessage.AppendText(exceptClient.Message);
            }
        }


        private void ButDelete_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                int nufOfRec = Convert.ToInt32(RecordID.Text);
                Random rand = new Random();
                object record = null;
                switch (Tables.SelectedItem)
                {

                    case NameTable.TableSectorsRecon:
                        record = new TableSectorsRecon
                        {
                            Id = nufOfRec
                        };
                        break;
                    case NameTable.TableFreqForbidden:
                        record = new TableFreqForbidden
                        {
                            Id = nufOfRec
                        };
                        break;
                    case NameTable.TableFreqRangesRecon:
                        record = new TableFreqRangesRecon
                        {
                            Id = nufOfRec
                        };
                        break;
                    case NameTable.TableRes:
                        record = new TableRes
                        {
                            Id = nufOfRec
                        };
                        break;
                    case NameTable.GlobalPropertiesRRS:
                        record = new GlobalPropertiesRRS
                        {
                            Id = nufOfRec
                        };
                        break;
                    case NameTable.GlobalProperties:
                        record = new GlobalProperties
                        {
                            Id = nufOfRec
                        };
                        break;
                    case NameTable.TableResArchive:
                        record = new TableResArchive()
                        {
                            Id = nufOfRec
                        };
                        break;
                    case NameTable.TableResPattern:
                        record = new TableResPattern()
                                     {
                                         Id = nufOfRec
                                     };
                        break;
                    default:
                        break;


                }
                if (record != null)
                    serviceClient?.Tables[(NameTable)Tables.SelectedItem].Delete(record);
            }
            catch (ExceptionClient exceptClient)
            {
                tbMessage.Foreground = System.Windows.Media.Brushes.Red;
                tbMessage.AppendText(exceptClient.Message);
            }
        }


        private void ButRemove_Click(object sender, RoutedEventArgs e)
        {
            int nufOfRec = Convert.ToInt32(RecordID.Text);
            Random rand = new Random();
            //object record = null;
            dynamic list = new object();
            switch (Tables.SelectedItem)
            {
                case NameTable.TableSectorsRecon:
                    list = new List<TableSectorsRecon>();
                    for (int i = nufOfRec; i < nufOfRec + 3; i++)
                    {
                        var record = new TableSectorsRecon
                        {
                            Id = i
                        };
                        list.Add(record);
                    }

                    break;
                case NameTable.TableFreqForbidden:
                    list = new List<TableFreqForbidden>();
                    for (int i = nufOfRec; i < nufOfRec + 3; i++)
                    {
                        var record = new TableFreqForbidden
                        {
                            Id = nufOfRec
                        };
                        list.Add(record);
                    }
                    break;
                case NameTable.TableFreqRangesRecon:
                    list = new List<TableFreqRangesRecon>();
                    for (int i = nufOfRec; i < nufOfRec + 3; i++)
                    {
                        var record = new TableFreqRangesRecon
                        {
                            Id = nufOfRec
                        };
                        list.Add(record);
                    }
                    break;
                case NameTable.TableRes:
                    list = new List<TableRes>();
                    for (int i = nufOfRec; i < nufOfRec + 3; i++)
                    {
                        var record = new TableRes
                        {
                            Id = nufOfRec
                        };
                        list.Add(record);
                    }
                    break;
                default:
                    break;


            }
            serviceClient?.Tables[(NameTable)Tables.SelectedItem].RemoveRange(list);
        }


        private void ButClear_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                serviceClient?.Tables[(NameTable)Tables.SelectedItem].Clear();
            }
            catch (ExceptionClient exceptClient)
            {
                Foreground = System.Windows.Media.Brushes.Red;
                tbMessage.AppendText(exceptClient.Message);
            }
        }


        private void ButLoad_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                tbMessage.Foreground = Brushes.Black;


                List<AbstractCommonTable> table = new List<AbstractCommonTable>();
                NameTable nameTable = (NameTable)Tables.SelectedItem;

                table = serviceClient?.Tables[nameTable].Load<AbstractCommonTable>();
                tbMessage.AppendText($"Load data from Db. {((NameTable)Tables.SelectedItem).ToString()} count records - {table.Count} \n");

            }
            catch (ExceptionClient exeptClient)
            {
                tbMessage.Foreground = System.Windows.Media.Brushes.Red;
                tbMessage.AppendText(exeptClient.Message);
            }
            catch (ExceptionDatabase excpetService)
            {
                tbMessage.Foreground = System.Windows.Media.Brushes.Red;
                tbMessage.AppendText(excpetService.Message);
            }
        }


        private void butChange_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                int nufOfRec = Convert.ToInt32(RecordID.Text);
                Random rand = new Random();
                object record = null;
                switch (Tables.SelectedItem)
                {

                    case NameTable.TableSectorsRecon:
                        record = new TableSectorsRecon
                        {
                            Id = nufOfRec,
                            AngleMin = Convert.ToInt16(rand.Next(0, 360)),
                            AngleMax = Convert.ToInt16(rand.Next(0, 360))
                        };
                        break;
                    case NameTable.TableFreqForbidden:
                        record = new TableFreqForbidden
                        {
                            Id = nufOfRec,
                            FreqMaxMHz = rand.Next(150000, 250000),
                            FreqMinMHz = rand.Next(30000, 50000),
                        };
                        break;
                    case NameTable.TableFreqRangesRecon:
                        record = new TableFreqRangesRecon
                        {
                            Id = nufOfRec,
                            FreqMaxMHz = rand.Next(150000, 250000),
                            FreqMinMHz = rand.Next(30000, 50000),
                        };
                        break;
                    case NameTable.TableRes:
                        record = new TableRes
                        {
                            Id = nufOfRec,
                            FrequencyMHz = rand.NextDouble(),
                            Bearing = rand.Next(0, 360),
                            //StandardDeviation = 4,
                            Pulse = null,
                            //Pulse = new ParamDP()
                            //{
                            //    Duration = rand.Next(0, 500),
                            //    Period = rand.Next(0, 500)
                            //},
                            Series = new ParamDP()
                            {
                                Duration = rand.Next(0, 500),
                                Period = rand.Next(0, 500)
                            },
                            //Note = DateTime.Now.ToString()
                        };
                        break;
                    case NameTable.GlobalPropertiesRRS:
                        var list = serviceClient?.Tables[NameTable.GlobalProperties].Load<GlobalProperties>().ToList();
                        list[0].Calibration.Type = CalibrationTypes.Load;
                        
                        //var record = new GlobalPropertiesRRS
                        //{
                        //    Id = 1,
                        //    Location = new Location() { Altitude = rand.Next(0, 360), Latitude = rand.Next(0, 360), Longitude = rand.Next(0, 360) },
                        //    //Calibration = new Calibration() { Type = CalibrationTypes.Load },
                        //    //RadioIntelegence = new RadioIntelegence()
                        //    //{
                        //    //    CourseAngle = 0,
                        //    //    SynchronizeType = SynchronizeTypes.GPS
                        //    //}
                        //};
                        break;
                    case NameTable.GlobalProperties:
                        var list2 = serviceClient?.Tables[NameTable.GlobalProperties].Load<GlobalProperties>().ToList();
                        list2[0].Calibration.Type = CalibrationTypes.Generator;

                        //record = new GlobalProperties
                        //{
                        //    Id = 1,
                        //    Location = new Coord() { Altitude = rand.Next(0, 360), Latitude = rand.Next(0, 360), Longitude = rand.Next(0, 360) },
                        //    Calibration = new Calibration() { Type = CalibrationTypes.Load },
                        //    RadioIntelegence = new RadioIntelegence()
                        //    {
                        //        CourseAngle = 0,
                        //        SynchronizeType = SynchronizeTypes.GPS
                        //    }
                        //};
                        break;
                    case NameTable.TableRemotePoints:
                        var table1 = serviceClient?.Tables[NameTable.TableRemotePoints].Load<TableRemotePoints>();
                        table1[0].Coordinates = new Coord() { Longitude = rand.Next(-180, 180) };
                        record = table1[0].Clone();
                        //table1.cOtable1 == null ?? table1[0].Coordinates.Longitude = rand.Next(0, 360);
                        break;
                        
                    case NameTable.TableResArchive:
                        record = new TableResArchive()
                                     {
                                         Id = nufOfRec,
                                         FrequencyMHz = rand.NextDouble(),
                                         //StandardDeviation = 4,
                                         Pulse = null,
                                         //Pulse = new ParamDP()
                                         //{
                                         //    Duration = rand.Next(0, 500),
                                         //    Period = rand.Next(0, 500)
                                         //},
                                         Series = new ParamDP()
                                                      {
                                                          Duration = rand.Next(0, 500),
                                                          Period = rand.Next(0, 500)
                                                      },
                                         DateTimeStart = DateTime.Now, Track = new ObservableCollection<TableTrackArchive>(){new TableTrackArchive(){Tau1 = 1, Time = DateTime.Now}}
                                         //Note = DateTime.Now.ToString()
                                     };
                        break;
                    case NameTable.TableResPattern:
                        record = new TableResPattern()
                                     {
                                         Id = nufOfRec,
                                         FrequencyMinMHz = rand.NextDouble(),
                                         //StandardDeviation = 4,
                                         Pulse = null,
                                         //Pulse = new ParamDP()
                                         //{
                                         //    Duration = rand.Next(0, 500),
                                         //    Period = rand.Next(0, 500)
                                         //},
                                         Series = new ParamDP()
                                                      {
                                                          Duration = rand.Next(0, 500),
                                                          Period = rand.Next(0, 500)
                                                      },
                                         ModulationType = ModulationType.LFM,
                                         WorkingMode = WorkingMode.LandLand,
                                         ImageByte = new byte[3]{3,3,4}
                                         //Note = DateTime.Now.ToString()
                                     };
                        break;
                    default:
                        return;
                }
                if (record != null)
                    serviceClient?.Tables[(NameTable)Tables.SelectedItem].Change(record);
            }
            catch (ExceptionClient exceptClient)
            {
                tbMessage.Foreground = System.Windows.Media.Brushes.Red;
                tbMessage.AppendText(exceptClient.Message);
            }
        }


        private void butChangeRange_Click(object sender, RoutedEventArgs e)
        {
            Random rand = new Random();
            int nufOfRec = Convert.ToInt32(RecordID.Text);
            dynamic listForSend = new List<AbstractCommonTable>();

            //switch (Tables.SelectedItem)
            //{

            //    case NameTable.TableJammer:
            //        listForSend = new List<TableJammer>();
            //        for (int i = nufOfRec; i < nufOfRec + 3; i++)
            //        {
            //            listForSend.Add(new TableJammer
            //            {
            //                Id = i,
            //                Coordinates = new Coord
            //                {
            //                    Altitude = (float)rand.NextDouble() * rand.Next(0, 360),
            //                    Latitude = rand.NextDouble() * rand.Next(0, 360),
            //                    Longitude = rand.NextDouble() * rand.Next(0, 360)
            //                }
            //            });
            //        };
            //        break;
            //    case NameTable.TableFreqForbidden:
            //        listForSend = new List<TableFreqForbidden>();
            //        for (int i = nufOfRec; i < nufOfRec + 3; i++)
            //        {
            //            listForSend.Add(new TableFreqForbidden
            //            {
            //                Id = i,
            //                FreqMaxKHz = rand.Next(150000, 250000),
            //                FreqMinKHz = rand.Next(30000, 50000)
            //            });
            //        };
            //        break;
            //    case NameTable.TableADSB:
            //        listForSend = new List<TableADSB>();
            //        for (int i = nufOfRec; i < nufOfRec + 3; i++)
            //        {
            //            listForSend.Add(new TableADSB
            //            {
            //                ICAO = i.ToString(),
            //                Time = DateTime.Now
            //            });
            //        };
            //        break;
            //    case NameTable.TableResFF:
            //        listForSend = new List<TableResFF>();
            //        for (int i = nufOfRec; i < nufOfRec + 3; i++)
            //        {
            //            listForSend.Add(new TableResFF
            //            {
            //                Id = i,
            //                FrequencyKHz = rand.Next(),
            //                //ListTrack = new System.Collections.ObjectModel.ObservableCollection<TableTrackResFF>() { new TableTrackResFF() { BearingOwn = rand.Next() }, new TableTrackResFF() { BearingOwn = rand.Next() } }
            //            });
            //        };
            //        break;

            //    default:
            //        break;
            //}

            serviceClient?.Tables[(NameTable)Tables.SelectedItem].ChangeRange(listForSend);
        }


        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            serviceClient?.Dispose();
            //System.Windows.Threading.Dispatcher.ExitAllFrames();
        }

        private void butTest_Click(object sender, RoutedEventArgs e)
        {

        }
    }
}
