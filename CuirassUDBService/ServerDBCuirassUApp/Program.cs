﻿using CuirassUDbPackage;
using Grpc.Core;
using CuirassUModelsDBLib.InheritorsEventArgs;
using ServerDBCuirassULib;
using System;
using System.IO;
using System.Runtime.InteropServices;
using System.Runtime.Serialization.Json;

namespace ServerDBCuirassUApp
{
    class Program
    {
        static void Main()
        {
            //не работает
            if (Instance.HasRunningCopy)
            {
                Console.WriteLine("The application is already running!");
                //Thread.Sleep(3000);

                Console.ReadLine();
                return;
            }
            //Process.GetCurrentProcess().PriorityClass = ProcessPriorityClass.High;
            //SetConsoleCloseHandler();
            PropertyHost? propertyHost = ReadProperty();
            if (propertyHost == null)
            {
                propertyHost = CreateProperty();
            }
            if (propertyHost == null)
            {
                Console.WriteLine("Can't create property...");
                Console.ReadLine();
                return;
            }

            Server server = new Server
            {
                Services = { CuirassUDbTransmission.BindService(new ProtoServer()) },
                Ports = { new ServerPort(propertyHost.IpAddress, propertyHost.PortHttp, ServerCredentials.Insecure) }
            };
            server.Start();
            ProtoServer.OnReceiveErrorDB += ServiceDB_OnReceiveErrorDB;
            ProtoServer.OnReceiveMsgDB += ServiceDB_OnReceiveMsgDB;
            ProtoServer.OnErrorClient += ServiceDB_OnErrorClient;
            ProtoServer.OnStateClient += ServiceDB_OnStateClient;
            Console.WriteLine("Cuirass-U DB server is listening on " + propertyHost.IpAddress + ":" + propertyHost.PortHttp);
            Console.WriteLine("Press any key to stop the server...");
            Console.ReadLine();
            server.ShutdownAsync().Wait();
            return;
        }

        private static void SetConsoleCloseHandler()
        {
            _handler = ConsoleEventCallback;
            SetConsoleCtrlHandler(_handler, true);
        }


        static bool ConsoleEventCallback(int eventType)
        {
            //// Console window closing   Fedya
            //if (eventType == 2)
            //{
            //    _server.TaskManager.ClearTasks();
            //    _server.TaskManager.DatabaseController.Disconnect();
            //    Thread.Sleep(1000);
            //    StopSimulatorIfNecessary();
            //    Config.Save();
            //}


            ////WCF
            //ServerDBLib.ServiceDB.TryDisconnectClients();
            //Environment.Exit(0);
            return false;
        }

        public static PropertyHost? ReadProperty()
        {
            DataContractJsonSerializer jsonFormat = new DataContractJsonSerializer(typeof(PropertyHost));
            try
            {
                using (FileStream stream = new FileStream(Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location) + "\\propertyHost.json", FileMode.OpenOrCreate))
                {
                    PropertyHost? propertyHost = (PropertyHost?)jsonFormat.ReadObject(stream);
                    return propertyHost;
                }
            }
            catch (Exception)
            {
                return null;
            }
        }

        public static PropertyHost? CreateProperty()
        {
            PropertyHost propertyHost;
            DataContractJsonSerializer jsonFormat = new DataContractJsonSerializer(typeof(PropertyHost));
            try
            {
                string addresIp = "127.0.0.1";
                int portHTTP = 30051;
                propertyHost = new PropertyHost(addresIp, portHTTP);
                using (FileStream stream = new FileStream(Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location) + "\\propertyHost.json", FileMode.OpenOrCreate))
                {
                    jsonFormat.WriteObject(stream, propertyHost);
                }
                return propertyHost;
            }
            catch (Exception ex)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.Write(ex.Message);
                return null;
            }
        }


        private static ConsoleEventDelegate? _handler;

        private delegate bool ConsoleEventDelegate(int eventType);
        [DllImport("kernel32.dll", SetLastError = true)]
        private static extern bool SetConsoleCtrlHandler(ConsoleEventDelegate callback, bool add);



        private static void ServiceDB_OnReceiveMsgDB(object? sender, AStandardEventArgs e)
        {
            Console.ForegroundColor = ConsoleColor.White;
            Console.BackgroundColor = ConsoleColor.Black;
            Console.WriteLine(e.GetMessage);
        }

        private static void ServiceDB_OnReceiveErrorDB(object? sender, AStandardEventArgs e)
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.BackgroundColor = ConsoleColor.Black;
            Console.WriteLine(e.GetMessage);
        }

        private static void ServiceDB_OnStateClient(object? sender, AStandardEventArgs e)
        {
            Console.BackgroundColor = ConsoleColor.Blue;
            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine(e.GetMessage);
        }

        private static void ServiceDB_OnErrorClient(object? sender, AStandardEventArgs e)
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.BackgroundColor = ConsoleColor.Black;
            Console.WriteLine(e.GetMessage);
        }
    }
}
